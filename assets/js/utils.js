export const generateSelectBox = (name, values = [], pleaseSelect = 'Please select', optionProps, additionalData = [], defaultValue = false, label = '') => {
    let wrapper = jQuery('<div style="display: flex; flex-direction: column; width: 25%"></div>')

    let selectBox = jQuery('<select id="' + name + '" name="' + name + '"><option value="" style="display: none" disabled selected>' + pleaseSelect + '</option></select>')

    if (values.length > 0) {
      let options = values.length > 0 && generateOptions(values, optionProps.value, optionProps.title, additionalData, defaultValue)

      selectBox.append(options)
    }

    if (label.length > 0) {
      let labelBox = jQuery('<label class="' + name + '" for="' + name + '"> ' + label + '</label>')
      wrapper.append(labelBox)
    }

    wrapper.append(selectBox)

  return wrapper
}

export const generateOptions = (options = [], valueProp, titleProp, additionalData = [], defaultValue) => {
  return options.map(value => '<option ' + generateSelected(defaultValue, value, valueProp) + generateOptionData(additionalData, value) + ' value="' + value[valueProp] + '">' + value[titleProp] + '</option>')
}

export const generateSelected = (defaultValue, value, valueProp) => {
  return defaultValue && value[valueProp] === defaultValue ? "selected" : ""
}

export const generateOptionData = (data, value) => {
  if (! data.length) return ''

  return data.map(item => 'data-' + item + '=' + '"' + value[item] + '"').join(' ')
}

export const setLoading = (wrapper, loading) => {
    const loadingHtml = jQuery(wrapper + ' .guru-loading');
    if (loading) return loadingHtml.show()
    return loadingHtml.hide()
}

export const toTimestamp = (strDate) => {
  let date = Date.parse(strDate)

  return date / 1000
}

export const toDate = (timestamp) => {
  return new Date(timestamp * 1000).toLocaleDateString()
}
