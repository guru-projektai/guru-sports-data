import { generateSelectBox, setLoading, toTimestamp, toDate } from './utils.js'

class AdminSettings {
  constructor() {
    this.bindings = new Bindings()
    this.syncCreation = new SyncCreation()
  }

  init() {
    this.bindings.init()
    this.syncCreation.init()
  }
}

class Bindings {
  constructor() {
    this.model = {
      url: '',
      seasons: [],
      league: {},
    }
  }

  init = () => {
    this.setBindings()
    this.loadSyncs()
  }

  setBindings = () => {
    let that = this;
    jQuery(function( $ ) {
      $('#leagueForm').on('submit', function (e) {
        that.seasonsSync();
        e.preventDefault();
      });

      $('#scrapeForm').on('submit', function (e) {
        that.scrapeUpdate();
        e.preventDefault();
      });

      $(document).on('click', '#global-save', async function (e) {
        await that.globalSave(e);
        (new SyncCreation()).getData();
      });

      $(document).on('change', '#league_select', function (e) {
        that.loadSeasons(e);
        e.preventDefault();
      });

      $(document).on('change', '#season_select', function (e) {
        that.loadDatePickers(e);
        e.preventDefault();
      });

      $(document).on('click', '#saveSync', async function (e) {
        await that.saveSync(e);
        that.loadSyncs();
      })

      $(document).on('click', '.deleteSync', async function (e) {
        if (window.confirm('Are you sure you want to delete it?')) {
          await that.deleteSync(e);
          that.loadSyncs();
        }
        e.preventDefault();
      });

      $(document).on('click', '.fetchSpecificSync', async function (e) {
        that.fetchSpecificSync(e);
        e.preventDefault();
      });

      $(document).on('click', '#manualFetch', function (e) {
        that.manualFetchEvents(e);
        e.preventDefault();
      });
    });
  }

  appendHtml = (seasons, league) => {
    jQuery(function( $ ) {
      let summary = $('<div>League: ' + league.title + ' with ' + seasons.length + ' seasons synced.</div>')

      let dataSection = $('.guru-sports-data-section')
      dataSection.find('.league-section').html(summary)
      dataSection.find('.global-save-wrap').html($('<input type="submit" id="global-save" class="button button-primary" value="Save League"/>'))
    });
  }

  loadData(result) {
    let that = this;
    jQuery(function ($) {
      if (result.success) {
        that.appendHtml(result.data.seasons, result.data.league)

        that.model.url = result.data.url
        that.model.seasons = result.data.seasons
        that.model.league = result.data.league
      } else {

          let response = $('.response')
          response.html(result.data).show()
          setTimeout(() => {
            response.html(result.data).hide()
          }, 7000)

          let section = $('.guru-sports-data-section')
          section.find('.league-section').empty()
          section.find('.global-save-wrap').empty()
      }
    });
  }

  seasonsSync() {
    let that = this;
    jQuery(function( $ ) {
      let form = $('#leagueForm')
      const formData = form.serializeArray()

      let button = form.find('#syncSeasons')
      button.val('Syncing...')
      button.prop('disabled', true)

      $.ajax({
        type: 'POST',
        url: 'admin-ajax.php',
        data: {
          action: 'retrieve_seasons_callback', // load function hooked to: "wp_ajax_*" action hook
          url: formData.find(item => item.name === 'url') && formData.find(item => item.name === 'url').value,
        }, success: result => that.loadData(result),
        error: () => alert('error'),
        complete: () => {
          button.val('Sync')
          button.prop('disabled', false)
        }
      })
    });
  }

  scrapeUpdate() {
    jQuery(function( $ ) {
      let form = $('#scrapeForm')
      const formData = form.serializeArray()
      let button = form.find('#scrapeUpdate')
      button.val('Updating...')
      button.prop('disabled', true)

      $.ajax({
        type: 'POST',
        url: 'admin-ajax.php',
        data: {
          action: 'update_proxy_callback', // load function hooked to: "wp_ajax_*" action hook
          staging_odds_api: formData.find(item => item.name === 'staging_odds_api') && formData.find(item => item.name === 'staging_odds_api').value,
          production_odds_api: formData.find(item => item.name === 'production_odds_api') && formData.find(item => item.name === 'production_odds_api').value,
          odds_api_url: formData.find(item => item.name === 'odds_api_url') && formData.find(item => item.name === 'odds_api_url').value
        }, success: result => {
          location.reload();
        },
        error: () => alert('error'),
        complete: () => {
          button.val('Update')
          button.prop('disabled', false)
        }
      })
    });
  }

  globalSave(e) {
    let that = this;
    jQuery(function( $ ) {
      let button = $(e.target)

      button.val('Saving...')
      button.prop('disabled', true)

      return $.ajax({
        type: 'POST',
        url: 'admin-ajax.php',
        data: {
          action: 'save_new_league_with_seasons', // load function hooked to: "wp_ajax_*" action hookont-size: 12px;
          data: that.model,
        }, success: function (result) {
          let response = $('.response')

          response.html(result.data).show()
          setTimeout(() => {
            response.html(result.data).hide()
          }, 7000)
        },
        error: function () {
          alert("error")
        },
        complete: function () {
          button.val('Save League')
          button.prop('disabled', false)
        }
      })
    });
  }

  loadSeasons(e) {
    jQuery(function( $ ) {
      let syncCreationForm = $('#sync-creation')
      const formData = syncCreationForm.serializeArray()
      $('.datepickers_block').hide()
      $('#saveSync').hide()

      setLoading('#sync-creation', true)
      $.ajax({
        type: 'POST',
        url: 'admin-ajax.php',
        data: {
          action: 'get_seasons', // load function hooked to: "wp_ajax_*" action hook
          league_id: formData.find(item => item.name === 'league_select') && formData.find(item => item.name === 'league_select').value,
        }, success: function (response) {
          const seasonSelectBox = generateSelectBox(
            'season_select',
            response.data,
            'Select season',
            {value: 'season_id', title: 'season_title'},
            ['start_date', 'end_date'],
            false,
            'Select season'
          )

          const periodSelectBox = generateSelectBox(
            'period_select',
            [
              {
                value: '1 week',
                name: '1 Week',
              },
              {
                value: '2 weeks',
                name: '2 Weeks',
              },
              {
                value: '1 month',
                name: '1 month',
              },
              {
                value: '2 months',
                name: '2 Months',
              }
            ],
            'Select period',
            {value: 'value', title: 'name'},
            [],
            false,
            'Select period'
          )

          syncCreationForm.find('.season_select_block').html(seasonSelectBox)
          syncCreationForm.find('.period_select_block').html(periodSelectBox)
        },
        error: function (xhr) {
          alert(JSON.parse(xhr.responseText))
        },
        complete: function () {
          setLoading('#sync-creation', false)
        }
      })
    });
  }

  loadDatePickers(e) {
    jQuery(function( $ ) {
      // const startDate = $(e.target).find(':selected').data('start_date') * 1000
      // const endDate = $(e.target).find(':selected').data('end_date') * 1000
      //
      // flatpickr('#start-date', {
      //   defaultDate: startDate,
      //   minDate: startDate,
      //   maxDate: endDate,
      // })
      //
      // flatpickr('#end-date', {
      //   defaultDate: endDate,
      //   minDate: startDate,
      //   maxDate: endDate,
      // })


      $('#saveSync').show()
      $('.datepickers_block').show()
    });
  }

  saveSync(e) {
    e.preventDefault()
    jQuery(function( $ ) {
      let syncCreationForm = $('#sync-creation')
      let data = syncCreationForm.serializeArray()

      let period = syncCreationForm.find('#period_select').val()
      // let startDate = syncCreationForm.find('#start-date').val()
      // if (startDate) startDate = toTimestamp(startDate)
      //
      // let endDate = syncCreationForm.find('#end-date').val()
      // if (endDate) endDate = toTimestamp(endDate)

      data.push(
        {name: 'period', value: period},
      )

      setLoading('#sync-creation', true)

      return $.ajax({
        type: 'POST',
        url: 'admin-ajax.php',
        data: {
          action: 'save_sync', // load function hooked to: "wp_ajax_*" action hook
          data,
        }, success: function (response) {
          if (response.success) {
            setTimeout(() => {
              $('.response-creation').html(response.data).hide()
            }, 7000)
            $('#league_select').val(null)
            $('#season_select').hide()
            $('.season_select').hide()
            $('.datepickers_block').hide()
            $('#saveSync').hide()
          }
        },
        error: function () {
          alert("error")
        },
        complete: function () {
          setLoading('#sync-creation', false)
        }
      })
    });
  }

  loadSyncs() {
    jQuery(function( $ ) {
      setLoading('.active-syncs', true)

      $.ajax({
        type: 'GET',
        url: 'admin-ajax.php',
        data: {
          action: 'get_syncs', // load function hooked to: "wp_ajax_*" action hook
        }, success: function (response) {
          let table = $('.active-syncs').find('table tbody')
          if (!response.data) return table.html('<td colspan="5">No active synchronizations yet</td>')

          let itemRow = $('<div></div>')
          response.data.forEach(item => {
            let row = $('<tr></tr>')

            $('<td></td>').text(item.league_title).appendTo(row)
            $('<td></td>').text(item.season_title).appendTo(row)
            $('<td></td>').text(item.count).appendTo(row)
            $('<td></td>').text(item.period).appendTo(row)
            $('<td></td>').html(item.shortcodes.map((shortcode) => {
              return '<span>' + shortcode + '</span><br />'
            })).appendTo(row)
            $('<td></td>').text(item.last_updated).appendTo(row)
            $('<td><input data-id="' + item.id + '" type="submit" value="Delete" class="deleteSync button button-link-delete"/></td>').appendTo(row)
            $('<td><input data-id="' + item.id + '" type="submit" value="Fetch" class="fetchSpecificSync button button-primary"/></td>').appendTo(row)

            itemRow.append(row)
          })

          table.html(itemRow.children())
        },
        error: function () {
          alert("error")
        },
        complete: function () {
          setLoading('.active-syncs', false)
        }
      })
    });
  }

  deleteSync(e) {
    jQuery(function( $ ) {
      setLoading('.active-syncs', true)

      const id = $(e.target).data('id')

      return $.ajax({
        type: 'POST',
        url: 'admin-ajax.php',
        data: {
          action: 'delete_sync', // load function hooked to: "wp_ajax_*" action hook
          id,
        }, success: function (response) {

        },
        error: function () {
          alert("error")
        },
        complete: function () {
          setLoading('.active-syncs', false)
        }
      })
    });
  }

  fetchSpecificSync(e) {
    jQuery(function( $ ) {
      const id = $(e.target).data('id')

      $(e.target).prop('disabled', true)
      return $.ajax({
        type: 'POST',
        url: 'admin-ajax.php',
        data: {
          action: 'fetch_specific_sync', // load function hooked to: "wp_ajax_*" action hook
          id,
        }, success: function (response) {
        },
        error: function () {
          alert("error")
        },
        complete: function () {
          $(e.target).prop('disabled', false)
        }
      })
    });
  }

  manualFetchEvents(e) {
    jQuery(function( $ ) {
      $(e.target).prop('disabled', true)
      $(e.target).val('Fetching...')

      return $.ajax({
        type: 'POST',
        url: 'admin-ajax.php',
        data: {
          action: 'manual_fetch_events', // load function hooked to: "wp_ajax_*" action hook
        }, success: function (response) {

        },
        error: function () {
          alert("error")
        },
        complete: function () {
          $(e.target).prop('disabled', false)
          $(e.target).val('Fetch events')
        }
      })
    });
  }
}

class SyncCreation {
  constructor() {
    this.data = {
      leagues: [],
      seasons: [],
    }
  }

  init = () => {
    this.getData()
  }

  getData() {
    jQuery(function( $ ) {
      let syncCreationForm = $('#sync-creation')

      setLoading('#sync-creation', true)

      $.ajax({
        type: 'GET',
        url: 'admin-ajax.php',
        data: {
          action: 'get_leagues', // load function hooked to: "wp_ajax_*" action hook
        }, success: result => {
          if (!result.data) return syncCreationForm.find('.league_select_block').html('Please synchronize league first.')

          const selectBox = generateSelectBox(
            'league_select',
            result.data,
            'Select league',
            {value: 'league_id', title: 'league_title'},
            [],
            false,
            'Select league'
          )

          syncCreationForm.find('.league_select_block').html(selectBox)
        },
        error: () => alert("error"),
        complete: function () {
          setLoading('#sync-creation', false)
        }
      })
    });
  }
}



jQuery(document).ready(function () {
  (new AdminSettings()).init()
})
