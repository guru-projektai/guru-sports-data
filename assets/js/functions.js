let format = 'decimal';

function decimalToFrac(decimal) {
    const tolerance = 1.0E-6;  // Tolerance level for the approximation
    let h1 = 1, h2 = 0;
    let k1 = 0, k2 = 1;
    let negative = decimal < 0;
    let b = decimal = Math.abs(decimal);

    do {
        let a = Math.floor(b);
        let aux = h1;
        h1 = a * h1 + h2;
        h2 = aux;
        aux = k1;
        k1 = a * k1 + k2;
        k2 = aux;
        b = 1 / (b - a);
    } while (Math.abs(decimal - h1 / k1) > decimal * tolerance);

    return (negative ? "-" : "") + h1 + '/' + k1;
}

function fracToDecimal(fractional) {
    let parts = fractional.split('/');
    if (parts.length === 2) {
        let numerator = parseFloat(parts[0]);
        let denominator = parseFloat(parts[1]);
        if (!isNaN(numerator) && !isNaN(denominator) && denominator !== 0) {
            return numerator / denominator;
        }
    }
    return NaN; // Invalid input
}

function updateFormat() {
    let summaryText = (format === 'decimal' || format === '') ? 'Decimal' : (format === 'fractional' ? 'Fractional' : '');
    document.querySelector('summary#summary_format div').textContent = summaryText;
    convertNumbers();
}

function convertNumbers() {
    let lines = document.querySelectorAll('.line');
    let number;
    lines.forEach(function(line) {
        let numberText = line.textContent.trim();
        let number = parseFloat(numberText);
        if (!isNaN(number)) { // Check if conversion was successful
            if (format === 'fractional') {
                number = decimalToFrac(number);
            } else {
                // Revert fractional number back to decimal
                number = fracToDecimal(numberText);
            }
            line.textContent = number;
        }
    });
}


document.addEventListener('DOMContentLoaded', function() {

    document.getElementById('decimal').addEventListener('click', function (event) {
        event.preventDefault();
        format = 'decimal';
        updateFormat(format);
    });

    document.getElementById('fractional').addEventListener('click', function (event) {
        event.preventDefault();
        format = 'fractional';
        updateFormat(format);
    });
});