const gulp = require('gulp');
const plumber = require('gulp-plumber');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const groupmq = require('gulp-group-css-media-queries');
const bs = require('browser-sync');
const csso = require('gulp-csso');
const concat = require('gulp-concat')
//const gutil = require('gulp-util');
const uglify = require('gulp-uglify');
const babel = require('gulp-babel');
const sass = require('gulp-sass')(require('sass'));

const SASS_SOURCES = [
  './assets/scss/*.scss',
];

/**
 * Compile Sass files
 */
gulp.task('compile', function(){
  return gulp.src(SASS_SOURCES)
    .pipe(plumber())
    .pipe(sass({
      indentType: 'tab',
      indentWidth: 1,
      outputStyle: 'expanded',
    })).on('error', sass.logError)
    .pipe(postcss([
      autoprefixer()
    ]))
    .pipe(groupmq()) // Group media queries!
    .pipe(csso())
    .pipe(gulp.dest('./assets/dist/'))
    .pipe(bs.stream())}); // Stream to browserSync

gulp.task('scripts', function(){
  return gulp.src('./assets/js/*.js')
    .pipe(babel())
    .pipe(uglify())
    .pipe(concat('compiled.js'))
    //.on('error', function (err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
    .pipe(gulp.dest('./assets/dist/js'))
});


/**
 * Default task executed by running `gulp`
 */
gulp.task('default', gulp.series('compile','scripts'));
