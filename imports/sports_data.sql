-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 07, 2023 at 04:48 PM
-- Server version: 10.2.31-MariaDB-log
-- PHP Version: 7.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

-- --------------------------------------------------------

--
-- Table structure for table `wp_guru_sports_data_events`
--

DROP TABLE IF EXISTS `wp_guru_sports_data_events`;
CREATE TABLE `wp_guru_sports_data_events` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `event_id` varchar(255) DEFAULT NULL,
  `league_id` decimal(12,0) DEFAULT NULL,
  `season_id` decimal(12,0) DEFAULT NULL,
  `league_title` varchar(255) DEFAULT NULL,
  `season_title` varchar(255) DEFAULT NULL,
  `data` longtext DEFAULT NULL,
  `date` decimal(12,0) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `home_team` varchar(255) DEFAULT NULL,
  `away_team` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_guru_sports_data_leagues`
--

DROP TABLE IF EXISTS `wp_guru_sports_data_leagues`;
CREATE TABLE `wp_guru_sports_data_leagues` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `league_id` decimal(12,0) DEFAULT NULL,
  `league_title` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_guru_sports_data_league_season`
--

DROP TABLE IF EXISTS `wp_guru_sports_data_league_season`;
CREATE TABLE `wp_guru_sports_data_league_season` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `league_id` decimal(12,0) DEFAULT NULL,
  `season_id` decimal(12,0) DEFAULT NULL,
  `season_title` varchar(255) DEFAULT NULL,
  `league_title` varchar(255) DEFAULT NULL,
  `start_date` decimal(12,0) DEFAULT NULL,
  `end_date` decimal(12,0) DEFAULT NULL,
  `current` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_guru_sports_data_syncs`
--

DROP TABLE IF EXISTS `wp_guru_sports_data_syncs`;
CREATE TABLE `wp_guru_sports_data_syncs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `league_id` decimal(12,0) DEFAULT NULL,
  `league_title` varchar(255) DEFAULT NULL,
  `season_id` decimal(12,0) DEFAULT NULL,
  `season_title` varchar(255) DEFAULT NULL,
  `start_date` decimal(12,0) DEFAULT NULL,
  `end_date` decimal(12,0) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `home_team` varchar(255) DEFAULT NULL,
  `away_team` varchar(255) DEFAULT NULL,
  `period` varchar(255) DEFAULT NULL,
  `active` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_pods_guru_sports_data_events`
--
ALTER TABLE `wp_guru_sports_data_events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_pods_guru_sports_data_leagues`
--
ALTER TABLE `wp_guru_sports_data_leagues`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_pods_guru_sports_data_league_season`
--
ALTER TABLE `wp_guru_sports_data_league_season`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_pods_guru_sports_data_syncs`
--
ALTER TABLE `wp_guru_sports_data_syncs`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_pods_guru_sports_data_events`
--
ALTER TABLE `wp_guru_sports_data_events`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_pods_guru_sports_data_leagues`
--
ALTER TABLE `wp_guru_sports_data_leagues`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_pods_guru_sports_data_league_season`
--
ALTER TABLE `wp_guru_sports_data_league_season`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_pods_guru_sports_data_syncs`
--
ALTER TABLE `wp_guru_sports_data_syncs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;