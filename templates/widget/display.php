<?php 

// User IP country info.
if (GURU_SPORTS_DATA_IS_SBG_SITE) {
    $country_name = sbg_betting_trackers_get_user_country();
    $country_id = sbg_get_country_id_from_name($country_name);
    $country_code = sbg_get_country_code_from_id($country_id);
    $country_code_flag = strtolower($country_code);

    // Bonus country info.
    $country_ids = sbg_get_country_ids_based_on_bonus_country($country_name);
    $bonus_country_id = !empty($country_ids) ? $country_ids['country_id'] : FALSE;
    $all_countries_country_id = sbg_get_country_id_from_name('All Countries');

    $euro_2021_sport_type_id = sbg_get_bookmaker_sport_type_id_from_name('Euro2021 odds');
    $euro_2021_promo_sport_type_id = sbg_get_bookmaker_sport_type_id_from_name('Euro2021 promo');
    $euro_2021_special_promo_sport_type_id = sbg_get_bookmaker_sport_type_id_from_name('Euro2021 special promo');
}

if (GURU_SPORTS_DATA_IS_LG_SITE) {
    $euro_2021_sport_type_id = sbg_get_bookmaker_sport_type_id_from_name('Euro2021 odds');
    $euro_2021_promo_sport_type_id = sbg_get_bookmaker_sport_type_id_from_name('Euro2021 promo');
    $euro_2021_special_promo_sport_type_id = sbg_get_bookmaker_sport_type_id_from_name('Euro2021 special promo');
}

$day_today = date('d');
$day_plus_1 = $day_today + 1;
$day_plus_1 = str_pad($day_plus_1, 2, 0, STR_PAD_LEFT);

// $current_day = date('Y') . '-' . date('m') . '-' . date('d') . ' 00:00:00';
$next_day = date('Y') . '-' . date('m') . '-' . $day_plus_1 . ' 00:00:00';

$current_day_timestamp = current_time('timestamp') + (get_option( 'gmt_offset' ) * HOUR_IN_SECONDS);
$current_day = date('Y-m-d 00:00:00', $current_day_timestamp);

// args
$args = array(
    'numberposts'   => 1,
    'post_type'     => 'free_bet_offer',
    'meta_query'    => array(
        'relation'      => 'AND',
        array(
            'key'       => 'free_bet_start_date_time',
            'value'     => $current_day,
            'compare'   => '='
        ),
        // array(
        //     'key'       => 'free_bet_expiration_date_time',
        //     'value'     => $next_day,
        //     'compare'   => '='
        // )
    )
);


// query
$the_query = new WP_Query( $args );

// d($the_query);

// News Cat ID by category.
$euro_2021_cat_id = get_cat_ID('Euro 2021');
$football_cat_id = get_cat_ID('Football');
$futbolas_cat_id = get_cat_ID('Futbolas');

if (!empty($euro_2021_cat_id)) {
    $news_cat_id = $euro_2021_cat_id;
} else if (!empty($football_cat_id) && empty($euro_2021_cat_id)) {
    $news_cat_id = $football_cat_id;
} else if (!empty($futbolas_cat_id) && empty($euro_2021_cat_id) && empty($football_cat_id)) {
    $news_cat_id = $futbolas_cat_id;
} else {
    $news_cat_id = 0;
}


// args
$args = array(
    'posts_per_page' => 3,
    'cat'           => $news_cat_id,
    'post_type'     => 'post',
    'post__not_in'  => array(get_the_ID()),
);


// query
$news_query = new WP_Query( $args );

?>
<?php if( $the_query->have_posts() ): ?>
    <?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
        <?php
            $free_bet_text              = get_post_meta(get_the_ID(), 'free_bet_text', true);
            $expiration_date_time       = get_post_meta(get_the_ID(), 'free_bet_expiration_date_time', true);
            $block_background_color     = get_post_meta(get_the_ID(), 'block_background_color', true);
            $button_background_color    = get_post_meta(get_the_ID(), 'button_background_color', true);
            $button_text                = get_post_meta(get_the_ID(), 'button_text', true);
            $button_text_color          = get_post_meta(get_the_ID(), 'button_text_color', true);
            $post_id                    = get_the_ID();
            global $wpdb; 
            $bookmaker_id = $wpdb->get_results("SELECT bookmaker_id FROM wp_pods_free_bet_offer WHERE id = $post_id");
            $bookmaker_id = !empty($bookmaker_id) ? (int) reset($bookmaker_id)->bookmaker_id : FALSE;

            $bookmaker_info_id = (int) sbg_betting_trackers_get_bookmaker_info_id($bookmaker_id);
            $bookmaker_logo_id = (int) sbg_betting_trackers_get_bookmaker_logo_id($bookmaker_info_id);

            $bookmaker_logo = pods_image($bookmaker_logo_id, $size = 'list_crop_thumb', $default = 0, array(), $force = false);

            $free_bet_bookmaker_link = !empty($euro_2021_special_promo_sport_type_id) ? sbg_betting_trackers_get_affiliate_link($bookmaker_id, $euro_2021_special_promo_sport_type_id, $bonus_country_id) : FALSE;
            $free_bet_bookmaker_link = empty($free_bet_bookmaker_link) ? sbg_betting_trackers_get_affiliate_link($bookmaker_id, $euro_2021_special_promo_sport_type_id, $all_countries_country_id) : $free_bet_bookmaker_link;

            if (GURU_SPORTS_DATA_IS_LG_SITE) {
                $free_bet_bookmaker_link = !empty($euro_2021_special_promo_sport_type_id) ? sbg_betting_trackers_get_affiliate_link($bookmaker_id, $euro_2021_special_promo_sport_type_id) : FALSE;
            }

        ?>
    <?php endwhile; ?>
<?php endif; ?>

<?php wp_reset_query();  // Restore global post data stomped by the_post(). ?>

<?php

// TOP bookmaker for Football.
if (GURU_SPORTS_DATA_IS_SBG_SITE) {
    $sport_type = 'Football';
    $sport_type_id = sbg_get_bookmaker_sport_type_id_from_name($sport_type);

    $extra_data = array();
    $extra_data['show_new'] = TRUE;
    $extra_data['display_type'] = FALSE;
    $extra_data['show_restricted'] = FALSE;
    $extra_data['crypto'] = FALSE;
    $extra_data['forced_country'] = FALSE;
    $extra_data['sbg_all_info'] = array();

    $bookmaker_suggestions_raw = sbg_betting_trackers_get_top_rating($sport_type, $country_name);

    // Remove current bookmaker from suggestions.
    if (!empty($bookmaker_suggestions_raw)) {
        foreach ($bookmaker_suggestions_raw as $key => $bookmaker) {
          if ($bookmaker_id == $bookmaker['bookmaker_id']) {
            unset($bookmaker_suggestions_raw[$key]);
          }
        }

        $bookmaker_suggestions_raw = array_values($bookmaker_suggestions_raw);

        if (!empty($bookmaker_suggestions_raw)) {
          if (!empty($sport_type_id)) {
            usort($bookmaker_suggestions_raw, function($a, $b) {
              if ($b['bookmaker_sport_type_review']['bookmaker_sport_review_rating'] == $a['bookmaker_sport_type_review']['bookmaker_sport_review_rating']) {
                // Rating is the same, sort by bookmaker name.
                if ($a['bookmaker_name'] > $b['bookmaker_name']) return 1;
              }

              // Sort the higher rating first.
              return $b['bookmaker_sport_type_review']['bookmaker_sport_review_rating'] <=> $a['bookmaker_sport_type_review']['bookmaker_sport_review_rating'];
            });
          } else {
            usort($bookmaker_suggestions_raw, function($a, $b) {
              if ($b['bookmaker_rating'] == $a['bookmaker_rating']) {
                // Rating is the same, sort by bookmaker name.
                if ($a['bookmaker_name'] > $b['bookmaker_name']) return 1;
              }

              // Sort the higher rating first.
              return $b['bookmaker_rating'] <=> $a['bookmaker_rating'];
            });
          }
        }
    }

    $bookmaker_suggestions = array_slice($bookmaker_suggestions_raw, 0, 3);

    $bookmaker_regulation_text = !empty($bookmaker_suggestions[0]) ? $bookmaker_suggestions[0]['bookmaker_review_regulation_text'] : FALSE;
}

// TOP bookmaker for Futbolas.
if (GURU_SPORTS_DATA_IS_LG_SITE) {
    // TODO
    $sport_branch = 'Futbolas';
    $top1 = TRUE;
    $bookmaker = guru_get_top_bookmakers($sport_branch, $top1);
    $bookmaker_suggestions = !empty($bookmaker) ? array($bookmaker) : FALSE;

    $bookmaker_regulation_text = !empty($bookmaker_suggestions[0]) ? $bookmaker_suggestions[0]['bookmaker_regulation_text'] : FALSE;

    // var_dump($bookmaker_suggestions);
}

?>


<div class="euro-2020-widget">
    <?php if (!empty($title)): ?>
        <div class="widget-title">
            <img class="header-logo european championship" src="/app/uploads/guru-sports-data/league-logos/1.png" alt="European Championship">
            <span><?= $title ?></span>
        </div>
    <?php else: ?>
        <div class="widget-title">
            <img class="header-logo european championship" src="/app/uploads/guru-sports-data/league-logos/1.png" alt="European Championship">
            <span>Kasdien EURO 2021 skirtos premijos!</span>
        </div>
    <?php endif; ?>

    <script type="text/javascript">
        jQuery(document).ready(function () {
            var expiration_date_time = "<?php print $expiration_date_time; ?>";

            // Transform expiration date time into universal 
            // format supported by all browsers.
            var exp_arr = expiration_date_time.split(/[- :]/);
            var exp_date = new Date(exp_arr[0], exp_arr[1]-1, exp_arr[2], exp_arr[3], exp_arr[4], exp_arr[5]);

            jQuery('.countdown').downCount({
                date: exp_date,
                offset: +3
            }, function () {

            });

            jQuery('.open-accepted-navigation').click(function() {
                jQuery('.euro-2020-widget-wrapper .free-bet-offer.accepted').toggleClass('hide');

                if (jQuery(this).find('i').hasClass('fa-chevron-down')) {
                    jQuery('.open-accepted-navigation i').removeClass('fa-chevron-down').addClass('fa-chevron-up');
                } else {
                    jQuery('.open-accepted-navigation i').removeClass('fa-chevron-up').addClass('fa-chevron-down');
                }
            });


            var screen_width = screen.width;

            if (screen_width < 1024) {
                // console.log('Screen width: ' + screen_width);

                jQuery('.euro-2020-widget-wrapper .open-accepted-navigation').removeClass('hide');

                jQuery('.euro-2020-widget-wrapper .free-bet-offer.accepted').addClass('hide');

                jQuery('.euro-2020-widget-wrapper .free-bet-offer.accepted').insertAfter(jQuery('.open-accepted-navigation'));

                return;
            }

            // Grab divs with the class name 'match-height'
            var getDivs = document.getElementsByClassName('match-height');

            // Find out how my divs there are with the class 'match-height' 
            var arrayLength = getDivs.length;
            var heights = [];

            // Create a loop that iterates through the getDivs variable and pushes the heights of the divs into an empty array
            for (var i = 0; i < arrayLength; i++) {
              heights.push(getDivs[i].offsetHeight);
            }

            // Find the largest of the divs
            function getHighest() {
                return Math.max(...heights);
            }

            // Set a variable equal to the tallest div
            var tallest = getHighest();
            // console.log('Tallest:' + tallest);

            // Iterate through getDivs and set all their height style equal to the tallest variable
            for (var i = 0; i < getDivs.length; i++) {
              getDivs[i].style.height = tallest + "px";
            }

        });

    </script>

    <div class="free-bet-offer match-height" style="background-color: <?php print $block_background_color; ?>;">
        <a href="<?php print $free_bet_bookmaker_link; ?>" target="_blank" rel="nofollow">
            <div class="free-bet-logo">
                <?php print $bookmaker_logo; ?>
            </div>
            <div class="free-bet-text">
                <?php print $free_bet_text; ?>
            </div>
            <div class="free-bet-expiration-time">

                <ul class="countdown">
                    <li>
                        <span class="hours">00</span>
                        <p class="hours_ref">hours</p>
                    </li>
                    <li class="seperator">:</li>
                    <li>
                        <span class="minutes">00</span>
                        <p class="minutes_ref">minutes</p>
                    </li>
                    <li class="seperator">:</li>
                    <li>
                        <span class="seconds">00</span>
                        <p class="seconds_ref">seconds</p>
                    </li>
                </ul>

            </div>
            <div class="free-bet-button" style="background-color: <?php print $button_background_color; ?>; color: <?php print $button_text_color; ?>;">
                <?php print $button_text; ?>
            </div>
            <div class="regulation-text">
                <?php print $bookmaker_regulation_text; ?>
            </div>
        </a>
    </div>

    <?php if (GURU_SPORTS_DATA_IS_SBG_SITE): ?>

        <div class="open-accepted-navigation hide">
            <div class="title">Can't open bookmaker?</div>
            <div class="button">
                <span>Another deal for you!</span>
                <i class="fas fa-chevron-down"></i></div>
        </div>

    <?php endif; ?>

    <?php if (GURU_SPORTS_DATA_IS_LG_SITE): ?>

        <div class="open-accepted-navigation hide">
            <div class="title">Kitas pasiūlymas Jums</div>
            <div class="button">
                <span>Atidaryti</span>
                <i class="fas fa-chevron-down"></i></div>
        </div>

    <?php endif; ?>

    <ul class="euro-2020-odds match-height">
        <?php print do_shortcode('[guru-sports-data league="European Championship" type="schedule"]'); ?>

        <?php if( $news_query->have_posts() ): ?>
            <div class="euro-2020-news">
                <div class="title">
                    <?php if (GURU_SPORTS_DATA_IS_SBG_SITE): ?>
                        Latest Euro 2021 News
                    <?php else: ?>
                        Euro 2021 naujienos
                    <?php endif; ?>
                </div>
                <ul>
                <?php while( $news_query->have_posts() ) : $news_query->the_post(); ?>
                    <?php
                        $post_title = get_the_title(get_the_ID());
                        $post_id    = get_the_ID();
                        $post_link  = get_permalink($post_id);
                        $post_date_time = get_post_time("Y/m/d H:i:s", true, $post_id);
                        $post_date  = strtotime(get_post_time("Y/m/d H:i:s", true, $post_id));
                        $post_ago   = human_time_diff($post_date, current_time('timestamp'));
                        $post_ago    = str_replace(array(' month', ' weeks', ' week', ' days', ' day', ' hours', ' minutes'), array('mo', 'w', 'w', 'd', 'd', 'h', 'm'), $post_ago);
                    ?>
                    <li>
                        <a href="<?php print $post_link; ?>">
                            <span class="posted-on"><?php print $post_ago; ?> ago</span><?php print $post_title; ?>
                        </a>
                    </li>
                <?php endwhile; ?>
                </ul>
            </div>
        <?php endif; ?>

        <?php wp_reset_query();  // Restore global post data stomped by the_post(). ?>
    </ul>

    <?php if (!empty($bookmaker_suggestions)): ?>
        <?php $count = 0; foreach ($bookmaker_suggestions as $bookmaker): ?>
            <?php

            if (GURU_SPORTS_DATA_IS_SBG_SITE) {

                $bookmaker_info_id = $bookmaker['bookmaker_info_id'];
                $bookmaker_logo_id = sbg_betting_trackers_get_bookmaker_logo_id($bookmaker_info_id);
                $bookmaker_logo = pods_image($bookmaker_logo_id, $size = 'list_crop_thumb', $default = 0, array(), $force = false);
                $bookmaker_button_text = $bookmaker['bookmaker_button_text'];
                $bookmaker_review_button_text = $bookmaker['bookmaker_review_button_text'];
                $bookmaker_review_regulation_text = $bookmaker['bookmaker_review_regulation_text'];
                $bookmaker_info_color = $bookmaker['bookmaker_info']['bookmaker_info_color'];
                $bookmaker_link = !empty($euro_2021_promo_sport_type_id) ? sbg_betting_trackers_get_affiliate_link($bookmaker['bookmaker_id'], $euro_2021_promo_sport_type_id, $bonus_country_id) : FALSE;

                $bookmaker_info_block_color = $bookmaker['bookmaker_info']['bookmaker_info_block_color'];

                $bookmaker_info_block_button_color = $bookmaker['bookmaker_info']['bookmaker_info_block_button_color'];
                $bookmaker_info_block_button_text_color = $bookmaker['bookmaker_info']['bookmaker_info_block_button_text_color'];

                $bookmaker_bg_color = !empty($bookmaker_info_block_color) ? $bookmaker_info_block_color : $bookmaker_info_color;
                $bookmaker_button_bg_color = $bookmaker_info_block_button_color;
                $bookmaker_button_text_color = $bookmaker_info_block_button_text_color;

            }

            if (GURU_SPORTS_DATA_IS_LG_SITE) {
                $bookmaker_info_id = $bookmaker['bookmaker_info_id'];
                $bookmaker_logo_id = sbg_betting_trackers_get_bookmaker_logo_id($bookmaker_info_id);
                $bookmaker_logo = pods_image($bookmaker_logo_id, $size = 'list_crop_thumb', $default = 0, array(), $force = false);
                $bookmaker_button_text = $bookmaker['bookmaker_info_button_text_list'];
                $bookmaker_review_button_text = $bookmaker['bookmaker_button_text'];
                $bookmaker_review_regulation_text = $bookmaker['bookmaker_regulation_text'];

                $bookmaker_info_color = $bookmaker['bookmaker_info_color'];
                $bookmaker_link = !empty($euro_2021_promo_sport_type_id) ? sbg_betting_trackers_get_affiliate_link($bookmaker['bookmaker_id'], $euro_2021_promo_sport_type_id) : FALSE;
                $bookmaker_link = !empty($bookmaker_link) ? $bookmaker_link : $bookmaker['bookmaker_link'];

                $bookmaker_info_block_color = $bookmaker['bookmaker_info_block_color'];

                $bookmaker_info_block_button_color = $bookmaker['bookmaker_info_block_button_color'];
                $bookmaker_info_block_button_text_color = $bookmaker['bookmaker_info_block_button_text_color'];

                $bookmaker_bg_color = !empty($bookmaker_info_block_color) ? $bookmaker_info_block_color : $bookmaker_info_color;
                $bookmaker_button_bg_color = $bookmaker_info_block_button_color;
                $bookmaker_button_text_color = $bookmaker_info_block_button_text_color;
            }

            ?>
            <div class="free-bet-offer accepted match-height" style="background-color: <?php print $bookmaker_bg_color; ?>;">
                <a href="<?php print $bookmaker_link; ?>" target="_blank" rel="nofollow">
                    <div class="free-bet-logo">
                        <?php print $bookmaker_logo; ?>
                    </div>
                    <div class="free-bet-text">
                        <?php print $bookmaker_button_text; ?>
                    </div>

                    <?php if (GURU_SPORTS_DATA_IS_SBG_SITE): ?>

                      <div class="accepting-players">
                        <span><i class="fa fa-check" aria-hidden="true"></i> Accepting players from<br /><img src="/app/themes/landx/images/flags/svg-flaticon-cropped/<?php print $country_code_flag; ?>.svg" alt="Country flag" /></span> <?php print $country_name; ?></span>
                      </div>

                    <?php endif; ?>

                    <?php if (GURU_SPORTS_DATA_IS_LG_SITE): ?>

                      <div class="accepting-players">
                        <span><i class="fa fa-check" aria-hidden="true"></i> Priimami žaidėjai iš<br /><img src="https://smartbettingguide.com/app/themes/landx/images/flags/svg-flaticon-cropped/lt.svg" alt="Country flag" /></span>Lietuvos</span>
                      </div>

                    <?php endif; ?>

                    <div class="free-bet-button" style="background-color: <?php print $bookmaker_button_bg_color; ?>; color: <?php print $bookmaker_button_text_color; ?>;">
                        <?php print $bookmaker_review_button_text; ?>
                    </div>
                    <div class="regulation-text">
                        <?php print $bookmaker_review_regulation_text; ?>
                    </div>
                </a>
            </div>
        <?php $count++; if ($count == 1) break; endforeach; ?>
    <?php endif; ?>

</div>