<p>
    <label for="<?= esc_attr($this->get_field_name('title')) ?>">
        <?php esc_attr_e( 'Title:', 'text_domain' ); ?>
    </label>
    <input type="text"
           class="widefat"
           id="<?= esc_attr($this->get_field_name('title')) ?>"
           name="<?= esc_attr($this->get_field_name('title')) ?>"
           value="<?= esc_attr($title) ?>"
    />

    <br />

    <label for="<?= esc_attr($this->get_field_name('template')) ?>">
        <?php esc_attr_e( 'Template:', 'text_domain' ); ?>
    </label>

    <select id="<?php echo $this->get_field_id('template'); ?>" name="<?php echo $this->get_field_name('template'); ?>" class="widefat">
        <?php foreach($list_templates as $entry) { ?>
            <option <?php selected($template, $entry); ?> value="<?php echo $entry; ?>"><?php echo $entry; ?></option>
        <?php } ?>      
    </select>
</p>