<?php
$upload_dir = wp_upload_dir();

$team_logo_dir   = $upload_dir['baseurl'] . '/guru-sports-data/team-logos';
$league_logo_dir = $upload_dir['baseurl'] . '/guru-sports-data/league-logos';
$events          = [];
$season          = [];
if ( ! empty( $data ) ) {

	$events = $data['events'] ?: [];
	$season = $data['season'][0];

}

//images

$calendar = plugin_dir_url( __DIR__ ) . 'assets/images/calendar.svg';
?>


<div class="py-4 score-collection <?php if ( $type === 'schedule' ) {
	echo 'schedule-type ';
} else {
	echo 'results-type';
} ?> <?php if ( is_single() ) {
	echo 'single-page';
} ?>">
    <input type="hidden" class="leagueName" value="<?= $league_name ?>"/>
    <input type="hidden" class="seasonName" value="<?= $season_name ?: 'false' ?>"/>
    <input type="hidden" class="limit" value="<?= $limit ?>"/>
    <input type="hidden" class="order" value="<?= $order ?>"/>
    <input type="hidden" class="type" value="<?= $type ?>"/>
    <input type="hidden" class="team1" value="<?= $team1 ?: 'false' ?>"/>
    <input type="hidden" class="team2" value="<?= $team2 ?: 'false' ?>"/>

    <div class="score-collection__header mb-4">
        <div class="score-collection__header__content flex items-start sm:justify-between sm:items-center flex-col sm:flex-row ">
            <div class="score-collection-title mb-2 sm:mb-0">
                <h3 class="header-title"><?= $title ?></h3>
            </div>
            <div class="flatpickr-date-select relative">
                <div class="loader-input-block flex items-center">
                    <input type="text" class="flatpickr-sport-data bg-transparent outline-0 text-left sm:text-right w-[140px] mr-2" data-input placeholder="<?= get_option('choose_date__guru'); ?>">
                    <div class="loader-input absolute top-2"></div>
                </div>

            </div>
        </div>
    </div>

    <div class="scaffold-div"></div>
    <!-- RESULTS TEMPLATE -->
    <div class="score-item results scaffold-results" style="display: none">
        <div class="score-item__content">
            <div class="score-label hidden"></div>
            <div class="score-item__wrapper">
                <div class="items-center rounded-2xl bg-white px-4 py-3 mx-auto flex flex-col sm:flex-row ">
                    <div class="flex flex-1 justify-between mb-2 sm:mb-0 w-full score-item-home ">
                        <div class="flex items-center">
                            <div class="item-block-image w-[32px] mr-2">
                                <img src="" alt="" class="!w-full">
                            </div>
                            <div class="item-block-title flex-1">
                                <div class="title-name text-base md:text-lg !leading-5 min-h-[40px] flex items-center"></div>
                            </div>
                        </div>
                        <div class="sm:hidden score-results w-auto flex items-center py-1 px-3 bg-neutral-100 rounded-lg min-w-[40px] justify-center">
                            <div class="home-score text-black font-bold "></div>
                        </div>
                    </div>
                    <div class="hidden sm:flex score-results w-auto items-center px-3 bg-neutral-100 rounded-lg">
                        <div class="home-score text-black font-bold ml-auto"></div>
                        <div class="text-center text-black-500 px-0.5">:</div>
                        <div class="away-score mr-auto text-black font-bold"></div>
                    </div>
                    <div class="flex flex-1 flex-row justify-between sm:justify-end w-full score-item-away ">
                        <div class="flex items-center">
                            <div class="item-block-title flex-1 order-2 sm:order-1">
                                <div class="title-name text-base md:text-lg !leading-5 min-h-[40px] flex items-center sm:justify-end sm:text-right"></div>
                            </div>
                            <div class="item-block-image w-[32px] mr-2 sm:mr-2 ml-0 sm:ml-2 order-1 sm:order-2">
                                <img src="" alt="" class="!w-full">
                            </div>
                        </div>
                        <div class="sm:hidden score-results w-auto flex items-center px-3 bg-neutral-100 rounded-lg min-w-[40px] justify-center">
                            <div class="away-score text-black font-bold"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- / RESULTS TEMPLATE -->

    <!-- SCHEDULE TEMPLATE -->

    <div class="score-item scaffold " style="display: none;">
        <div class="score-item__content sm:items-center rounded-2xl bg-white px-4 py-3 mx-auto flex">
            <input type="hidden" class="home-team-name" value=""/>
            <input type="hidden" class="away-team-name" value=""/>
            <input type="hidden" class="event-timestamp" value=""/>
            <div class="flex flex-1 flex-col sm:flex-row sm:items-center">
                <div class="score-item-home flex-1 mb-2 sm:mb-0">
                    <div class="item-block flex items-center">
                        <div class="item-block-image w-[32px] mr-2">
                            <img src="" alt="" class="!w-full">
                        </div>
                        <div class="item-block-title flex-1">
                            <div class="title-name text-base md:text-lg !leading-5 min-h-[40px] flex items-center"></div>
                        </div>
                    </div>
                    <div class="odd-home odd hide">
                        <a href="#" rel="nofollow" target="_blank">
                            <div class="bookmaker"></div>
                            <div class="odd-text">
                                <div class="text"></div>
                                <div class="line"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="w-[40px] text-center text-xs hidden sm:block">&#9587;</div>
                <div class="score-item-away flex-1">
                    <div class="item-block flex items-center">
                        <div class="item-block-image w-[32px] mr-2">
                            <img src="" alt="" class="!w-full"/>
                        </div>
                        <div class="item-block-title flex-1">
                            <div class="title-name text-base md:text-lg !leading-5 min-h-[40px] flex items-center"></div>
                        </div>
                    </div>
                    <div class="odd-away odd hide">
                        <a href="#" rel="nofollow" target="_blank">
                            <div class="bookmaker"></div>
                            <div class="odd-text">
                                <div class="text"></div>
                                <div class="line"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="score-item-block w-auto text-right ml-auto self-center">
                <span class="time-block bg-neutral-100 rounded-lg font-bold px-3 py-2"></span>
                <span class="home-score"></span>
                <span class="away-score"></span>
                <div class="odd-draw odd hide">
                    <a href="#" rel="nofollow" target="_blank">
                        <div class="bookmaker"></div>
                        <div class="odd-text">
                            <div class="text"></div>
                            <div class="line"></div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <!-- /SCHEDULE TEMPLATE -->

    <div class="loader-block">
        <div class="loader">
            <div class="animate-pulse grid sm:grid-cols-1 md:grid-cols-2 gap-4">
                <div>
                    <div class="items-center rounded-2xl bg-white px-4 py-3 mx-auto flex flex-row">
                        <div class="flex-1">
                            <div class="flex items-center h-[40px]">
                                <div class="w-[32px] mr-2 rounded-full bg-slate-200 h-[32px]"></div>
                                <div class="item-block-title flex-1 grid grid-cols-1 gap-2">
                                    <div class="h-[10px] flex items-center bg-slate-200 w-auto rounded-xl"></div>
                                    <div class="h-[10px] flex items-center bg-slate-200 w-auto rounded-xl"></div>
                                </div>
                            </div>
                        </div>
                        <div class="w-[40px] text-center text-xs"></div>
                        <div class="flex-1">
                            <div class="flex items-center h-[40px]">
                                <div class="w-[32px] mr-2 rounded-full bg-slate-200 h-[32px]"></div>
                                <div class="item-block-title flex-1 grid grid-cols-1 gap-2">
                                    <div class="h-[10px] flex items-center bg-slate-200 w-auto rounded-xl"></div>
                                    <div class="h-[10px] flex items-center bg-slate-200 w-auto rounded-xl"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="hidden md:block">
                    <div class="items-center rounded-2xl bg-white px-4 py-3 mx-auto flex flex-row">
                        <div class="flex-1">
                            <div class="flex items-center h-[40px]">
                                <div class="w-[32px] mr-2 rounded-full bg-slate-200 h-[32px]"></div>
                                <div class="item-block-title flex-1 grid grid-cols-1 gap-2">
                                    <div class="h-[10px] flex items-center bg-slate-200 w-auto rounded-xl"></div>
                                    <div class="h-[10px] flex items-center bg-slate-200 w-auto rounded-xl"></div>
                                </div>
                            </div>
                        </div>
                        <div class="w-[40px] text-center text-xs"></div>
                        <div class="flex-1">
                            <div class="flex items-center h-[40px]">
                                <div class="w-[32px] mr-2 rounded-full bg-slate-200 h-[32px]"></div>
                                <div class="item-block-title flex-1 grid grid-cols-1 gap-2">
                                    <div class="h-[10px] flex items-center bg-slate-200 w-auto rounded-xl"></div>
                                    <div class="h-[10px] flex items-center bg-slate-200 w-auto rounded-xl"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="no-events rounded-2xl bg-white px-4 py-3 text-center flex items-center justify-center min-h-[64px]"
         style="display: none">
		<?= $no_events_text ?>
    </div>
    <div class="score-collection__content grid md:grid-cols-1 lg:grid-cols-2 gap-4"></div>
    <button class="expand btn" style="display: none">
        <span class="button-label"><?= $show_more_text ?></span>
        <span class="icon"><i class="fas fa-chevron-down"></i></span>
    </button>
</div>

<script type="text/javascript">

    // wp_localize_script('guru_sports_data', 'is_sbg_site', GURU_SPORTS_DATA_IS_SBG_SITE);
    // wp_localize_script('guru-sports_data', 'is_lg_site', GURU_SPORTS_DATA_IS_LG_SITE);

</script>


<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function () {
        var is_sbg_site = '<?= GURU_SPORTS_DATA_IS_SBG_SITE ?>';
        var is_lg_site = '<?= GURU_SPORTS_DATA_IS_LG_SITE ?>';

        function searchTeamName(key) {
            if (is_lg_site == 1) {
                let inputArray = [
                    {name: "Austria", name_lt: "Austrija"},
                    {name: "Belgium", name_lt: "Belgija"},
                    {name: "Croatia", name_lt: "Kroatija"},
                    {name: "Czech Republic", name_lt: "Čekija"},
                    {name: "Denmark", name_lt: "Danija"},
                    {name: "England", name_lt: "Anglija"},
                    {name: "Finland", name_lt: "Suomija"},
                    {name: "France", name_lt: "Prancūzija"},
                    {name: "Germany", name_lt: "Vokietija"},
                    {name: "Hungary", name_lt: "Vengrija"},
                    {name: "Italy", name_lt: "Italija"},
                    {name: "Netherlands", name_lt: "Olandija"},
                    {name: "North Macedonia", name_lt: "Šiaurės Makedonija"},
                    {name: "Poland", name_lt: "Lenkija"},
                    {name: "Portugal", name_lt: "Portugalija"},
                    {name: "Russia", name_lt: "Rusija"},
                    {name: "Scotland", name_lt: "Škotija"},
                    {name: "Slovakia", name_lt: "Slovakija"},
                    {name: "Spain", name_lt: "Ispanija"},
                    {name: "Sweden", name_lt: "Švedija"},
                    {name: "Switzerland", name_lt: "Šveicarija"},
                    {name: "Turkey", name_lt: "Turkija"},
                    {name: "Ukraine", name_lt: "Ukraina"},
                    {name: "Wales", name_lt: "Velsas"},
                ];

                for (let i = 0; i < inputArray.length; i++) {
                    if (inputArray[i].name === key) {
                        return inputArray[i].name_lt;
                    }
                }
            }

            return key;
        }

        class DatePicker {
            datepickerInstance = null
            showMoreText = ''
            showLessText = ''

            constructor(classInstance = this) {
                this.showMoreText = '<?= $show_more_text ?>';
                this.showLessText = '<?= $show_less_text ?>';
                this.datepickerInstance = flatpickr('.flatpickr-sport-data', {
                    enableTime: false,
                    defaultDate: new Date(),
                    onReady: function (selectedDate, time, calendar) {
                        let parent = calendar.input.closest('.score-collection');
                        const type = parent.querySelector('.type').value;
                        if (type === 'results') {
                            calendar.set('maxDate', new Date());
                            classInstance.toggleDateArrows(time, parent);
                        }
                    },
                    onChange: function (item, val, calendar) {
                        let parent = calendar.input.closest('.score-collection');
                        const type = parent.querySelector('.type').value;
                        let startDate = new Date(item);
                        startDate.setHours(0, 0, 0, 0);
                        let endDate = new Date(item);
                        endDate.setHours(23, 59, 59, 999);
                        parent.querySelector('.no-events').style.display = 'none';
                        const expandElement = parent.querySelector('.expand');
                        const fasElement = expandElement.querySelector('.fas');
                        fasElement.classList.remove('fa-chevron-up');
                        fasElement.classList.add('fa-chevron-down');
                        expandElement.classList.remove('expanded');
                        expandElement.querySelector('.button-label').innerHTML = classInstance.showMoreText;
                        if (type === 'results') {
                            classInstance.toggleDateArrows(val, parent);
                        }

                        if (type == 'schedule') {
                            // Unslick before changing date.
                            document.querySelectorAll('.euro-2020-odds .score-collection__content').forEach(function (element) {
                                if (element.classList.contains('slick-initialized')) {
                                    $(element).slick('unslick');
                                }
                            });
                        }

                        classInstance.call(parent, startDate / 1000, endDate / 1000);

                        // alert('FINAL');
                    }
                });
                // document.querySelectorAll('.date-arrow').forEach(function (arrow) {
                //     arrow.addEventListener('click', function () {
                //         const parent = arrow.closest('.score-collection');
                //         if (parent.classList.contains('loading-data')) return;
                //         const action = arrow.getAttribute('data-action') === 'previous' ? -1 : 1;
                //         const datepicker = parent.querySelector('.flatpickr-sport-data')._flatpickr;
                //         const newDate = new Date(datepicker.selectedDates[0]);
                //         newDate.setDate(newDate.getDate() + action);
                //         datepicker.setDate(newDate, true);
                //     });
                // });
                document.querySelectorAll('.input-button-datepicker').forEach(function (button) {
                    button.addEventListener('click', function () {
                        button.closest('.score-collection').querySelector('.flatpickr-sport-data')._flatpickr.open();
                    });
                });
                document.querySelectorAll('.expand').forEach(function (expand) {
                    expand.addEventListener('click', function () {
                        expand.classList.toggle('expanded');
                        expand.querySelector('.button-label').innerHTML = expand.classList.contains('expanded') ? classInstance.showLessText : classInstance.showMoreText;
                        expand.querySelector('.fas').classList.toggle('fa-chevron-down');
                        expand.querySelector('.fas').classList.toggle('fa-chevron-up');
                        expand.closest('.score-collection').querySelectorAll('.collapsible').forEach(function (item) {
                            item.classList.toggle('expanded');
                            item.style.display = item.style.display === 'none' ? '' : 'none';
                        });
                    });
                });
                // Initial call
                document.querySelectorAll('.flatpickr-sport-data:not(.initialized)').forEach(function (flatpickrElement) {
                    let parent = flatpickrElement.closest('.score-collection');
                    let startDate = new Date();
                    startDate.setHours(0, 0, 0, 0);
                    let endDate = new Date();
                    endDate.setHours(23, 59, 59, 999);
                    flatpickrElement.classList.add('initialized');
                    classInstance.call(parent, startDate / 1000, endDate / 1000, classInstance);
                });
            }

            toggleDateArrows(time, parent) {
                let now = new Date();
                now.setHours(0, 0, 0, 0);
                let parsedNow = Math.round(now.getTime() / 1000);
                let dateParsed = new Date(time);
                dateParsed.setHours(0, 0, 0, 0);
                let date = Math.round(dateParsed.getTime() / 1000);
                // if (date === parsedNow) {
                //     parent.querySelector('.date-next').style.visibility = 'hidden';
                // } else {
                //     parent.querySelector('.date-next').style.visibility = 'visible';
                // }
            }

            async getEarliestEventDate(parent, startDate, classInstance) {
                let instance = classInstance.datepickerInstance;
                if (classInstance.datepickerInstance.length > 1) {
                    instance = classInstance.datepickerInstance.find(function (instance) {
                        return instance.input.closest('.score-collection') === parent;
                    });
                }
                let input = instance.input;
                let context = input._flatpickr;
                if (input.classList.contains('reinitialized')) return;
                input.classList.add('reinitialized');
                input.setAttribute('disabled', 'disabled');
                parent.querySelector('.loader-input-block .loader-input').style.display = 'block';
                parent.classList.add('loading-data');

                try {
                    const response = await fetch('<?= admin_url( "admin-ajax.php" ) ?>', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded',
                        },
                        body: new URLSearchParams({
                            action: 'get_earliest_event_date',
                            league: parent.querySelector('.leagueName').value,
                            season: parent.querySelector('.seasonName').value,
                            limit: parent.querySelector('.limit').value,
                            order: parent.querySelector('.order').value,
                            type: parent.querySelector('.type').value,
                            team1: parent.querySelector('.team1').value,
                            team2: parent.querySelector('.team2').value,
                            startDate: startDate,
                        }),
                    });

                    if (response.ok) {
                        const jsonResponse = await response.json();
                        if (jsonResponse.data) {
                            context.setDate(new Date(jsonResponse.data.date * 1000), true);
                        } else {
                            parent.querySelector('.score-collection__content').innerHTML = '';
                            parent.querySelector('.no-events').style.display = 'block';
                        }
                    }
                } catch (error) {
                    console.error('Error:', error);
                } finally {
                    parent.querySelector('.loader-input-block .loader-input').style.display = 'none';
                    input.removeAttribute('disabled');
                    parent.classList.remove('loading-data');
                }
            }

            async getEventOdds(home_team, away_team, event_timestamp) {
                let instance = classInstance.datepickerInstance;
                if (classInstance.datepickerInstance.length > 1) {
                    instance = classInstance.datepickerInstance.find(function (instance) {
                        return instance.input.closest('.score-collection') === parent;
                    })
                }

                try {
                    const response = await fetch('<?= admin_url( "admin-ajax.php" ) ?>', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded',
                        },
                        body: new URLSearchParams({
                            action: 'guru_sports_data_get_single_event_odds',
                            home_team: home_team,
                            away_team: away_team,
                            event_timestamp: event_timestamp,
                        }),
                    });

                    const data = await response.json();

                    // Uncomment and modify the following lines according to your needs
                    // if (data.data) {
                    //     console.log(data.data);
                    //     context.setDate(new Date(data.data.date * 1000), true)
                    // } else {
                    //     parent.querySelector('.score-collection__content').innerHTML = '';
                    //     parent.querySelector('.no-events').style.display = 'block';
                    // }

                } catch (error) {
                    console.error('Error:', error);
                } finally {
                    // Uncomment and modify the following lines according to your needs
                    // input.closest('.loader-input-block').querySelector('.loader-input').style.display = 'none';
                    // input.removeAttribute('disabled');
                    // parent.classList.remove('loading-data');
                }
            }

            renderResults(parent, response) {
                let collection = parent.querySelector('.scaffold-div').cloneNode(true);
                response.data.forEach(function (item, key) {
                    const data = JSON.parse(item.data);
                    let clonedItem = parent.querySelector('.score-item.scaffold-results').cloneNode(true);

                    let home_team = searchTeamName(item.home_team);
                    let away_team = searchTeamName(item.away_team);

                    clonedItem.querySelector('.score-item-home .title-name').innerHTML = home_team;
                    clonedItem.querySelector('.score-item-away .title-name').innerHTML = away_team;
                    clonedItem.querySelector('.score-item__content .home-team-name').value = item.home_team;
                    clonedItem.querySelector('.score-item__content .away-team-name').value = item.away_team;
                    const imageSlug = data.sport ? data.sport.slug : data.tournament.uniqueTournament.category.sport.slug;
                    let imageHomeUrl = '<?= $team_logo_dir . "/" ?>' + data.homeTeam.id + '.png';
                    fetch(imageHomeUrl)
                        .then(function (response) {
                            if (response.ok) {
                                clonedItem.querySelector('.score-item-home img').src = imageHomeUrl;
                            } else {
                                throw new Error('Image not found');
                            }
                        })
                        .catch(function () {
                            imageHomeUrl = '<?= plugin_dir_url( __DIR__ ) . "assets/images/football.png"?>';
                            clonedItem.querySelector('.score-item-home img').src = imageHomeUrl;
                        });

                    let imageAwayUrl = '<?= $team_logo_dir . "/" ?>' + data.awayTeam.id + '.png';
                    fetch(imageAwayUrl)
                        .then(function (response) {
                            if (response.ok) {
                                clonedItem.querySelector('.score-item-away img').src = imageAwayUrl;
                            } else {
                                throw new Error('Image not found');
                            }
                        })
                        .catch(function () {
                            imageAwayUrl = '<?= plugin_dir_url( __DIR__ ) . "assets/images/football.png"?>';
                            clonedItem.querySelector('.score-item-away img').src = imageAwayUrl;
                        });

                    if (item.type !== 'finished') {
                        const date = new Date(data.startTimestamp * 1000);
                        let minutes = date.getMinutes() === 0 ? '00' : date.getMinutes();
                        if (minutes < 10) {
                            minutes = '0' + date.getMinutes();
                        }
                        const hours = date.getHours() < 10 ? '0' + date.getHours() : date.getHours();
                        clonedItem.querySelector('.score-item-block .time-block').innerHTML = hours + ':' + minutes;
                    } else {
                        const endingTitle = data.statusDescription === 'AET' ? 'Final / OT' : 'Final';
                        clonedItem.querySelector('.score-label').innerHTML = endingTitle;
                        clonedItem.querySelector('.score-item-home').classList.remove('winner');
                        clonedItem.querySelector('.score-item-home').classList.add(data.homeScore.current > data.awayScore.current ? 'winner' : '');
                        clonedItem.querySelector('.score-results .home-score').innerHTML = data.homeScore.current;
                        clonedItem.querySelector('.score-item-away').classList.remove('winner');
                        clonedItem.querySelector('.score-item-away').classList.add(data.awayScore.current > data.homeScore.current ? 'winner' : '');
                        clonedItem.querySelector('.score-results .away-score').innerHTML = data.awayScore.current;
                    }

                    let showItem = clonedItem.classList.remove('scaffold-results');
                    showItem.style.display = 'block';
                    let singlePage = parent.classList.contains('single-page');
                    parent.querySelector('.expand').style.display = 'none';
                    if (singlePage) {
                        if (key >= 4) {
                            parent.querySelector('.expand').style.display = 'block';
                            showItem.classList.add('collapsible');
                            showItem.style.display = 'none';
                        }
                    } else {
                        if (key >= 4) {
                            parent.querySelector('.expand').style.display = 'block';
                            showItem.classList.add('collapsible');
                            showItem.style.display = 'none';
                        }
                    }
                    collection.append(showItem);
                });
                parent.querySelector('.score-collection__content').classList.add('results');
                parent.querySelector('.score-collection__content').innerHTML = '';
                parent.querySelector('.score-collection__content').appendChild(collection);
            }

            renderSchedule(parent, response) {
                // Unslick before rendering schedule.
                Array.from(document.querySelectorAll('.euro-2020-odds .score-collection__content')).forEach(function (element) {
                    if (element.classList.contains('slick-initialized')) {
                        $(element).slick('unslick');
                    }
                });

                let collection = parent.querySelector('.scaffold-div').cloneNode(true);
                response.data.forEach(function (item, key) {
                    const data = JSON.parse(item.data);
                    let clonedItem = parent.querySelector('.score-item.scaffold').cloneNode(true);
                    let modified = clonedItem;

                    let home_team = searchTeamName(item.home_team);
                    let away_team = searchTeamName(item.away_team);

                    modified.querySelector('.score-item-home .title-name').innerHTML = home_team;
                    modified.querySelector('.score-item-away .title-name').innerHTML = away_team;
                    modified.querySelector('.score-item__content .home-team-name').value = item.home_team;
                    modified.querySelector('.score-item__content .away-team-name').value = item.away_team;
                    modified.querySelector('.score-item__content .event-timestamp').value = item.date;

                    let league_name = parent.querySelector('.leagueName').value;

                    // Get Odds only if it's Euro2020 schedule.
                    if (league_name === 'European Championship') {

                        fetch('<?= admin_url( "admin-ajax.php" ) ?>', {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded',
                            },
                            body: new URLSearchParams({
                                action: 'guru_sports_data_get_single_event_odds',
                                home_team: item.home_team,
                                away_team: item.away_team,
                                event_timestamp: item.date,
                            }),
                        }).then(function (response) {
                            return response.json();
                        }).then(function (response) {
                            if (response.data) {
                                if (response.data.odd_1.bookmaker_logo &&
                                    response.data.odd_2.bookmaker_logo &&
                                    response.data.odd_x.bookmaker_logo &&
                                    response.data.odd_1.bookmaker_link &&
                                    response.data.odd_2.bookmaker_link &&
                                    response.data.odd_x.bookmaker_link) {
                                    modified.querySelector('.score-item__content .odd-home .line').textContent = response.data.odd_1.line;
                                    modified.querySelector('.score-item__content .odd-home .text').textContent = 'HOME';
                                    modified.querySelector('.score-item__content .odd-home .bookmaker').innerHTML = response.data.odd_1.bookmaker_logo;
                                    modified.querySelector('.score-item__content .odd-home > a').href = response.data.odd_1.bookmaker_link;
                                    modified.querySelector('.score-item__content .odd-home').classList.remove('hide');

                                    // Odd Away
                                    modified.querySelector('.score-item__content .odd-away .line').textContent = response.data.odd_2.line;
                                    modified.querySelector('.score-item__content .odd-away .text').textContent = 'AWAY';
                                    modified.querySelector('.score-item__content .odd-away .bookmaker').innerHTML = response.data.odd_2.bookmaker_logo;
                                    modified.querySelector('.score-item__content .odd-away > a').href = response.data.odd_2.bookmaker_link;
                                    modified.querySelector('.score-item__content .odd-away').classList.remove('hide');

                                    // Odd Draw
                                    modified.querySelector('.score-item__content .odd-draw .line').textContent = response.data.odd_x.line;
                                    modified.querySelector('.score-item__content .odd-draw .text').textContent = 'DRAW';
                                    modified.querySelector('.score-item__content .odd-draw .bookmaker').innerHTML = response.data.odd_x.bookmaker_logo;
                                    modified.querySelector('.score-item__content .odd-draw > a').href = response.data.odd_x.bookmaker_link;
                                    modified.querySelector('.score-item__content .odd-draw').classList.remove('hide');
                                } else {
                                    modified.querySelectorAll('.score-item__content .odd').forEach(el => el.style.display = 'none');
                                }
                            } else {
                                // parent.querySelector('.score-collection__content').innerHTML = '';
                                // parent.querySelector('.no-events').style.display = 'block';
                            }
                        }).catch(function () {
                            // Handle error
                        }).finally(function () {
                            // input.closest('.loader-input-block').querySelector('.loader-input').style.display = 'none';
                            // input.removeAttribute('disabled');
                            // parent.classList.remove('loading-data');
                        });
                    }

                    const imageSlug = data.sport ? data.sport.slug : data.tournament.uniqueTournament.category.sport.slug;
                    let imageHomeUrl = '<?= $team_logo_dir . "/" ?>' + data.homeTeam.id + '.png';
                    fetch(imageHomeUrl).then(function () {
                        modified.querySelector('.score-item-home img').src = imageHomeUrl;
                    }).catch(function () {
                        imageHomeUrl = '<?= plugin_dir_url( __DIR__ ) . "assets/images/football.png"?>';
                        modified.querySelector('.score-item-home img').src = imageHomeUrl;
                    });
                    let imageAwayUrl = '<?= $team_logo_dir . "/" ?>' + data.awayTeam.id + '.png';
                    fetch(imageAwayUrl).then(function () {
                        modified.querySelector('.score-item-away img').src = imageAwayUrl;
                    }).catch(function () {
                        imageAwayUrl = '<?= plugin_dir_url( __DIR__ ) . "assets/images/football.png"?>';
                        modified.querySelector('.score-item-away img').src = imageAwayUrl;
                    });

                    if (item.type !== 'finished') {
                        const date = new Date(data.startTimestamp * 1000);
                        let minutes = date.getMinutes() === 0 ? '00' : date.getMinutes();
                        if (minutes < 10) {
                            minutes = '0' + date.getMinutes();
                        }
                        const hours = date.getHours() < 10 ? '0' + date.getHours() : date.getHours();
                        modified.querySelector('.score-item-block .time-block').innerHTML = hours + ':' + minutes;
                    } else {
                        modified.querySelector('.score-results .home-score').innerHTML = data.homeScore.current;
                        modified.querySelector('.score-results .home-score').classList.remove('winner');
                        if (data.homeScore.current > data.awayScore.current) {
                            modified.querySelector('.score-results .home-score').classList.add('winner');
                        }
                        modified.querySelector('.score-results .away-score').innerHTML = data.awayScore.current;
                        modified.querySelector('.score-results .away-score').classList.remove('winner');
                        if (data.awayScore.current > data.homeScore.current) {
                            modified.querySelector('.score-results .away-score').classList.add('winner');
                        }
                    }
                    let showItem = modified;
                    showItem.style.display = 'block';
                    if (key > 3) {
                        parent.querySelector('.expand').style.display = 'block';
                        showItem.classList.add('collapsible');
                        showItem.style.display = 'none';
                    } else {
                        parent.querySelector('.expand').style.display = 'none';
                    }
                    collection.appendChild(showItem);
                });

                parent.querySelector('.score-collection__content').innerHTML = '';
                parent.querySelector('.score-collection__content').appendChild(collection);
            }

            updateDom(parent, response) {
                const type = parent.querySelector('.type').value;
                if (type === 'results') {
                    this.renderResults(parent, response);
                } else {
                    this.renderSchedule(parent, response);
                }
            }

            call(parent, startDate, endDate, classInstance = null) {
                const context = this;
                if (!parent) return;

                parent.querySelector('.score-collection__content').style.opacity = 0;
                parent.querySelector('.loader').style.display = 'block';
                parent.querySelector('.expand').style.display = 'none';
                parent.classList.add('loading-data');

                const formData = new FormData();
                formData.append('action', 'get_events');
                formData.append('league', parent.querySelector('.leagueName').value);
                formData.append('season', parent.querySelector('.seasonName').value);
                formData.append('limit', parent.querySelector('.limit').value);
                formData.append('order', parent.querySelector('.order').value);
                formData.append('type', parent.querySelector('.type').value);
                formData.append('team1', parent.querySelector('.team1').value);
                formData.append('team2', parent.querySelector('.team2').value);
                formData.append('startDate', startDate);
                formData.append('endDate', endDate);

                fetch('<?= admin_url( 'admin-ajax.php' ) ?>', {
                    method: 'POST',
                    body: formData,
                })
                    .then(response => response.json())
                    .then(response => {
                        if (!response.data) {
                            if (classInstance) {
                                parent.querySelector('.score-collection__content').style.opacity = 0;
                                return classInstance.getEarliestEventDate(parent, startDate, classInstance);
                            }
                            parent.querySelector('.score-collection__content').innerHTML = '';
                            parent.querySelector('.expand').style.display = 'none';
                            parent.querySelector('.no-events').style.display = 'block';
                            return;
                        }

                        context.updateDom(parent, response);
                        parent.querySelector('.score-collection__content').style.opacity = 1;

                        // Include any additional logic for your specific use case here
                    })
                    .catch(() => {
                        // Handle any errors that may occur
                    })
                    .finally(() => {
                        if (!classInstance) {
                            parent.querySelector('.loader').style.display = 'none';
                            parent.querySelector('.score-collection__content').style.opacity = 1;
                        }
                        parent.querySelector('.loader').style.display = 'none';
                        parent.classList.remove('loading-data');
                    });
            }
        }

        const datePicker = new DatePicker();


    })
</script>
