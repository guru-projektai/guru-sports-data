<?php

/** @var $format - format that is taking from $_GET to set odds format */
/** @var  $upload_dir */
/** @var  $tracker_type */
/** @var  $competition */

$upload_dir = wp_upload_dir();

$project = WP_PROJECT;
if (!empty(get_option('guru_widget_project_name'))){
    $project = get_option('guru_widget_project_name');
}



$team_logo_dir = $upload_dir['baseurl'] . '/guru-sports-data/team-logos';
$league_logo_dir = $upload_dir['baseurl'] . '/guru-sports-data/league-logos';
$events = [];
$season = [];
if (!empty($data)) {

    $events = $data['events'] ?: [];
    $season = $data['season'][0];

}
$sidebar = false;
if (is_active_sidebar('sidebar-post')) {
    $sidebar = true;
}
//images

$calendar = plugin_dir_url(__DIR__) . 'assets/images/calendar.svg';
?>
<div class="py-4 score-collection <?php if ($type === 'schedule') {
    echo 'schedule-type ';
} else {
    echo 'results-type';
} ?> <?php if (is_single()) {
    echo 'single-page';
} ?>">
    <input type="hidden" class="leagueName" value="<?= $league_name ?>"/>
    <input type="hidden" class="seasonName" value="<?= $season_name ?: 'false' ?>"/>
    <input type="hidden" class="limit" value="<?= $limit ?>"/>
    <input type="hidden" class="order" value="<?= $order ?>"/>
    <input type="hidden" class="type" value="<?= $type ?>"/>
    <input type="hidden" class="team1" value="<?= $team1 ?: 'false' ?>"/>
    <input type="hidden" class="team2" value="<?= $team2 ?: 'false' ?>"/>

    <div class="score-collection__header mb-4">
        <div class="score-collection__header__content flex items-start sm:justify-between sm:items-center flex-col sm:flex-row ">
            <div class="score-collection-title mb-4 sm:mb-0">
                <h3 class="header-title"><?= $title ?></h3>
            </div>
            <div class="flex ml-auto">
                <details
                        class="<?= $type != 'schedule' ? 'hidden' : 'w-1/2 sm:w-auto sm:min-w-[180px]' ?> table_of_content categories_dropdown group col self-center relative max-w-[320px] md:ml-auto mr-2">
                    <summary id="summary_format"
                             class="relative border-neutral-300 border z-10 flex justify-between items-center text-base sm:text-lg rounded-xl bg-white cursor-pointer group-[[open]]:after:rotate-180 after:text-3xl after:mx-4 after:leading-[0] after:w-3 after:min-w-[0.75rem] after-content-triangle">
                        <div class="py-2 pl-6 w-full text-base"><?= $format == 'decimal' || empty($format) ? 'Decimal' : ''; ?><?= $format == 'fractional' ? 'Fractional' : ''; ?></div>
                    </summary>
                    <ul class="max-h-[420px] z-9 overflow-y-auto shadow-2xl absolute w-full">
                        <li class="my-2">
                            <a href="#" class="!font-normal w-full block" id="decimal">Decimal</a>
                        </li>
                        <li class="my-2">
                            <a href="#" class="!font-normal w-full block" id="fractional">Fractional</a>
                        </li>
                    </ul>
                </details>
                <div class="<?= $type != 'schedule' ? 'w-full sm:w-auto' : 'w-1/2 lg:w-auto lg:min-w-[180px]' ?> flatpickr-date-select relative ">
                    <div class="loader-input-block sm:flex sm:items-center">
                        <div class="relative sm:max-w-[180px] w-full">

                            <label for="name"
                                   class="absolute -top-4 md:-top-2 left-3 rounded text-neutral-600 text-xs px-1 hidden md:block">
                                <span class="relative z-20"><?= get_option('choose_date__guru'); ?></span>
                                <span class="z-10 sm:bg-white absolute h-[10px] -left-[1px] -right-[2px] top-[8px] block"></span>
                            </label>
                            <input type="text" data-input name="name"
                                   class="flatpickr-sport-data w-full py-2 px-4 block w-full rounded-xl text-base outline-0 border border-neutral-300  placeholder:text-neutral-300 focus:border-neutral-600 text-left"
                                   style="background-image:url('<?= $calendar ?>'); background-position: right 8px center; background-repeat: no-repeat;">
                        </div>
                        <div class="loader-input absolute top-2"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <?php if ($sidebar) : ?>
        <div class="scaffold-div grid grid-cols-1 <?= $type !== 'schedule' || !$show_odds ? 'xl:grid-cols-2' : '' ?> gap-4"></div>
    <?php else: ?>
        <div class="scaffold-div grid grid-cols-1 lg:grid-cols-2 gap-4"></div>
    <?php endif; ?>
    <!-- RESULTS TEMPLATE -->
    <div class="score-item results scaffold-results" style="display: none">
        <div class="score-item__content">
            <input type="hidden" class="home-team-name" value=""/>
            <input type="hidden" class="away-team-name" value=""/>
            <input type="hidden" class="event-timestamp" value=""/>
            <div class="score-label hidden"></div>
            <div class="score-item__wrapper">
                <div class="items-center rounded-2xl bg-white px-4 py-3 mx-auto flex flex-col sm:flex-row ">
                    <div class="flex flex-1 justify-between mb-2 sm:mb-0 w-full score-item-home ">
                        <div class="flex items-center">
                            <div class="item-block-image w-[32px] mr-2">
                                <img src="" alt="" class="!w-full">
                            </div>
                            <div class="item-block-title flex-1">
                                <div class="title-name text-base md:text-lg !leading-5 min-h-[40px] flex items-center"></div>
                            </div>
                        </div>
                        <div class="sm:hidden score-results w-auto flex items-center py-1 px-3 bg-neutral-100 rounded-lg min-w-[40px] justify-center">
                            <div class="home-score text-black font-bold "></div>
                        </div>
                    </div>
                    <div class="hidden sm:flex score-results-mobile w-auto items-center px-3 bg-neutral-100 rounded-lg">
                        <div class="home-score text-black font-bold ml-auto"></div>
                        <div class="text-center text-black-500 px-0.5">:</div>
                        <div class="away-score mr-auto text-black font-bold"></div>
                    </div>
                    <div class="flex flex-1 flex-row justify-between sm:justify-end w-full score-item-away ">
                        <div class="flex items-center">
                            <div class="item-block-title flex-1 order-2 sm:order-1">
                                <div class="title-name text-base md:text-lg !leading-5 min-h-[40px] flex items-center sm:justify-end sm:text-right"></div>
                            </div>
                            <div class="item-block-image w-[32px] mr-2 sm:mr-2 ml-0 sm:ml-2 order-1 sm:order-2">
                                <img src="" alt="" class="!w-full">
                            </div>
                        </div>
                        <div class="sm:hidden score-results w-auto flex items-center px-3 bg-neutral-100 rounded-lg min-w-[40px] justify-center">
                            <div class="away-score text-black font-bold"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- / RESULTS TEMPLATE -->

    <!-- SCHEDULE TEMPLATE -->

    <div class="score-item scaffold " style="display: none;">
        <div class="score-item__content sm:items-center rounded-2xl bg-white px-4 py-3 mx-auto sm:flex">
            <input type="hidden" class="home-team-name" value=""/>
            <input type="hidden" class="away-team-name" value=""/>
            <input type="hidden" class="event-timestamp" value=""/>
            <div class="score-item-block w-full sm:w-auto self-center mr-3 mb-2 sm:mb-0 flex bg-neutral-100 rounded-lg px-3 pr-0 sm:pr-3 py-2">
                <div class="time-block font-bold"></div>
                <div class="ml-auto flex items-center sm:hidden text-xs text-center <?= !$show_odds ? 'hidden' : '' ?>">
                    <div class="w-10">Home</div>
                    <div class="w-10 mx-1.5">Draw</div>
                    <div class="w-10">Away</div>
                </div>
            </div>
            <div class="flex w-full">
                <div class="flex flex-col w-auto sm:ml-5 justify-start ">
                    <div class="score-item-home mb-1 sm:mb-0">
                        <div class="item-block flex items-center">
                            <div class="item-block-image w-[32px] mr-2">
                                <img src="" alt="" class="!w-full">
                            </div>
                            <div class="item-block-title flex-1">
                                <div class="title-name text-base md:text-lg !leading-5 min-h-[40px] flex items-center"></div>
                            </div>
                        </div>
                    </div>
                    <div class="score-item-away">
                        <div class="item-block flex items-center">
                            <div class="item-block-image w-[32px] mr-2">
                                <img src="" alt="" class="!w-full"/>
                            </div>
                            <div class="item-block-title">
                                <div class="title-name text-base md:text-lg !leading-5 min-h-[40px] flex items-center"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="odds flex flex-col ml-auto <?= !$show_odds ? 'hidden' : '' ?>">
                    <div class="hidden md:block text-sm mb-3 text-center"><?= get_option('prematch_best_odds__guru', 'PRE-MATCH BEST ODDS'); ?> </div>
                    <div class="flex gap-3 xl:gap-8 items-center">
                        <a href="#" rel="nofollow" target="_blank"
                           class="flex flex-col sm:flex-row !no-underline justify-center odd_home">
                            <div class="odd-logo w-9 rounded-sm overflow-hidden sm:mr-2 order-2 sm:order-1 mt-3 md:mt-0">
                                <img src="" alt=""
                                     class="!w-full !rounded-none" onerror="this.onerror=null; this.src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mP8/wcAAgAB/at+5LkAAAAASUVORK5CYII=';"/>
                            </div>
                            <div class="odd-text order-1 sm:order-2">
                                <div class="text text-sm font-bold leading-3 hidden sm:block">Home</div>
                                <div class="line grid text-sm leading-5 bg-gray-100 sm:bg-transparent text-center sm:text-left rounded-sm mb-1 sm:mb-0 mt-1">
                                    <div class="min-h-full bg-gradient h-[20px]"></div>
                                </div>
                            </div>
                        </a>
                        <a href="#" rel="nofollow" target="_blank"
                           class="flex flex-col sm:flex-row !no-underline justify-center odd_draw">
                            <div class="odd-logo w-9 rounded-sm overflow-hidden sm:mr-2 order-2 sm:order-1 mt-3 md:mt-0">
                                <img src="" alt=""
                                     class="!w-full !rounded-none" onerror="this.onerror=null; this.src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mP8/wcAAgAB/at+5LkAAAAASUVORK5CYII=';"/>
                            </div>
                            <div class="odd-text order-1 sm:order-2">
                                <div class="text text-sm font-bold leading-3 hidden sm:block">Draw</div>
                                <div class="line grid text-sm leading-5 bg-gray-100 sm:bg-transparent text-center sm:text-left rounded-sm mb-1 sm:mb-0 mt-1">
                                    <div class="min-h-full bg-gradient h-[20px]"></div>
                                </div>
                            </div>
                        </a>
                        <a href="#" rel="nofollow" target="_blank"
                           class="flex flex-col sm:flex-row !no-underline justify-center odd_away">
                            <div class="odd-logo w-9 rounded-sm overflow-hidden sm:mr-2 order-2 sm:order-1 mt-3 md:mt-0">
                                <img src="" alt=""
                                     class="!w-full !rounded-none" onerror="this.onerror=null; this.src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mP8/wcAAgAB/at+5LkAAAAASUVORK5CYII=';"/>
                            </div>
                            <div class="odd-text order-1 sm:order-2">
                                <div class="text text-sm font-bold leading-3 hidden sm:block">Away</div>
                                <div class="line grid text-sm leading-5 bg-gray-100 sm:bg-transparent text-center sm:text-left rounded-sm mb-1 sm:mb-0 mt-1">
                                    <div class="min-h-full bg-gradient h-[20px]"></div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- /SCHEDULE TEMPLATE -->

    <div class="loader-block">
        <div class="loader">
            <div class="animate-pulse grid sm:grid-cols-1 md:grid-cols-2 gap-4">
                <div>
                    <div class="items-center rounded-2xl bg-white px-4 py-3 mx-auto flex flex-row">
                        <div class="flex-1">
                            <div class="flex items-center h-[40px]">
                                <div class="w-[32px] mr-2 rounded-full bg-slate-200 h-[32px]"></div>
                                <div class="item-block-title flex-1 grid grid-cols-1 gap-2">
                                    <div class="h-[10px] flex items-center bg-slate-200 w-auto rounded-xl"></div>
                                    <div class="h-[10px] flex items-center bg-slate-200 w-auto rounded-xl"></div>
                                </div>
                            </div>
                        </div>
                        <div class="w-[40px] text-center text-xs"></div>
                        <div class="flex-1">
                            <div class="flex items-center h-[40px]">
                                <div class="w-[32px] mr-2 rounded-full bg-slate-200 h-[32px]"></div>
                                <div class="item-block-title flex-1 grid grid-cols-1 gap-2">
                                    <div class="h-[10px] flex items-center bg-slate-200 w-auto rounded-xl"></div>
                                    <div class="h-[10px] flex items-center bg-slate-200 w-auto rounded-xl"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="hidden md:block">
                    <div class="items-center rounded-2xl bg-white px-4 py-3 mx-auto flex flex-row">
                        <div class="flex-1">
                            <div class="flex items-center h-[40px]">
                                <div class="w-[32px] mr-2 rounded-full bg-slate-200 h-[32px]"></div>
                                <div class="item-block-title flex-1 grid grid-cols-1 gap-2">
                                    <div class="h-[10px] flex items-center bg-slate-200 w-auto rounded-xl"></div>
                                    <div class="h-[10px] flex items-center bg-slate-200 w-auto rounded-xl"></div>
                                </div>
                            </div>
                        </div>
                        <div class="w-[40px] text-center text-xs"></div>
                        <div class="flex-1">
                            <div class="flex items-center h-[40px]">
                                <div class="w-[32px] mr-2 rounded-full bg-slate-200 h-[32px]"></div>
                                <div class="item-block-title flex-1 grid grid-cols-1 gap-2">
                                    <div class="h-[10px] flex items-center bg-slate-200 w-auto rounded-xl"></div>
                                    <div class="h-[10px] flex items-center bg-slate-200 w-auto rounded-xl"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="no-events rounded-2xl bg-white px-4 py-3 text-center flex items-center justify-center min-h-[64px]"
         style="display: none">
        <?= $no_events_text ?>
    </div>
    <div class="score-collection__content"></div>
    <button class="expand relative btn m-auto mt-5 inline-flex items-center whitespace-nowrap justify-center w-[240px] h-12 text-base text-black rounded-2xl border border-neutral-200 hover:bg-neutral-50 bg-white active:hover:bg-neutral-200 py-3 px-10 !no-underline"
            style="display: none">
        <span class="button-label pr-6"><?= $show_more_text ?></span>
        <span class="w-6 h-6 has-children-icon absolute right-5"><?= get_icon('chevron') ?></span>
    </button>
</div>

<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", () => {
        var is_sbg_site = '<?= GURU_SPORTS_DATA_IS_SBG_SITE ?>';
        var is_lg_site = '<?= GURU_SPORTS_DATA_IS_LG_SITE ?>';

        function searchTeamName(key) {
            if (is_lg_site == 1) {
                let inputArray = [
                    {name: "Austria", name_lt: "Austrija"},
                    {name: "Belgium", name_lt: "Belgija"},
                    {name: "Croatia", name_lt: "Kroatija"},
                    {name: "Czech Republic", name_lt: "Čekija"},
                    {name: "Denmark", name_lt: "Danija"},
                    {name: "England", name_lt: "Anglija"},
                    {name: "Finland", name_lt: "Suomija"},
                    {name: "France", name_lt: "Prancūzija"},
                    {name: "Germany", name_lt: "Vokietija"},
                    {name: "Hungary", name_lt: "Vengrija"},
                    {name: "Italy", name_lt: "Italija"},
                    {name: "Netherlands", name_lt: "Olandija"},
                    {name: "North Macedonia", name_lt: "Šiaurės Makedonija"},
                    {name: "Poland", name_lt: "Lenkija"},
                    {name: "Portugal", name_lt: "Portugalija"},
                    {name: "Russia", name_lt: "Rusija"},
                    {name: "Scotland", name_lt: "Škotija"},
                    {name: "Slovakia", name_lt: "Slovakija"},
                    {name: "Spain", name_lt: "Ispanija"},
                    {name: "Sweden", name_lt: "Švedija"},
                    {name: "Switzerland", name_lt: "Šveicarija"},
                    {name: "Turkey", name_lt: "Turkija"},
                    {name: "Ukraine", name_lt: "Ukraina"},
                    {name: "Wales", name_lt: "Velsas"},
                ];

                for (let i = 0; i < inputArray.length; i++) {
                    if (inputArray[i].name === key) {
                        return inputArray[i].name_lt;
                    }
                }
            }

            return key;
        }

        class DatePicker {
            datepickerInstance = null;
            showMoreText = '';
            showLessText = '';
            competition = "<?= $competition ?>";
            tracker_type = "<?= $tracker_type ?>";

            constructor(classInstance = this) {
                this.showMoreText = '<?= $show_more_text ?>';
                this.showLessText = '<?= $show_less_text ?>';
                this.datepickerInstance = flatpickr('.flatpickr-sport-data', {
                    enableTime: false,
                    allowInput: true,
                    disableMobile: true,
                    defaultDate: new Date(),
                    onClose: function (selectedDates, time, calendar) {
                        calendar.isClosed = true;
                    },
                    onReady: function (selectedDate, time, calendar) {
                        let parent = calendar.input.closest('.score-collection');
                        calendar.isClosed = true;
                        if (parent) {
                            const type = parent.querySelector('.type').value;
                            if (type === 'results') {
                                calendar.set('maxDate', new Date());
                                //classInstance.toggleDateArrows(time, parent);
                            }
                        }
                        if (!calendar.input.classList.contains('initialized')) {
                            calendar.input.addEventListener('click', function (event) {
                                var datePicker = event.target._flatpickr;
                                if (datePicker.isClosed) {
                                    datePicker.isClosed = false;
                                } else {
                                    datePicker.close();
                                }
                            });
                        }
                    },
                    onChange: function (item, val, calendar) {
                        let parent = calendar.input.closest('.score-collection');
                        const type = parent.querySelector('.type').value;
                        let startDate = new Date(item);
                        startDate.setHours(0, 0, 0, 0);
                        let endDate = new Date(item);
                        endDate.setHours(23, 59, 59, 999);
                        parent.querySelector('.no-events').style.display = 'none';
                        if (parent.querySelector('.expand').length > 0) {
                            let expandButton = parent.querySelector('.expand');
                            expandButton.querySelector('.fas').classList.remove('fa-chevron-up');
                            expandButton.querySelector('.fas').classList.add('fa-chevron-down');
                            expandButton.classList.remove('expanded');
                            expandButton.querySelector('.button-label').innerHTML = classInstance.showMoreText;
                        }
                        if (type === 'results') {
                            //classInstance.toggleDateArrows(val, parent);
                        }

                        classInstance.call(parent, startDate / 1000, endDate / 1000);
                    }
                });

                const expandButtons = document.querySelectorAll('.expand');
                expandButtons.forEach(button => {
                    button.onclick = function () {
                        button.classList.toggle('expanded');
                        button.querySelector('.button-label').innerHTML = button.classList.contains('expanded') ? classInstance.showLessText : classInstance.showMoreText;
                        const collapsibles = button.closest('.score-collection').querySelectorAll('.collapsible');
                        collapsibles.forEach(collapsible => {
                            collapsible.classList.toggle('expanded');
                            collapsible.style.display = collapsible.style.display === 'none' ? '' : 'none';
                        });
                    }
                });

                // Initial call
                document.querySelectorAll('.flatpickr-sport-data:not(.initialized)').forEach(function (flatpickrElement) {
                    let parent = flatpickrElement.closest('.score-collection');
                    let startDate = new Date();
                    startDate.setHours(0, 0, 0, 0);
                    let endDate = new Date();
                    endDate.setHours(23, 59, 59, 999);
                    flatpickrElement.classList.add('initialized');
                    classInstance.call(parent, startDate / 1000, endDate / 1000, classInstance);
                });
            }

            getEarliestEventDate(parent, startDate, classInstance) {
                let instance = classInstance.datepickerInstance;
                if (classInstance.datepickerInstance.length > 1) {
                    instance = classInstance.datepickerInstance.find(function (instance) {
                        return instance.input.closest('.score-collection') === parent;
                    })
                }
                let input = instance.input;
                let context = input._flatpickr;
                if (input.classList.contains('reinitialized')) {
                    return;
                }
                input.classList.add('reinitialized');
                input.setAttribute('disabled', 'disabled');
                input.closest('.loader-input-block').querySelector('.loader-input').style.display = 'block';
                parent.classList.add('loading-data');
                fetch('<?= admin_url("admin-ajax.php") ?>', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                    },
                    body: new URLSearchParams({
                        action: 'get_earliest_event_date',
                        league: parent.querySelector('.leagueName').value,
                        season: parent.querySelector('.seasonName').value,
                        limit: parent.querySelector('.limit').value,
                        order: parent.querySelector('.order').value,
                        type: parent.querySelector('.type').value,
                        team1: parent.querySelector('.team1').value,
                        team2: parent.querySelector('.team2').value,
                        startDate: startDate,
                    }),
                })
                    .then(response => response.json())
                    .then(response => {
                        if (response.data) {
                            context.setDate(new Date(response.data.date * 1000), true)
                        } else {
                            parent.querySelector('.score-collection__content').innerHTML = '';
                            parent.querySelector('.no-events').style.display = 'flex';
                        }
                    })
                    .catch(error => {
                        console.error(error);
                    })
                    .finally(() => {
                        input.closest('.loader-input-block').querySelector('.loader-input').style.display = 'none';
                        input.removeAttribute('disabled');
                        parent.classList.remove('loading-data');
                    });
            }

            renderResults(parent, response) {
                let collection = parent.querySelector('.scaffold-div').cloneNode(true);
                response.data.forEach(function (item, key) {
                    const data = JSON.parse(item.data);
                    let clonedItem = parent.querySelector('.score-item.scaffold-results').cloneNode(true);

                    let home_team = searchTeamName(item.home_team);
                    let away_team = searchTeamName(item.away_team);

                    clonedItem.querySelector('.score-item-home .title-name').innerHTML = home_team;
                    clonedItem.querySelector('.score-item-away .title-name').innerHTML = away_team;
                    clonedItem.querySelector('.score-item__content .home-team-name').value = item.home_team;
                    clonedItem.querySelector('.score-item__content .away-team-name').value = item.away_team;
                    const imageSlug = data.sport ? data.sport.slug : data.tournament.uniqueTournament.category.sport.slug;
                    let imageHomeUrl = '<?= $team_logo_dir . '/' ?>' + data.homeTeam.id + '.png';

                    fetch(imageHomeUrl)
                        .then(response => {
                            if (response.ok) {
                                clonedItem.querySelector('.score-item-home img').src = imageHomeUrl;
                            } else {
                                throw new Error('Image not found');
                            }
                        })
                        .catch(error => {
                            imageHomeUrl = '<?= plugin_dir_url(__DIR__) . 'assets/images/football.png'?>';
                            clonedItem.querySelector('.score-item-home img').src = imageHomeUrl;
                        });

                    let imageAwayUrl = '<?= $team_logo_dir . '/' ?>' + data.awayTeam.id + '.png';
                    fetch(imageAwayUrl)
                        .then(response => {
                            if (response.ok) {
                                clonedItem.querySelector('.score-item-away img').src = imageAwayUrl;
                            } else {
                                throw new Error('Image not found');
                            }
                        })
                        .catch(error => {
                            imageAwayUrl = '<?= plugin_dir_url(__DIR__) . 'assets/images/football.png'?>';
                            clonedItem.querySelector('.score-item-away img').src = imageAwayUrl;
                        });

                    if (item.type !== 'finished') {
                        const date = new Date(data.startTimestamp * 1000);
                        let minutes = date.getMinutes() === 0 ? '00' : date.getMinutes();
                        if (minutes < 10) {
                            minutes = '0' + date.getMinutes();
                        }
                        const hours = date.getHours() < 10 ? '0' + date.getHours() : date.getHours();
                        clonedItem.querySelector('.score-item-block .time-block').innerHTML = hours + ':' + minutes;
                    } else {
                        const endingTitle = data.statusDescription === 'AET' ? 'Final / OT' : 'Final';
                        clonedItem.querySelector('.score-label').innerHTML = endingTitle;
                        clonedItem.querySelector('.score-item-home').classList.remove('winner');
                        const home_winner = data.homeScore.current > data.awayScore.current ? 'winner' : '';
                        if (home_winner.length > 0) {
                            clonedItem.querySelector('.score-item-home').classList.add(home_winner);
                        }
                        clonedItem.querySelector('.score-results .home-score').innerHTML = data.homeScore.current;
                        clonedItem.querySelector('.score-results .away-score').innerHTML = data.awayScore.current;
                        clonedItem.querySelector('.score-results-mobile .home-score').innerHTML = data.homeScore.current;
                        clonedItem.querySelector('.score-results-mobile .away-score').innerHTML = data.awayScore.current;

                        clonedItem.querySelector('.score-item-away').classList.remove('winner');
                        const away_winner = data.awayScore.current > data.homeScore.current ? 'winner' : '';
                        if (away_winner.length > 0) {
                            clonedItem.querySelector('.score-item-away').classList.add(away_winner);
                        }

                    }
                    clonedItem.classList.remove('scaffold-results');
                    clonedItem.style.display = 'block';
                    let singlePage = parent.classList.contains('single-page');
                    parent.querySelector('.expand').style.display = 'none';
                    if (singlePage) {
                        if (key >= 4) {
                            parent.querySelector('.expand').style.display = 'block';
                            clonedItem.classList.add('collapsible');
                            clonedItem.style.display = 'none';
                        }
                    } else {
                        if (key >= 4) {
                            parent.querySelector('.expand').style.display = 'block';
                            clonedItem.classList.add('collapsible');
                            clonedItem.style.display = 'none';
                        }
                    }
                    collection.appendChild(clonedItem);
                })
                parent.querySelector('.score-collection__content').classList.add('results');
                parent.querySelector('.score-collection__content').innerHTML = '';
                parent.querySelector('.score-collection__content').appendChild(collection);
            }


            renderSchedule(parent, response, tracker_type, competition) {
                let collection = parent.querySelector('.scaffold-div').cloneNode(true);
                response.data.forEach(function (item, key) {
                    const data = JSON.parse(item.data);
                    //console.log(data);
                    const query = `
                          query getOdds (
                              $country: String,
                              $region: String,
                              $project: String!,
                              $show_restricted: Boolean,
                              $tracker_type: String,
                              $odds_type: String,
                              $event_timestamp: Int!,
                              $home_team: String!,
                              $away_team: String!,
                              $competition: String
                              ) {
                              odds (
                                country: $country,
                                region: $region,
                                project: $project,
                                show_restricted: $show_restricted,
                                tracker_type: $tracker_type,
                                odds_type: $odds_type,
                                event_timestamp: $event_timestamp,
                                home_team: $home_team,
                                away_team: $away_team,
                                competition: $competition
                              ) {
                                    home_bookmakers (limit: 50, offset: 0) {
                                      odd
                                      name
                                      tracker {
                                        link
                                      }
                                      main_info {
                                        logo_small {
                                          src
                                          alt
                                        }
                                        logo_square {
                                          src
                                          alt
                                        }
                                      }
                                    }



                            draw_bookmakers (limit: 50, offset: 0) {
                                  odd
                                  name
                                  tracker {
                                    link
                                  }
                                  main_info {
                                    logo_small {
                                      src
                                      alt
                                    }
                                    logo_square {
                                      src
                                      alt
                                    }
                                  }
                                }

                            away_bookmakers (limit: 50, offset: 0) {
                                  odd
                                  name
                                  tracker {
                                    link
                                  }
                                  main_info {
                                    logo_small {
                                      src
                                      alt
                                    }
                                    logo_square {
                                      src
                                      alt
                                    }
                                  }
                                }
                              }
                              }
                        `;

                    const bodyElement = document.querySelector('body');
                    const country = bodyElement.getAttribute('data-country');
                    //console.log(data);
                    const variables = {
                        "event_timestamp": data.startTimestamp,
                        "home_team": data.homeTeam.name,
                        "away_team": data.awayTeam.name,
                        "project": "SBG",
                        "country":  country.toUpperCase(),
                        "tracker_type": tracker_type,
                        "show_restricted": false,
                        "competition": competition
                    };

                    let clonedItem = parent.querySelector('.score-item.scaffold').cloneNode(true);
                    let modified = clonedItem;

                    let home_team = searchTeamName(item.home_team);
                    let away_team = searchTeamName(item.away_team);

                    modified.querySelector('.score-item-home .title-name').innerHTML = home_team;
                    modified.querySelector('.score-item-away .title-name').innerHTML = away_team;
                    modified.querySelector('.score-item__content .home-team-name').value = item.home_team;
                    modified.querySelector('.score-item__content .away-team-name').value = item.away_team;
                    modified.querySelector('.score-item__content .event-timestamp').value = item.date;

                    //odds

                    // convert odds format: GGM-2592

                    fetch('<?= get_option(GURU_DATA_ODDS_API_URL) ?>', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                            query: query,
                            variables: variables
                        }),
                    })
                        .then(response => response.json())
                        .then(odds => {
                            if (odds.data) {
                                let p = 0;
                                if (odds.data.odds.home_bookmakers.length > 0) {
                                    if (odds.data.odds.home_bookmakers[0].main_info.logo_square.src) {
                                        modified.querySelector('.odd_home img').src = odds.data.odds.home_bookmakers[0].main_info.logo_square.src;
                                    }
                                    if (odds.data.odds.home_bookmakers[0].tracker.link) {
                                        modified.querySelector('.odd_home').href = odds.data.odds.home_bookmakers[0].tracker.link;
                                    }
                                    if (odds.data.odds.home_bookmakers[0].odd) {
                                        modified.querySelector('.odd_home .line').textContent = format === 'fractional' ? decimalToFrac(odds.data.odds.home_bookmakers[0].odd) : odds.data.odds.home_bookmakers[0].odd.toFixed(2);
                                    }
                                }
                                else {
                                    modified.querySelector('.odd_home').remove();
                                    p++
                                }

                                if (odds.data.odds.draw_bookmakers.length > 0) {
                                    if (odds.data.odds.draw_bookmakers[0].main_info.logo_square.src) {
                                        modified.querySelector('.odd_draw img').src = odds.data.odds.draw_bookmakers[0].main_info.logo_square.src;
                                    }
                                    if (odds.data.odds.draw_bookmakers[0].tracker.link) {
                                        modified.querySelector('.odd_draw').href = odds.data.odds.draw_bookmakers[0].tracker.link;
                                    }
                                    if (odds.data.odds.draw_bookmakers[0].odd) {
                                        modified.querySelector('.odd_draw .line').textContent = format === 'fractional' ? decimalToFrac(odds.data.odds.draw_bookmakers[0].odd) : odds.data.odds.draw_bookmakers[0].odd.toFixed(2);
                                    }
                                }
                                else {
                                    modified.querySelector('.odd_draw').remove();
                                    p++
                                }
                                if (odds.data.odds.away_bookmakers.length > 0) {
                                    if (odds.data.odds.away_bookmakers[0].main_info.logo_square.src) {
                                        modified.querySelector('.odd_away img').src = odds.data.odds.away_bookmakers[0].main_info.logo_square.src;
                                    }
                                    if (odds.data.odds.away_bookmakers[0].tracker.link) {
                                        modified.querySelector('.odd_away').href = odds.data.odds.away_bookmakers[0].tracker.link;
                                    }
                                    if (odds.data.odds.away_bookmakers[0].odd) {
                                        modified.querySelector('.odd_away .line').textContent = format === 'fractional' ? decimalToFrac(odds.data.odds.away_bookmakers[0].odd) : odds.data.odds.away_bookmakers[0].odd.toFixed(2);
                                    }
                                }
                                else {
                                    modified.querySelector('.odd_away').remove();
                                    p++
                                }

                                if (p == 3) {
                                    modified.querySelector('.odds').remove();
                                }
                            }
                        })
                        .catch(error => {
                            console.error('Error fetching data:', error);
                        });

                    let league_name = parent.querySelector('.leagueName').value;

                    const imageSlug = data.sport ? data.sport.slug : data.tournament.uniqueTournament.category.sport.slug;
                    let imageHomeUrl = '<?= $team_logo_dir . '/' ?>' + data.homeTeam.id + '.png';
                    fetch(imageHomeUrl)
                        .then(response => {
                            if (response.ok) {
                                modified.querySelector('.score-item-home img').src = imageHomeUrl;
                            } else {
                                imageHomeUrl = '<?= plugin_dir_url(__DIR__) . 'assets/images/football.png'?>';
                                modified.querySelector('.score-item-home img').src = imageHomeUrl;
                            }
                        });
                    let imageAwayUrl = '<?= $team_logo_dir . '/' ?>' + data.awayTeam.id + '.png';
                    fetch(imageAwayUrl)
                        .then(response => {
                            if (response.ok) {
                                modified.querySelector('.score-item-away img').src = imageAwayUrl;
                            } else {
                                imageAwayUrl = '<?= plugin_dir_url(__DIR__) . 'assets/images/football.png'?>';
                                modified.querySelector('.score-item-away img').src = imageAwayUrl;
                            }
                        });

                    if (item.type !== 'finished') {
                        const date = new Date(data.startTimestamp * 1000);
                        let minutes = date.getMinutes() === 0 ? '00' : date.getMinutes();
                        if (minutes < 10) {
                            minutes = '0' + date.getMinutes();
                        }
                        const hours = date.getHours() < 10 ? '0' + date.getHours() : date.getHours();
                        modified.querySelector('.score-item-block .time-block').innerHTML = hours + ':' + minutes;
                    } else {
                        const homeScoreElement = modified.querySelector('.score-results .home-score');
                        homeScoreElement.innerHTML = data.homeScore.current;
                        homeScoreElement.classList.remove('winner');
                        if (data.homeScore.current > data.awayScore.current) {
                            homeScoreElement.classList.add('winner');
                        }

                        const awayScoreElement = modified.querySelector('.score-results .away-score');
                        awayScoreElement.innerHTML = data.awayScore.current;
                        awayScoreElement.classList.remove('winner');
                        if (data.awayScore.current > data.homeScore.current) {
                            awayScoreElement.classList.add('winner');
                        }
                    }
                    let showItem = modified;
                    showItem.classList.remove('scaffold');
                    showItem.style.display = 'block';
                    if (key > 3) {
                        parent.querySelector('.expand').style.display = 'block';
                        showItem.classList.add('collapsible');
                        showItem.style.display = 'none';
                    } else {
                        parent.querySelector('.expand').style.display = 'none';
                    }
                    collection.appendChild(showItem);
                });

                parent.querySelector('.score-collection__content').innerHTML = '';
                parent.querySelector('.score-collection__content').appendChild(collection);
            }

            updateDom(parent, response, classInstance) {
                const type = parent.querySelector('.type').value;
                type === 'results' ? this.renderResults(parent, response) : this.renderSchedule(parent, response, this.tracker_type, this.competition);
            }

            call(parent, startDate, endDate, classInstance = null) {
                const context = this;
                if (!parent) return;
                parent.querySelector('.score-collection__content').style.opacity = 0;
                parent.querySelector('.loader').style.display = 'block';
                parent.querySelector('.expand').style.display = 'none';
                parent.classList.add('loading-data');

                const formData = new FormData();
                formData.append('action', 'get_events');
                formData.append('league', parent.querySelector('.leagueName').value);
                formData.append('season', parent.querySelector('.seasonName').value);
                formData.append('limit', parent.querySelector('.limit').value);
                formData.append('order', parent.querySelector('.order').value);
                formData.append('type', parent.querySelector('.type').value);
                formData.append('team1', parent.querySelector('.team1').value);
                formData.append('team2', parent.querySelector('.team2').value);
                formData.append('startDate', startDate);
                formData.append('endDate', endDate);

                fetch('<?= admin_url("admin-ajax.php") ?>', {
                    method: 'POST',
                    body: formData,
                })
                    .then(response => response.json())
                    .then(response => {
                        if (!response.data) {
                            if (classInstance) {
                                parent.querySelector('.score-collection__content').style.opacity = 0;
                                return classInstance.getEarliestEventDate(parent, startDate, classInstance);
                            }
                            parent.querySelector('.score-collection__content').innerHTML = '';
                            parent.querySelector('.expand').style.display = 'none';
                            parent.querySelector('.no-events').style.display = 'flex';
                            return;
                        }

                        context.updateDom(parent, response);
                        parent.querySelector('.score-collection__content').style.opacity = 1;

                    })
                    .catch(error => {
                        console.error('Error:', error);
                    })
                    .finally(() => {
                        if (!classInstance) {
                            parent.querySelector('.loader').style.display = 'none';
                            parent.querySelector('.score-collection__content').style.opacity = 1;
                        }
                        parent.querySelector('.loader').style.display = 'none';
                        parent.classList.remove('loading-data');
                    });
            }
        }

        const datePicker = new DatePicker();
    })
</script>
