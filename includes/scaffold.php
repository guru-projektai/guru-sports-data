<?php foreach ($events as $event) {
    $eventData = json_decode($event->data);
    ?>
    <div class="score-item">
        <div class="score-item__content">
            <div class="score-item-home <?php if (count($eventData->homeScore) > 0) { ?> <?php if ($eventData->homeScore->current > $eventData->awayScore->current) { echo 'winner'; } ?> <?php } ?>">
                <div class="item-block">
                    <div class="item-block-title">
                        <div class="title-name"><?= $eventData->homeTeam->name ?></div>
                    </div>
                    <div class="item-block-image">
                        <?php
                        $external_link = $team_logo_dir . '/' . $eventData->sport->slug . '_' . $eventData->homeTeam->id . '.png' ;
                        if (!@getimagesize($external_link)) {
                            $external_link = plugin_dir_url( __FILE__ ) . '/assets/images/football.png';
                        }
                        ?>
                        <img src="<?= $external_link ?>"
                             alt="<?= $eventData->homeTeam->name ?>">
                    </div>
                </div>
            </div>
            <div class="score-item-block">
                <?php if ($eventData->status->type !== 'finished')  { ?>
                    <?php $date = explode(' ', gmdate('Y-m-d H:s', $eventData->startTimestamp)); ?>
                    <span class="time-block"><?= $date[0] . ' ' . $date[1] ?></span>
                <?php } else { ?>
                    <span class="home-score <?php if (count($eventData->homeScore) > 0) { ?> <?php if ($eventData->homeScore->current > $eventData->awayScore->current) { echo 'winner'; } ?> <?php } ?>"><?= $eventData->homeScore->current ?></span>
                    <span class="away-score <?php if (count($eventData->awayScore) > 0) { ?> <?php if ($eventData->awayScore->current > $eventData->homeScore->current) { echo 'winner'; } ?> <?php } ?>"><?= $eventData->awayScore->current ?></span>
                <?php } ?>
            </div>
            <div class="score-item-away <?php if (count($eventData->awayScore) > 0) { ?> <?php if ($eventData->awayScore->current > $eventData->homeScore->current) { echo 'winner'; } ?> <?php } ?>">
                <div class="item-block">
                    <div class="item-block-image">
                        <?php
                        $external_link = $team_logo_dir . '/' . $eventData->sport->slug . '_' . $eventData->awayTeam->id . '.png' ;
                        if (!@getimagesize($external_link)) {
                            $external_link = plugin_dir_url( __FILE__ ) . '/assets/images/football.png';
                        }
                        ?>
                        <img src="<?= $external_link ?>"
                             alt="<?= $eventData->awayTeam->name ?>" />
                    </div>
                    <div class="item-block-title">
                        <div class="title-name"><?= $eventData->awayTeam->name ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?><?php
