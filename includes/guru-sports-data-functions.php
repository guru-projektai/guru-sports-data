<?php

use SQLBuilder\Universal\Query\SelectQuery;

function save_new_league($new_league)
{
    $league_seasons_query = new SelectQuery;
    $leagues_query = new SelectQuery;

    $league_seasons_query
        ->from(GURU_SPORTS_DATA_LEAGUES_SEASONS_TABLE)
        ->select('*')
        ->limit(0);

    $leagues_query
        ->from(GURU_SPORTS_DATA_LEAGUES_TABLE)
        ->select('*')
        ->limit(0);

    $leagues_seasons_data = (new DatabaseService($league_seasons_query))->build() ?: false;
    $leagues_data = (new DatabaseService($leagues_query))->build() ?: false;

    try {
        update_or_create_league_seasons($leagues_seasons_data, $new_league);
        update_or_create_league($leagues_data, $new_league['league'], $new_league['url']);

        (new Request())->save_logo(Request::LEAGUE_LOGO, $new_league['league']['id']);

        wp_send_json_success('Successfully created/updated league with seasons');
    } catch (Exception $exception) {
        wp_send_json_error('Something went wrong with saving new leagues: ' . $exception->getMessage());
    }
}

function get_leagues_call()
{
    $query = new SelectQuery;

    $query
        ->from(GURU_SPORTS_DATA_LEAGUES_TABLE)
        ->select('*')
        ->limit(0);

    $leagues = (new DatabaseService($query))->build() ?: false;

    try {
        wp_send_json_success($leagues);
    } catch (Exception $exception) {
        wp_send_json_error('Something went wrong with fetching leagues: ' . $exception->getMessage());
    }
}

function get_seasons_call($league_id)
{
    $query = new SelectQuery;

    $query
        ->from(GURU_SPORTS_DATA_LEAGUES_SEASONS_TABLE)
        ->select('*')
        ->limit(0)
        ->where()
        ->equal('league_id', $league_id);

    $seasons = (new DatabaseService($query))->build() ?: false;

    try {
        wp_send_json_success($seasons);
    } catch (Exception $exception) {
        wp_send_json_error('Something went wrong with fetching seasons: ' . $exception->getMessage());
    }
}

function get_syncs_call()
{
    $syncs_data = get_syncs();

    $syncs = [];

    // Find every sync events count
    foreach ($syncs_data as $sync) {
        $query = new SelectQuery;

        $query
            ->from(GURU_SPORTS_DATA_EVENTS_TABLE)
            ->select('*')
            ->where()
            ->equal('league_id', $sync->league_id)
            ->equal('season_id', $sync->season_id)
            ->limit(0)
            ->orderBy('modified', 'DESC');

        $events = (new DatabaseService($query))->build() ?: false;

        $events_count = 0;
        $last_updated = '-';

        if ($events) {
            $events_count = count($events);
            $last_updated = $events[0]->modified;
        }

        $sync->period = ! $sync->period ? '2 weeks' : $sync->period;

        $league_title = $sync->league_title;

        $sync->count = $events_count;
        $sync->last_updated = $last_updated;
        $sync->shortcodes = [
            '[guru-sports-data league="' . $league_title . '" type="schedule"]',
            '[guru-sports-data league="' . $league_title . '" type="results"]',
        ];

        $syncs[] = $sync;
    }

    try {
        wp_send_json_success($syncs);
    } catch (Exception $exception) {
        wp_send_json_error('Something went wrong with fetching syncs: ' . $exception->getMessage());
    }
}

function delete_sync_call($id)
{
    try {
        (new DatabaseService)->delete(GURU_SPORTS_DATA_SYNCS_TABLE, 'id', $id);

        add_shortcodes_to_testing_page();

        wp_send_json_success('Successfully deleted');
    } catch (Exception $exception) {
        wp_send_json_error('Something went wrong with deleting sync: ' . $exception->getMessage());
    }
}

function fetch_specific_sync_call($id)
{
    try {
        $query = new SelectQuery;

        $query
            ->from(GURU_SPORTS_DATA_SYNCS_TABLE)
            ->select('*')
            ->limit(1)
            ->where()
            ->equal('id', $id);

        $sync = (new DatabaseService($query, DatabaseService::GET_ITEM))->build() ?: false;

        if (! $sync) throw new Exception('No sync found');

        $sync = json_decode(json_encode($sync), false);

        $league_title = strtolower($sync->league_title);

        set_transient('guru-sports-data-sync-' . $sync->id, $sync);

        as_schedule_single_action(
            time(),
            GURU_SPORTS_SCHEDULE_SYNC_SINGLE,
            [
                'sync_id' => $sync->id,
                'retry' => 0,
                'param_one' => 'last',
                'param_two' => '0',
                'league_title' => $league_title,
            ],
            "guru_sports_data_fetch_events"
        );

        wp_send_json_success($sync);
    } catch (Exception $exception) {
        wp_send_json_error('Something went wrong with deleting sync: ' . $exception->getMessage());
    }
}

function get_syncs()
{
    $query = new SelectQuery;

    $query
        ->from(GURU_SPORTS_DATA_SYNCS_TABLE)
        ->select('*')
        ->limit(0);

    return (new DatabaseService($query))->build() ?: false;
}

function get_events($league, $season, $type, $limit, $order)
{
    $query = new SelectQuery;

    $query
        ->from(GURU_SPORTS_DATA_EVENTS_TABLE)
        ->select('*')
        ->limit(0);

    return (new DatabaseService($query))->build();
}

function save_sync_call($data)
{
    try {
        // var_dump($data);
        // var_dump(GURU_SPORTS_DATA_SYNCS_TABLE);
        // die();
        // var_dump(guru_transform_data($data));

        (new DatabaseService)->create(GURU_SPORTS_DATA_SYNCS_TABLE, guru_transform_data($data));

        // add_shortcodes_to_testing_page();

        wp_send_json_success('Successfully saved');
    } catch (Exception $exception) {
        wp_send_json_error('Something went wrong with saving synchronization: ' . $exception->getMessage());
    }
}

function testing_new_download_images() {
    // $league_logo = (new Request())->save_logo(Request::LEAGUE_LOGO, 7);

    // $team_logo = (new Request())->save_logo(Request::TEAM_LOGO, 2672);
    // $team_logo = (new Request())->save_logo(Request::TEAM_LOGO, 2672, '', array(2672));

    // $team_logo = self::TEAM_LOGO, $league_id = '', $sport_slug = '', $team_ids = [])

    // $response = Request::call($url, Request::TYPE_IMAGE_TASK);

    // $url = "https://api.sofascore.com/api/v1/team/$image_id/image";

    // $response = Request::downloadImage($url, self::TYPE_IMAGE_TASK, 'image', true);
}

function add_shortcodes_to_testing_page()
{
    $to_add_content = '';

    $query = new SelectQuery;

    $query
        ->from(GURU_SPORTS_DATA_SYNCS_TABLE)
        ->select('*')
        ->limit(0)
        ->orderBy('created', 'DESC');

    $syncs = (new DatabaseService($query))->build() ?: false;

    if (! $syncs) {
        $syncs = [];
        $to_add_content = '<h2>No synchronizations yet.</h2>';
    }

    foreach ($syncs as $sync) {
        $to_add_content .= '<p><h2 style="font-weight: bold">' . $sync->league_title . ' results</h2></p>';
        $to_add_content .= '<p>[guru-sports-data league="' . $sync->league_title . '" type="results"]</p>';
        $to_add_content .= '<p><h2 style="font-weight: bold">' . $sync->league_title . ' schedule</h2></p>';
        $to_add_content .= '<p>[guru-sports-data league="' . $sync->league_title . '" type="schedule"]</p>';
    }

    $post = get_page_by_path('guru-sports-data-testing-plugin', OBJECT, 'post');

    if (! $post) {
        global $user_ID;

        $new_post = [
            'post_title' => 'Guru Sports Data Testing Plugin',
            'post_content' => $to_add_content,
            'post_status' => 'publish',
            'post_date' => date('Y-m-d H:i:s'),
            'post_author' => $user_ID,
            'post_type' => 'post'
        ];

        wp_insert_post($new_post);
    }

    if ($post) wp_update_post([
        'ID' => $post->ID,
        'post_content' => $to_add_content,
    ]);
}

function get_earliest_event_call($league, $season, $limit, $type, $order, $team1, $team2, $startDate)
{
    $events_data = fetch_events($league, $season, $limit, $type, $order, $team1, $team2, $startDate);

    try {
        wp_send_json_success($events_data['event']);
    } catch (Exception $exception) {
        wp_send_json_error('Something went wrong with finding earliest event date: ' . $exception->getMessage());
    }
}

function get_events_call($league, $season, $limit, $type, $order, $team1, $team2, $startDate, $endDate)
{
    $events_data = fetch_events($league, $season, $limit, $type, $order, $team1, $team2, $startDate, $endDate);

    try {
        wp_send_json_success($events_data['events']);
    } catch (Exception $exception) {
        wp_send_json_error('Something went wrong with fetching events: ' . $exception->getMessage());
    }
}

function guru_transform_data($item)
{
    $obj = json_decode(json_encode($item), false);
    $reindexed = array_column($obj, null, 'name');

    return array_merge([
        'created' => current_time('mysql'),
        'modified' => current_time('mysql'),
        'league_id' => $reindexed['league_select']->value,
        'season_id' => $reindexed['season_select']->value,
        'period' => $reindexed['period']->value,
        'active' => 1
    ], get_injected_values($reindexed));
}

function get_injected_values($item)
{
    $league_seasons_query = new SelectQuery;
    $leagues_query = new SelectQuery;

    $league_seasons_query
        ->from(GURU_SPORTS_DATA_LEAGUES_SEASONS_TABLE)
        ->select('*')
        ->limit(0)
        ->where()
        ->equal('season_id', $item['season_select']->value);

    $leagues_query
        ->from(GURU_SPORTS_DATA_LEAGUES_TABLE)
        ->select('*')
        ->limit(0)
        ->where()
        ->equal('league_id', $item['league_select']->value);

    $league_seasons_data = (new DatabaseService($league_seasons_query))->build() ?: false;
    $league_data = (new DatabaseService($leagues_query))->build() ?: false;

    return [
        'season_title' => $league_seasons_data[0]->season_title,
        'league_title' => $league_data[0]->league_title,
    ];
}

/**
 *  Update sync start_date and end_date based on updated league_season start_date and end_date
 */
function update_syncs()
{
    $syncs = get_syncs();
    $query = new SelectQuery;

    foreach ($syncs->data() as $sync) {
        $query
            ->from(GURU_SPORTS_DATA_LEAGUES_SEASONS_TABLE)
            ->select('*')
            ->where()
            ->equal('season_id', $sync->season_id)
            ->equal('league_id', $sync->league_id)
            ->limit(0);

        $data = (new DatabaseService($query))->build() ?: false;

        if (! $data) continue;

        $sync->start_date = $data[0]->start_date;
        $sync->end_date = $data[0]->end_date;


        (new DatabaseService)->update(GURU_SPORTS_DATA_SYNCS_TABLE, $sync->id, [
            'start_date' => $sync->start_date,
            'end_date' => $sync->end_date,
            'modified' => current_time('mysql'),
        ]);
    }
}

function update_or_create_league_seasons($pods_data, $new_league)
{
    $data = json_decode(json_encode($pods_data), true);

    foreach ($new_league['seasons'] as $item) {
        $id = guru_sports_data_get_item_id_by(
            $data,
            $item['league_id'],
            'league_id',
            $item['season_id'],
            'season_id'
        );

        $update_by = $id ? ['id' => $id] : ['id' => 99999999];

        (new DatabaseService)->createOrUpdate(GURU_SPORTS_DATA_LEAGUES_SEASONS_TABLE, $update_by, $item);
    }
}

function update_or_create_league($pods_data, $new_league, $url)
{
    $data = json_decode(json_encode($pods_data), true);
    $new_league['url'] = $url;

    $league = transform_league_data($new_league);

    $id = guru_sports_data_simple_get_item_id_by(
        $data,
        $league['league_id'],
        'league_id'
    );

    $update_by = $id ? ['id' => $id] : ['id' => '9999999'];

    (new DatabaseService)->createOrUpdate(GURU_SPORTS_DATA_LEAGUES_TABLE, $update_by, $league);
}

function transform_league_data($league) {
    return [
        'league_id' => $league['id'],
        'league_title' => $league['title'],
        'url' => $league['url'],
    ];
}

function fetch_events($league_name, $season_name, $limit, $type, $order, $team1 = null, $team2 = null, $startDate = null, $endDate = null)
{
    $query = new SelectQuery;

    $query->from(GURU_SPORTS_DATA_EVENTS_TABLE)->select('*');

    $query->where()
        ->equal('league_title', $league_name);

	$league = get_current_league_season($league_name);
    $current_league_season = !empty($league) ? $league[0] : null;
    if (! $current_league_season) return false;

    $season = get_season($league_name, $season_name ?: $current_league_season->season_title);

    // season_title if given, else -> current league season id
    $season_name && $season_name !== 'false' ?
        $query->where()->equal('season_title', $season_name)
        : $query->where()->equal('season_id', $current_league_season->season_id);

    // If type is results -> status = finished  ||  if type is not result -> status != finished and != canceled
    $type === 'results' ?
        $query->where()->equal('type', 'finished')
        : $query->where()
            ->notEqual('type', 'finished')
            ->notEqual('type', 'canceled');

    // VS case
    if ($team1 && $team1 !== 'false')
        $query
            ->where()->like('home_team', $team1)
            ->or()
            ->where()->like('away_team', $team1);

    if ($team2 && $team2 !== 'false')
        $query
            ->where()->like('home_team', $team2)
            ->or()
            ->where()->like('away_team', $team2);

    if ($startDate && $endDate) {
        // get events between selected date (for ex.: 2020-02-07 and + 1 day) period 24hours
        $query
            ->where()
            ->between('date', $startDate, $endDate);
    }

    // Earliest event date return
    if ($startDate && ! $endDate) {
        $order = 'DESC';

        if ($type === 'results') {
            $query
                ->where()
                ->lessThan('date', $startDate);
        } else {
            $order = 'ASC';

            $query
                ->where()
                ->greaterThan('date', $startDate);
        }

        $query->orderBy('date', $order)->limit(1);

        $event = (new DatabaseService($query, DatabaseService::GET_ITEM))->build() ?: false;

        return [
            'event' => $event,
        ];
    }

    // $query
    //   ->leftJoin('wp_pods_odds_feed', 'odds_feed')
    //   ->on("odds_feed.home_team = wp_pods_guru_sports_data_events.home_team");

    $query
        ->groupBy('event_id');

    $query
        ->orderBy('date', $order)
        ->limit($limit);

    $events = (new DatabaseService($query))->build() ?: false;
    // die();

    return [
        'events' => $events,
        'season' => $season,
    ];
}

function get_odds_by_event($home_team, $away_team, $event_timestamp, $get_links = TRUE, $user_country_name = FALSE, $odds_bookmakers_only = FALSE) {
    if (GURU_SPORTS_DATA_IS_SBG_SITE) {
        // User IP country info.
        $country_name = sbg_betting_trackers_get_user_country();

        // Overriden country name.
        if ($user_country_name) {
            $country_name = $user_country_name;
        }

        $country_id = sbg_get_country_id_from_name($country_name);

        // Bonus country info.
        $country_ids = sbg_get_country_ids_based_on_bonus_country($country_name);
        $bonus_country_id = !empty($country_ids) ? $country_ids['country_id'] : FALSE;

        $accepting_countries_field_id = DatabaseService::getField('bookmaker', 'bookmaker_accepting_countries');
        $euro_2021_sport_type_id = sbg_get_bookmaker_sport_type_id_from_name('Euro2021 odds');
        $football_sport_type_id = sbg_get_bookmaker_sport_type_id_from_name('Football');
    }

    if (GURU_SPORTS_DATA_IS_LG_SITE) {
        $euro_2021_sport_type_id = sbg_get_bookmaker_sport_type_id_from_name('Euro2021 odds');
    }

    $bookmaker_ids_only = array();
    if ($odds_bookmakers_only) {
        foreach ($odds_bookmakers_only as $bookmaker) {
            $bookmaker_ids_only[] = sbg_get_bookmaker_id_from_name($bookmaker);
        }
    }
    // var_dump($odds_bookmakers_only);
    // var_dump($bookmaker_ids_only);

    // Odd 1 query
    $query = new SelectQuery;
    $query->from('wp_pods_odds_feed', 't')->select('t.odd_1', 't.bookmaker_id');

    $query->where()
        ->equal('home_team', $home_team)
        ->equal('away_team', $away_team)
        ->equal('event_timestamp', $event_timestamp);

    if (GURU_SPORTS_DATA_IS_SBG_SITE) {
        $query
          ->leftJoin('wp_podsrel', 'accepting_countries_field')
          ->on("t.bookmaker_id = accepting_countries_field.item_id AND accepting_countries_field.field_id = $accepting_countries_field_id");

        $query
            ->leftJoin('wp_pods_country', 'accepting_country')
            ->on('accepting_countries_field.related_item_id = accepting_country.id');
    }

    $query
        ->leftJoin('wp_pods_bookmaker_info', 'bookmaker_info')
        ->on('t.bookmaker_id = bookmaker_info.bookmaker_id');

    $query
        ->leftJoin('wp_pods_bookmaker', 'bookmaker')
        ->on('t.bookmaker_id = bookmaker.id');

    if (GURU_SPORTS_DATA_IS_SBG_SITE) {
        $query
            ->leftJoin('wp_pods_country_relation', 'country_relation')
            ->on('t.bookmaker_id = country_relation.bookmaker_id');

        $query
            ->leftJoin('wp_pods_bookmaker_link', 'sport_link')
            ->on('country_relation.link_id = sport_link.id');
    }

    if (GURU_SPORTS_DATA_IS_SBG_SITE) {
        $query
            ->where()
            ->equal('country_relation.sport_type_id', $football_sport_type_id)
            ->equal('country_relation.country_id', $bonus_country_id)
            ->isNot('country_relation.sport_review_id', NULL)
            ->isNot('country_relation.link_id', NULL)
            ->equal('sport_link.bookmaker_link_status', 1)
            ->equal('accepting_country.id', $country_id)
            ->equal('bookmaker.bookmaker_restricted_enabled', 1);
    }

    // Show odds data only from selected bookmakers.
    if ($bookmaker_ids_only) {
        $query
            ->where()
            ->in('bookmaker.id', $bookmaker_ids_only);
    }

    $query
        ->where()
        ->equal('bookmaker_info.bookmaker_info_enabled', 1)
        ->equal('bookmaker.bookmaker_enabled', 1);

    $query
        ->orderBy('odd_1', 'DESC')
        ->limit(1);

    // Odd 1 query
    $odd_1 = (new DatabaseService($query, DatabaseService::GET_ITEM))->build() ?: false;

    // var_dump($odd_1);
    // var_dump($query);
    // die();

    $odd_1_line = !empty($odd_1) ? $odd_1->odd_1 : FALSE;
    $odd_1_bookmaker_id = !empty($odd_1) ? (int) $odd_1->bookmaker_id : FALSE;
    $odd_1_bookmaker_name = !empty($odd_1_bookmaker_id) ? sbg_get_bookmaker_name_from_id($odd_1_bookmaker_id) : FALSE;
    $odd_1_bookmaker_info_id = (int) sbg_betting_trackers_get_bookmaker_info_id($odd_1_bookmaker_id);
    $odd_1_bookmaker_logo_id = (int) sbg_betting_trackers_get_bookmaker_logo_id($odd_1_bookmaker_info_id, 'square');
    $odd_1_bookmaker_logo = pods_image($odd_1_bookmaker_logo_id, $size = 'list_crop_thumb', $default = 0, array(), $force = false);

    $odd_1_bookmaker_link = '';
    if ($get_links) {
        $odd_1_bookmaker_link = !empty($euro_2021_sport_type_id) ? sbg_betting_trackers_get_affiliate_link($odd_1_bookmaker_id, $euro_2021_sport_type_id, $bonus_country_id) : FALSE;
    }

    if (GURU_SPORTS_DATA_IS_SBG_SITE) {
        $odd_1_bookmaker_restricted_countries = !empty($odd_1_bookmaker_id) ? sbg_betting_trackers_get_bookmaker_restricted_countries($odd_1_bookmaker_id) : array();
    }

    // var_dump($odd_1_bookmaker_id);
    // var_dump($odd_1_bookmaker_info_id);
    // var_dump($odd_1_bookmaker_logo_id);
    // var_dump($odd_1_bookmaker_link);

    $odd_1_bookmaker_is_restricted = FALSE;

    if (!empty($odd_1_bookmaker_restricted_countries)) {
      foreach ($odd_1_bookmaker_restricted_countries as $restricted_country) {
        // Mark restricted bookmaker in this country.
        if ($restricted_country == $country_id) {
          $odd_1_bookmaker_is_restricted = TRUE;
        }
      }
    }

    // Odd 2 query
    $query = new SelectQuery;
    $query->from('wp_pods_odds_feed', 't')->select('t.odd_2', 't.bookmaker_id');

    $query->where()
        ->equal('home_team', $home_team)
        ->equal('away_team', $away_team)
        ->equal('event_timestamp', $event_timestamp);

    if (GURU_SPORTS_DATA_IS_SBG_SITE) {
    $query
      ->leftJoin('wp_podsrel', 'accepting_countries_field')
      ->on("t.bookmaker_id = accepting_countries_field.item_id AND accepting_countries_field.field_id = $accepting_countries_field_id");

    $query
        ->leftJoin('wp_pods_country', 'accepting_country')
        ->on('accepting_countries_field.related_item_id = accepting_country.id');
    }

    $query
        ->leftJoin('wp_pods_bookmaker_info', 'bookmaker_info')
        ->on('t.bookmaker_id = bookmaker_info.bookmaker_id');

    $query
        ->leftJoin('wp_pods_bookmaker', 'bookmaker')
        ->on('t.bookmaker_id = bookmaker.id');

    if (GURU_SPORTS_DATA_IS_SBG_SITE) {
        $query
            ->leftJoin('wp_pods_country_relation', 'country_relation')
            ->on('t.bookmaker_id = country_relation.bookmaker_id');

        $query
            ->leftJoin('wp_pods_bookmaker_link', 'sport_link')
            ->on('country_relation.link_id = sport_link.id');
    }

    if (GURU_SPORTS_DATA_IS_SBG_SITE) {
        $query
            ->where()
            ->equal('country_relation.sport_type_id', $football_sport_type_id)
            ->equal('country_relation.country_id', $bonus_country_id)
            ->isNot('country_relation.sport_review_id', NULL)
            ->isNot('country_relation.link_id', NULL)
            ->equal('sport_link.bookmaker_link_status', 1)
            ->equal('accepting_country.id', $country_id)
            ->equal('bookmaker.bookmaker_restricted_enabled', 1);
    }

    // Show odds data only from selected bookmakers.
    if ($bookmaker_ids_only) {
        $query
            ->where()
            ->in('bookmaker.id', $bookmaker_ids_only);
    }

    $query
        ->where()
        ->equal('bookmaker_info.bookmaker_info_enabled', 1)
        ->equal('bookmaker.bookmaker_enabled', 1);

    $query
        ->orderBy('odd_2', 'DESC')
        ->limit(1);

    $odd_2 = (new DatabaseService($query, DatabaseService::GET_ITEM))->build() ?: false;
    $odd_2_line = !empty($odd_2) ? $odd_2->odd_2 : FALSE;
    $odd_2_bookmaker_id = !empty($odd_2) ? (int) $odd_2->bookmaker_id : FALSE;
    $odd_2_bookmaker_name = !empty($odd_2_bookmaker_id) ? sbg_get_bookmaker_name_from_id($odd_2_bookmaker_id) : FALSE;
    $odd_2_bookmaker_info_id = (int) sbg_betting_trackers_get_bookmaker_info_id($odd_2_bookmaker_id);
    $odd_2_bookmaker_logo_id = (int) sbg_betting_trackers_get_bookmaker_logo_id($odd_2_bookmaker_info_id, 'square');
    $odd_2_bookmaker_logo = pods_image($odd_2_bookmaker_logo_id, $size = 'list_crop_thumb', $default = 0, array(), $force = false);

    $odd_2_bookmaker_link = '';
    if ($get_links) {
        $odd_2_bookmaker_link = !empty($euro_2021_sport_type_id) ? sbg_betting_trackers_get_affiliate_link($odd_2_bookmaker_id, $euro_2021_sport_type_id, $bonus_country_id) : FALSE;
    }

    if (GURU_SPORTS_DATA_IS_SBG_SITE) {
        $odd_2_bookmaker_restricted_countries = !empty($odd_2_bookmaker_id) ? sbg_betting_trackers_get_bookmaker_restricted_countries($odd_2_bookmaker_id) : array();
    }

    $odd_2_bookmaker_is_restricted = FALSE;

    if (!empty($odd_2_bookmaker_restricted_countries)) {
      foreach ($odd_2_bookmaker_restricted_countries as $restricted_country) {
        // Mark restricted bookmaker in this country.
        if ($restricted_country == $country_id) {
          $odd_2_bookmaker_is_restricted = TRUE;
        }
      }
    }

    // Odd Draw query
    $query = new SelectQuery;
    $query->from('wp_pods_odds_feed', 't')->select('t.odd_x', 't.bookmaker_id');

    $query->where()
        ->equal('home_team', $home_team)
        ->equal('away_team', $away_team)
        ->equal('event_timestamp', $event_timestamp);

    if (GURU_SPORTS_DATA_IS_SBG_SITE) {
        $query
          ->leftJoin('wp_podsrel', 'accepting_countries_field')
          ->on("t.bookmaker_id = accepting_countries_field.item_id AND accepting_countries_field.field_id = $accepting_countries_field_id");

        $query
            ->leftJoin('wp_pods_country', 'accepting_country')
            ->on('accepting_countries_field.related_item_id = accepting_country.id');
    }

    $query
        ->leftJoin('wp_pods_bookmaker_info', 'bookmaker_info')
        ->on('t.bookmaker_id = bookmaker_info.bookmaker_id');

    $query
        ->leftJoin('wp_pods_bookmaker', 'bookmaker')
        ->on('t.bookmaker_id = bookmaker.id');

    if (GURU_SPORTS_DATA_IS_SBG_SITE) {
        $query
            ->leftJoin('wp_pods_country_relation', 'country_relation')
            ->on('t.bookmaker_id = country_relation.bookmaker_id');

        $query
            ->leftJoin('wp_pods_bookmaker_link', 'sport_link')
            ->on('country_relation.link_id = sport_link.id');
    }

    if (GURU_SPORTS_DATA_IS_SBG_SITE) {
        $query
            ->where()
            ->equal('country_relation.sport_type_id', $football_sport_type_id)
            ->equal('country_relation.country_id', $bonus_country_id)
            ->isNot('country_relation.sport_review_id', NULL)
            ->isNot('country_relation.link_id', NULL)
            ->equal('sport_link.bookmaker_link_status', 1)
            ->equal('accepting_country.id', $country_id)
            ->equal('bookmaker.bookmaker_restricted_enabled', 1);
    }

    // Show odds data only from selected bookmakers.
    if ($bookmaker_ids_only) {
        $query
            ->where()
            ->in('bookmaker.id', $bookmaker_ids_only);
    }

    $query
        ->where()
        ->equal('bookmaker_info.bookmaker_info_enabled', 1)
        ->equal('bookmaker.bookmaker_enabled', 1);

    $query
        ->orderBy('odd_x', 'DESC')
        ->limit(1);

    $odd_x = (new DatabaseService($query, DatabaseService::GET_ITEM))->build() ?: false;
    $odd_x_line = !empty($odd_x) ? $odd_x->odd_x : FALSE;
    $odd_x_bookmaker_id = !empty($odd_x) ? (int) $odd_x->bookmaker_id : FALSE;
    $odd_x_bookmaker_name = !empty($odd_x_bookmaker_id) ? sbg_get_bookmaker_name_from_id($odd_x_bookmaker_id) : FALSE;
    $odd_x_bookmaker_info_id = (int) sbg_betting_trackers_get_bookmaker_info_id($odd_x_bookmaker_id);
    $odd_x_bookmaker_logo_id = (int) sbg_betting_trackers_get_bookmaker_logo_id($odd_x_bookmaker_info_id, 'square');
    $odd_x_bookmaker_logo = pods_image($odd_x_bookmaker_logo_id, $size = 'list_crop_thumb', $default = 0, array(), $force = false);

    $odd_x_bookmaker_link = '';
    if ($get_links) {
        $odd_x_bookmaker_link = !empty($euro_2021_sport_type_id) ? sbg_betting_trackers_get_affiliate_link($odd_x_bookmaker_id, $euro_2021_sport_type_id, $bonus_country_id) : FALSE;
    }

    if (GURU_SPORTS_DATA_IS_SBG_SITE) {
        $odd_x_bookmaker_restricted_countries = !empty($odd_x_bookmaker_id) ? sbg_betting_trackers_get_bookmaker_restricted_countries($odd_x_bookmaker_id) : array();
    }

    $odd_x_bookmaker_is_restricted = FALSE;

    if (!empty($odd_x_bookmaker_restricted_countries)) {
      foreach ($odd_x_bookmaker_restricted_countries as $restricted_country) {
        // Mark restricted bookmaker in this country.
        if ($restricted_country == $country_id) {
          $odd_x_bookmaker_is_restricted = TRUE;
        }
      }
    }

    $odds = [
        'odd_1' => [
            'bookmaker_id' => $odd_1_bookmaker_id,
            'bookmaker_name' => $odd_1_bookmaker_name,
            'bookmaker_info_id' => $odd_1_bookmaker_info_id,
            'bookmaker_logo_id' => $odd_1_bookmaker_logo_id,
            'bookmaker_logo' => $odd_1_bookmaker_logo,
            'bookmaker_link' => $odd_1_bookmaker_link,
            'line' => $odd_1_line,
            'bookmaker_restricted_countries' => $odd_1_bookmaker_restricted_countries,
            'bookmaker_is_restricted' => $odd_1_bookmaker_is_restricted,
        ],
        'odd_2' => [
            'bookmaker_id' => $odd_2_bookmaker_id,
            'bookmaker_name' => $odd_2_bookmaker_name,
            'bookmaker_info_id' => $odd_2_bookmaker_info_id,
            'bookmaker_logo_id' => $odd_2_bookmaker_logo_id,
            'bookmaker_logo' => $odd_2_bookmaker_logo,
            'bookmaker_link' => $odd_2_bookmaker_link,
            'line' => $odd_2_line,
            'bookmaker_restricted_countries' => $odd_2_bookmaker_restricted_countries,
            'bookmaker_is_restricted' => $odd_2_bookmaker_is_restricted,
        ],
        'odd_x' => [
            'bookmaker_id' => $odd_x_bookmaker_id,
            'bookmaker_name' => $odd_x_bookmaker_name,
            'bookmaker_info_id' => $odd_x_bookmaker_info_id,
            'bookmaker_logo_id' => $odd_x_bookmaker_logo_id,
            'bookmaker_logo' => $odd_x_bookmaker_logo,
            'bookmaker_link' => $odd_x_bookmaker_link,
            'line' => $odd_x_line,
            'bookmaker_restricted_countries' => $odd_x_bookmaker_restricted_countries,
            'bookmaker_is_restricted' => $odd_x_bookmaker_is_restricted,
        ]
    ];

    return $odds;
}

function get_current_league_season($league_name, $league_id = null, $current = 1)
{
    $query = new SelectQuery;

    $query->from(GURU_SPORTS_DATA_LEAGUES_SEASONS_TABLE)
        ->select('*')
        ->orderBy('id', 'DESC');

    if ($league_id) $query->where()->equal('league_id', $league_id);
    if ($league_name) $query->where()->equal('league_title', "$league_name");
    $query->where()->equal('current', 1);

    return (new DatabaseService($query))->build() ?: false;
}

function get_season($league_name, $season_name)
{
    $query = new SelectQuery;

    $query
        ->from(GURU_SPORTS_DATA_LEAGUES_SEASONS_TABLE)
        ->select('*')
        ->where()
        ->equal('league_title', $league_name)
        ->equal('season_title', $season_name);

    return (new DatabaseService($query))->build() ?: false;
}

function get_league_id_by_name($league_name)
{
    $query = new SelectQuery;

    $query
        ->from(GURU_SPORTS_DATA_LEAGUES_TABLE)
        ->select('*')
        ->where()
        ->equal('league_title', $league_name);

    return (new DatabaseService($query))->build() ?: false;
}

/**
 * Get item id by specific column
 *
 * @param $array
 * @param $value
 * @param string $search_by_column
 * @param $value_two
 * @param string $search_by_column_two
 * @return array
 */
function guru_sports_data_get_item_id_by($array, $value, $search_by_column, $value_two, $search_by_column_two) {
    if (! is_array($array)) return null;

    $item = array_filter($array, static function ($item) use ($value, $search_by_column, $value_two, $search_by_column_two) {
        return $item[$search_by_column] === $value && $item[$search_by_column_two] === $value_two;
    });

    // Reset keys
    if ($item) return array_values($item)[0]['id'];
}

/**
 * Get item id by specific column
 *
 * @param $array
 * @param $value
 * @param string $search_by_column
 * @param bool $to_string
 * @return array
 */
function guru_sports_data_simple_get_item_id_by($array, $value, $search_by_column = 'league_id', $to_string = false) {
    if (! is_array($array)) return null;

    $item = array_filter($array, static function ($item) use ($value, $search_by_column, $to_string) {
        $val = $value;
        if ($to_string) $val = strval($value);

        return $item[$search_by_column] === $val;
    });

    // Reset keys
    if ($item) return array_values($item)[0]['id'];
}


add_action( 'rest_api_init', function () {
    register_rest_route( 'guru-sports-data', '/euro2020_events/(?P<startDate>\d+)/(?P<endDate>\d+)', array(
            'methods' => 'GET',
            'callback' => 'guru_sports_data_rest_euro_2020_events',
            'permission_callback' => '__return_true'
    ) );
});

function guru_sports_data_rest_euro_2020_events($data) {
    $league = 'European Championship';
    $season = 'Euro Cup 2020';
    $limit  = 10;
    $type   = 'schedule';
    $order  = 'ASC';
    $team1  = FALSE;
    $team2  = FALSE;
    $startDate = $data['startDate'];
    $endDate   = $data['endDate'];

    $events = fetch_events($league, $season, $limit, $type, $order, $team1, $team2, $startDate, $endDate);

    return $events;
}


// Rest API to get odds by particular event with
// country name and optional bookmakers list argument
// (ex. Betfair-Pinnacle as a list separated by dash).
add_action( 'rest_api_init', function () {
    register_rest_route( 'guru-sports-data', '/euro2020_odds/(?P<homeTeam>[a-zA-Z0-9-]+)/(?P<awayTeam>[a-zA-Z0-9-]+)/(?P<eventTimestamp>\d+)/(?P<countryName>[a-zA-Z0-9-]+)(?:/(?P<oddsBookmakersOnly>[a-zA-Z0-9-]+))?', array(
            'methods' => 'GET',
            'callback' => 'guru_sports_data_rest_euro_2020_odds',
            'permission_callback' => '__return_true'
    ) );
});

function guru_sports_data_rest_euro_2020_odds($data) {
    $user_country_name = $data['countryName'];

    $odds = get_transient('guru-sports-data-event-odds-data-' . $data['homeTeam'] . '-' . $data['awayTeam'] . '-' . $data['eventTimestamp'] . '-' . $user_country_name . '-' . $data['oddsBookmakersOnly']);

    $home_team = str_replace('-', ' ', $data['homeTeam']);
    $away_team = str_replace('-', ' ', $data['awayTeam']);
    $event_timestamp = $data['eventTimestamp'];
    $odds_bookmakers_only = $data['oddsBookmakersOnly'];
    $odds_bookmakers_only_array = !empty($odds_bookmakers_only) ? explode('-', $odds_bookmakers_only) : FALSE;


    if (!$odds) {
        $odds = get_odds_by_event($home_team, $away_team, $event_timestamp, $get_links = FALSE, $user_country_name, $odds_bookmakers_only_array);

        set_transient('guru-sports-data-event-odds-data-' . $data['homeTeam'] . '-' . $data['awayTeam'] . '-' . $data['eventTimestamp'] . '-' . $user_country_name . '-' . $data['oddsBookmakersOnly'], $odds, 30 * 60);
    }

    return $odds;
}

// Rest API to get small bookmaker logo for other Guru projects.
add_action( 'rest_api_init', function () {
    register_rest_route( 'guru-sports-data', '/bookmaker-logo/small/(?P<bookmakerName>[a-zA-Z0-9-]+)', array(
            'methods' => 'GET',
            'callback' => 'guru_sports_data_rest_euro_2020_bookmaker_logo_small',
            'permission_callback' => '__return_true'
    ));
});

function guru_sports_data_rest_euro_2020_bookmaker_logo_small($data) {
    $bookmaker_name = $data['bookmakerName'];
    $bookmaker_id   = sbg_get_bookmaker_id_from_name($bookmaker_name);

    $bookmaker_info_id = (int) sbg_betting_trackers_get_bookmaker_info_id($bookmaker_id);
    $bookmaker_logo_id = !empty($bookmaker_info_id) ? (int) sbg_betting_trackers_get_bookmaker_logo_id($bookmaker_info_id, 'square') : FALSE;
    $bookmaker_logo = !empty($bookmaker_logo_id) ? pods_image($bookmaker_logo_id, $size = 'list_crop_thumb', $default = 0, array(), $force = false) : FALSE;

    return $bookmaker_logo;
}


// Rest API if Bookmaker is restricted other Guru projects.
add_action( 'rest_api_init', function () {
    register_rest_route( 'guru', '/bookmaker-restricted/(?P<bookmakerName>[a-zA-Z0-9-]+)/(?P<countryName>[a-zA-Z0-9-]+)', array(
            'methods' => 'GET',
            'callback' => 'guru_sports_data_rest_bookmaker_is_restricted',
            'permission_callback' => '__return_true'
    ) );
});

function guru_sports_data_rest_bookmaker_is_restricted($data) {
    $bookmaker_name = $data['bookmakerName'];
    $country_name   = $data['countryName'];
    $bookmaker_id   = sbg_get_bookmaker_id_from_name($bookmaker_name);

    $bookmaker_is_restricted = (int) sbg_betting_trackers_is_bookmaker_restricted($bookmaker_name, $country_name);

    return $bookmaker_is_restricted;
}
