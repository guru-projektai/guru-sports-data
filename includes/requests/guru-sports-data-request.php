<?php

use SQLBuilder\Universal\Query\SelectQuery;

class Request {
    const TEAM_LOGO = 'TEAM_LOGO';
    const LEAGUE_LOGO = 'LEAGUE_LOGO';

    const TYPE_JSON_TASK = 'paulens~cheerio-json-download';
    const TYPE_CRAWLER_TASK = 'paulens~web-scraper-season-id-json';
    const TYPE_IMAGE_TASK = 'paulens~pupeteer-proxy-download-image';

    const RECALL_RESPONSE_CODES = [
        403
    ];

    /**
     * @param $leagueId
     * @param $seasonId
     * @return void|array
     * @throws Exception
     */
    public static function fetch_season_dates($leagueId, $seasonId)
    {
        $url = "https://divanscore.p.rapidapi.com/tournaments/detail?tournamentId=$leagueId";
        $response = self::call($url, self::TYPE_JSON_TASK);

        if ($response['error']) throw new Exception($response['error']);
        $data = json_decode($response['data']);

        $start = $data->uniqueTournament->startDateTimestamp;
        $end = $data->uniqueTournament->endDateTimestamp;

        return [
            'start' => $start,
            'last' => $end
        ];
    }


    /**
     * @param $leagueId
     * @param $seasonId
     * @return void|array
     * @throws Exception
     */
    public static function fetch_odds($leagueKey, $region = 'eu')
    {

        // https://api.the-odds-api.com/v3/odds/?sport=soccer_uefa_european_championship&region=eu&mkt=h2h&apiKey=31c17d9e439a840dd96d394e03eb065a

        // d("https://api.the-odds-api.com/v3/odds/?sport=$leagueKey&region=$region&mkt=h2h&apiKey=31c17d9e439a840dd96d394e03eb065a");

        $response = self::call("https://api.the-odds-api.com/v3/odds/?sport=$leagueKey&region=$region&mkt=h2h&apiKey=f9a2e01d557af62f98f1381158496bcf", self::TYPE_JSON_TASK);

        if ($response['error']) throw new Exception($response['error']);

        $odds = json_decode($response['data']);

        return $odds;
    }


//    /**
//     * @return void
//     * @throws Exception
//     */
//    public static function fetch_events()
//    {
//        $syncs = pods(GURU_SPORTS_DATA_SYNCS_TABLE);
//
//        $syncs->find([
//            'distinct' => FALSE,
//            'limit' => 0,
//        ]);
//
//        $data = $syncs->data();
//        if (! $data) return;
//
//        foreach ($data as $item) {
//            self::fetch_event_single($item);
//        }
//    }

    /**
     * @param $sync
     * @param $transient_id
     * @param $retry
     * @param string $param_one
     * @param int $param_two
     * @throws Exception
     */
    public static function fetch_event_single($sync, $transient_id, $retry, $param_one = 'next', $param_two = 0)
    {
        $url = "https://divanscore.p.rapidapi.com/tournaments/get-$param_one-matches?tournamentId=$sync->league_id&seasonId=$sync->season_id&pageIndex=$param_two";

        $response = self::call(
            $url,
            self::TYPE_JSON_TASK
        );

        if ($response['error']) throw new Exception($response['error']);

        self::create_or_update_events([
            'response' => $response['data'],
            'sync_id' => $sync->id,
            'retry' => $retry,
            'season_id' => $sync->season_id,
            'season_title' => $sync->season_title,
            'league_title' => $sync->league_title,
            'sync_period' => $sync->period ?: '2 weeks',
            'param_one' => $param_one,
            'param_two' => $param_two,
        ]);
    }

    /**
     * @param $data
     */
    public static function create_or_update_events($data)
    {
        $response = json_decode($data['response']);

        $params = [
            'pod' => GURU_SPORTS_DATA_EVENTS_TABLE,
        ];

        $retry = $data['retry'];
        $sync_id = $data['sync_id'];
        $season_id = $data['season_id'];
        $season_title = $data['season_title'];
        $league_title = $data['league_title'];
        $sync_period = $data['sync_period'];

        $param_one = $data['param_one'];
        $param_two = (int)$data['param_two'];

        if (isset($response->error)) {
            if ($param_one === 'last') {
                as_schedule_single_action(
                    time(),
                    GURU_SPORTS_SCHEDULE_SYNC_SINGLE,
                    [
                        'sync_id' => $sync_id,
                        'retry' => $retry ?: 0,
                        'param_one' => 'next',
                        'param_two' => $param_one === 'next' ? $param_two + 1 : 0,
                        'league_title' => $league_title,
                    ],
                    "guru_sports_data_fetch_events"
                );
            }

            return;
        }

        $events = $response->events;

        if ($param_one === 'last') {
            $events = array_reverse($events, false);
        }

        foreach ($events as $index => $event) {
            $data = [
                'league_id' => $event->tournament->uniqueTournament->id,
                'season_id' => $season_id,
                'league_title' => $event->tournament->uniqueTournament->name,
                'season_title' => $season_title,
                'home_team' => $event->homeTeam->name,
                'away_team' => $event->awayTeam->name,
                'type' => $event->status->type,
                'date' => $event->startTimestamp,
                'event_id' => $event->id,
                'data' => json_decode(json_encode($event), true),
            ];

            $params['data'] = $data;

            $id = self::exists($data, 'event_id', 'event_id');

            if ($id) $params['id'] = $id;

            // Fetch
            if (! self::in_time_period($event, $sync_period)) {
                if ($param_one === 'next') break;

                as_schedule_single_action(
                    time(),
                    GURU_SPORTS_SCHEDULE_SYNC_SINGLE,
                    [
                        'sync_id' => $sync_id,
                        'retry' => $retry ?: 0,
                        'param_one' => 'next',
                        'param_two' => 0,
                        'league_title' => $league_title,
                    ],
                    "guru_sports_data_fetch_events"
                );

                break;
            }

            self::guru_set_transient($event->id, $params);

            self::call_scheduler([
                'event_id' => (string)$event->id,
                'sync_id' => $sync_id,
                'retry' => $retry,
            ], $params['data']['league_title']);

            if ($index + 1 !== count($events)) continue;

            if ($response->hasNextPage == 1 || $response->hasNextPage === 'true') {
                as_schedule_single_action(
                    time(),
                    GURU_SPORTS_SCHEDULE_SYNC_SINGLE,
                    [
                        'sync_id' => $sync_id,
                        'retry' => $retry ?: 0,
                        'param_one' => $param_one,
                        'param_two' => $param_two + 1,
                        'league_title' => $league_title,
                    ],
                    "guru_sports_data_fetch_events"
                );
            } else {
                as_schedule_single_action(
                    time(),
                    GURU_SPORTS_SCHEDULE_SYNC_SINGLE,
                    [
                        'sync_id' => $sync_id,
                        'retry' => $retry ?: 0,
                        'param_one' => 'next',
                        'param_two' => $param_one === 'next' ? $param_two + 1 : 0,
                        'league_title' => $league_title,
                    ],
                    "guru_sports_data_fetch_events"
                );
            }
        }

        // If it's simple type
//        if ($tournaments === 1) {
//            foreach ($response->weekMatches->tournaments[0]->events as $event) {
//                $data = [
//                    'league_id' => $response->weekMatches->tournaments[0]->tournament->uniqueId,
//                    'season_id' => $response->weekMatches->tournaments[0]->season->id,
//                    'league_title' => $response->weekMatches->tournaments[0]->tournament->uniqueName,
//                    'season_title' => $response->weekMatches->tournaments[0]->season->year,
//                    'home_team' => $event->homeTeam->name,
//                    'away_team' => $event->awayTeam->name,
//                    'type' => $event->status->type,
//                    'date' => $event->startTimestamp,
//                    'event_id' => $event->id,
//                    'data' => json_decode(json_encode($event), true),
//                ];
//
//                $params['data'] = $data;
//
//                $id = self::exists($data, 'event_id', 'event_id');
//                if ($id) $params['id'] = $id;
//
//                // If event doesn't exist in database OR in time period
//                if (! $id || self::in_time_period($event)) {
//                    self::guru_set_transient($event->id, $params);
//
//                    self::call_scheduler([
//                        'event_id' => (string)$event->id,
//                        'sync_id' => $sync_id,
//                        'retry' => $retry,
//                    ], $params['data']['league_title']);
//                }
//            }
//            // If it's group type
//        } else {
//            foreach ($response->weekMatches->tournaments as $tournament) {
//                if (! $tournament->season) continue;
//
//                $data = [
//                    'league_id' => $tournament->tournament->uniqueId,
//                    'season_id' => $tournament->season->id,
//                    'league_title' => $tournament->tournament->uniqueName,
//                    'season_title' => $tournament->season->year,
//                ];
//
//                foreach ($tournament->events as $event) {
//                    $data['date'] = $event->startTimestamp;
//                    $data['event_id'] = $event->id;
//                    $data['home_team'] = $event->homeTeam->name;
//                    $data['away_team'] = $event->awayTeam->name;
//                    $data['type'] = $event->status->type;
//                    $data['data'] = json_decode(json_encode($event), true);
//
//                    $id = self::exists($data, 'event_id', 'event_id');
//
//                    if ($id) $params['id'] = $id;
//
//                    $params['data'] = $data;
//
//                    // If event doesn't exist in database OR in time period
//                    if (! $id || self::in_time_period($event)) {
//                        self::guru_set_transient($event->id, $params);
//
//                        self::call_scheduler([
//                            'event_id' => (string)$event->id,
//                            'sync_id' => $sync_id,
//                            'retry' => $retry
//                        ], $params['data']['league_title']);
//                    }
//                }
//            }
//        }
    }

    /**
     * @param $url
     * @param $task_name
     * @param string $type
     * @param bool $recall_on_error
     * @return array
     * @throws Exception
     */
    public static function call($url, $task_name, $type = 'json', $recall_on_error = false)
    {
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => [
                "X-RapidAPI-Host: divanscore.p.rapidapi.com",
                "X-RapidAPI-Key: 4ce57f8fc6mshb80c3b40d50ab8ap1bfe0ejsnf9260ed225d3"
            ],
        ]);

        $data = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
            return false;
        }


//        $accessKey = get_option(GURU_DATA_SCRAPER_ACCESS_KEY, false);
//        if (! $accessKey) throw new Exception('Access key not provided');
//
//        $error = null;
//        $data = null;
//
//        $curl = curl_init();
//
//        switch ($type) {
//            case 'json':
//                $post_fields = '{
//                 "startUrls": [
//                    {
//                      "url": "[url]",
//                      "method": "GET"
//                    }
//                  ]
//                }';
//
//                break;
//            case 'image':
//                $post_fields = '{
//                  "sources": ["[url]"]
//                }';
//
//                break;
//        }
//
//        $post_fields = str_replace('[url]', $url, $post_fields);
//
//        curl_setopt_array($curl, [
//            CURLOPT_URL => "https://api.apify.com/v2/actor-tasks/$task_name/run-sync?token=$accessKey",
//            CURLOPT_RETURNTRANSFER => true,
//            CURLOPT_TIMEOUT => 120,
//            CURLOPT_CUSTOMREQUEST => "POST",
//            CURLOPT_POSTFIELDS => $post_fields,
////            CURLOPT_PROXY => 'http://proxy.apify.com:8000',
////            CURLOPT_PROXYUSERPWD => 'auto:EsoReFLC8YMkcX7ZEwmM2zRu5',
//            CURLOPT_HTTPHEADER => [
//                "content-type: application/json"
//            ],
//        ]);
//
//        $data = curl_exec($curl);
//
//        if ($error_number = curl_errno($curl)) {
//            $error = curl_error($curl) . '. Curl status code: '. $error_number;
//        }
//
//        if ($type === 'image') {
//            $decoded_data = json_decode($data);
//            if ($decoded_data && $decoded_data->error) {
//                return self::call($url, $task_name, $type, $recall_on_error);
//            }
//        }
//
//        curl_close($curl);

        return [
            'data' => $data,
            'error' => $err
        ];
    }

    /**
     * @param $url
     * @param $task_name
     * @param string $type
     * @param bool $recall_on_error
     * @return array
     * @throws Exception
     */
    public static function downloadImage($url, $recall_on_error = false)
    {
        // $accessKey = get_option(GURU_DATA_SCRAPER_ACCESS_KEY, false);
        $accessKey = '252edfb675db421ead12c6fa782491a147d67ad244f';
        if (! $accessKey) throw new Exception('Access key not provided');

        $error = null;
        $data = null;

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => "http://api.scrape.do/?token=$accessKey&render=false&url=$url",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 120,
            // CURLOPT_CUSTOMREQUEST => "POST",
            // CURLOPT_POSTFIELDS => $post_fields,
//            CURLOPT_PROXY => 'http://proxy.apify.com:8000',
//            CURLOPT_PROXYUSERPWD => 'auto:EsoReFLC8YMkcX7ZEwmM2zRu5',
            // CURLOPT_HTTPHEADER => [
            //     "content-type: application/json"
            // ],
        ]);

        $data = curl_exec($curl);

        if ($error_number = curl_errno($curl)) {
            $error = curl_error($curl) . '. Curl status code: '. $error_number;
        }

        $decoded_data = json_decode($data);
        if ($decoded_data && $decoded_data->error) {
            return self::downloadImage($url, $recall_on_error);
        }

        curl_close($curl);

        // var_dump($data);
        // var_dump($error);

        return [
            'data' => $data,
            'error' => $error
        ];
    }


    /**
     * Basic curl without cloud based scraper
     *
     * @param $url
     * @return array
     */
    private static function basic_curl($url)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        return [
            'data' => $response,
            'error' => $err
        ];
    }

    /**
     * @param $eventId
     * @param $data
     */
    private static function guru_set_transient($eventId, $data)
    {
        set_transient('guru-sports-data-event-' . $eventId, $data, 24 * 60 * 60);
    }

    /**
     * @param $event
     * @param $period
     * @return bool
     *
     * If event is not younger than 2 weeks and it's not older than 2 weeks (ex.: 2020-01-12 <= now >= 2020-02-08) 4 weeks period
     */
    private static function in_time_period($event, $period)
    {
        $event_timestamp = (int)$event->startTimestamp;

        return
            (strtotime("-$period") <= $event_timestamp)
            &&
            (strtotime("+$period") >= $event_timestamp);
    }

    /**
     * @param $data
     * @param $column
     * @param $search_by
     * @return array
     *
     * Checks database if event already exists with unique id (event_id)
     */
    private static function exists($data, $column, $search_by)
    {

        $query = new SelectQuery;

        $query
            ->from(GURU_SPORTS_DATA_EVENTS_TABLE)
            ->select('*')
            ->limit(0);

        $events_data = (new DatabaseService($query))->build() ?: false;

        if ($events_data) {
            $all_events = json_decode(json_encode($events_data), true);

            // If item id exists - update, else create new one
            return guru_sports_data_simple_get_item_id_by(
                $all_events,
                $data[$column],
                $search_by,
                true
            );
        }
    }

    /**
     * @param string $type
     * @param string $sport_slug
     * @param array $team_ids
     *
     * Saves team logos
     * @param string $league_id
     * @throws Exception
     */
    public function save_logo($type = self::TEAM_LOGO, $league_id = '', $sport_slug = '', $team_ids = [])
    {
        switch ($type) {
            case self::TEAM_LOGO:
                add_filter('upload_dir', array($this, 'guru_sports_data_change_team_logos_upload_path'));

                foreach ($team_ids as $team_id) {
                    $image_id = $team_id;

                    $full_path = wp_upload_dir()['path'] . '/' . "$image_id.png";

                    // If file exists - do nothing
                    if (($file_exists = file_exists($full_path)) && imagecreatefrompng($full_path)) break;

                    if ($file_exists) wp_delete_file($full_path);

                    $url = "https://divanscore.p.rapidapi.com/teams/get-logo?teamId=$team_id";
                    $response = self::call($url, self::TYPE_JSON_TASK);

                    if ($response['error']) throw new Exception($response['error']);

                    $file = wp_upload_bits("$image_id.png", null, $response['data']);

                }

                break;

            case self::LEAGUE_LOGO:
                add_filter('upload_dir', array($this, 'guru_sports_data_change_league_logos_upload_path'));

                $full_path = wp_upload_dir()['path'] . '/' . "$league_id.png";

                // If file exists - do nothing
                if (($file_exists = file_exists($full_path)) && imagecreatefrompng($full_path)) break;

                if ($file_exists) wp_delete_file($full_path);

                $url = "https://divanscore.p.rapidapi.com/tournaments/get-logo?tournamentId=$league_id";
                $response = self::call($url, self::TYPE_JSON_TASK);

                if ($response['error']) throw new Exception($response['error']);

                wp_upload_bits("$league_id.png", null, $response['data']);

                break;
        }

        remove_filter('upload_dir', array($this, 'guru_sports_data_change_league_logos_upload_path'));
        remove_filter('upload_dir', array($this, 'guru_sports_data_change_team_logos_upload_path'));
    }

    /**
     * @param $arr
     * @return mixed
     *
     * Creates guru-sports-data/team-logos directory in uploads folder if doesn't exist
     */
    public function guru_sports_data_change_team_logos_upload_path($arr)
    {
        return $this->update_uploads_folder_path($arr, 'team-logos');
    }

    /**
     * @param $arr
     * @return mixed
     *
     * Creates guru-sports-data/league-logos directory in uploads folder if doesn't exist
     */
    public function guru_sports_data_change_league_logos_upload_path($arr)
    {
        return $this->update_uploads_folder_path($arr, 'league-logos');
    }

    /**
     * @param $arr
     * @param $type
     * @return mixed
     *
     * Update uploads folder path based on type (team-logos or league-logos)
     */
    private function update_uploads_folder_path($arr, $type)
    {
        $folder = "/guru-sports-data/$type"; // No trailing slash at the end.

        $arr['path'] = $arr['basedir'] . $folder;
        $arr['url'] = $arr['baseurl'] . $folder;
        $arr['subdir'] = $folder;

        // Create folder if doesn't exist
        $uploads_dir = trailingslashit( $arr['basedir'] ) . "guru-sports-data/$type";
        wp_mkdir_p( $uploads_dir );

        return $arr;
    }

    /**
     * @param array $attributes
     *
     * Calls Action Scheduler
     * @param $league_title
     */
    private static function call_scheduler($attributes, $league_title) {
        as_schedule_single_action(
            time(),
            GURU_SPORTS_SCHEDULE_SYNC_EVENT,
            $attributes,
            "guru_sports_data_fetch_event"
        );
    }
}
