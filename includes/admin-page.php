<?php

class Guru_Sports_Data_Admin
{
    const MENU_URL = 'guru_sports_data_settings';
    const OPTIONS = 'guru_sports_data';

    /** @var array */
    private $options;

    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        add_options_page(
            'Guru Sports Data settings',
            'Guru Sports Data settings',
            'manage_options',
            self::MENU_URL,
            array( $this, 'create_admin_page' )
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        wp_enqueue_style('flatpickr-datetimepicker-css', '//cdnjs.cloudflare.com/ajax/libs/flatpickr/4.2.3/flatpickr.css');
        wp_enqueue_style('guru-sports-data-css', Guru_Sports_Data_URL . 'assets/dist/admin.css');
        wp_enqueue_script('flatpickr-datetimepicker-js', '//cdnjs.cloudflare.com/ajax/libs/flatpickr/4.2.3/flatpickr.js');

        $preview_page = get_permalink(get_page_by_path('guru-sports-data-testing-plugin', OBJECT, 'post'));

        $this->options = get_option('guru_sports_data');
        ?>
        <div id="saveResult"></div>
        <div id="saveMessage"></div>
        <script type="module" src="<?= Guru_Sports_Data_URL . 'assets/js/admin.js' ?>"></script>
        <div class="wrap guru-sports-data-section">
            <h1>Guru sports data settings</h1>

            <div class="scrape-credentials">
                <div class="title">
                    <h3>Links for odds API:</h3>
                </div>
                <form method="post" id="scrapeForm">
                    <label for="staging_odds_api">Staging API :</label>
                    <input type="text" name="staging_odds_api" id="staging_odds_api" value="<?= get_option(GURU_DATA_STAGING_ODDS_API) ?>" style="width: 300px">
                    <label for="production_odds_api">Production API :</label>
                    <input type="text" name="production_odds_api" id="production_odds_api" value="<?= get_option(GURU_DATA_PRODUCTION_ODDS_API) ?>" style="width: 300px">
                    <hr/>
                    <label for="staging_odds_api">Selected API :</label>
                    <select name="odds_api_url">
                        <option value="<?= get_option(GURU_DATA_STAGING_ODDS_API) ?>" <?= get_option(GURU_DATA_STAGING_ODDS_API)  == get_option(GURU_DATA_ODDS_API_URL) ? 'selected' : ''?>>Staging</option>
                        <option value="<?= get_option(GURU_DATA_PRODUCTION_ODDS_API) ?>" <?= get_option(GURU_DATA_PRODUCTION_ODDS_API) == get_option(GURU_DATA_ODDS_API_URL) ? 'selected' : ''?>>Production</option>
                    </select>
                    <strong><?= get_option(GURU_DATA_ODDS_API_URL) ?> is selected</strong>
                    <div>
                        <input type="submit" id="scrapeUpdate" value="Update" class="button button-primary"/>
                    </div>
                </form>
            </div>

            <div class="league-sync">
                <div class="title">
                    <h3>League synchronization</h3>
                    <div class="response"></div>
                </div>
                <form method="post" id="leagueForm">
                    <label for="url">League url:</label>
                    <input type="text" name="url" id="url" required style="width: 300px">
                    <input type="submit" id="syncSeasons" value="Sync" class="button button-primary"/>
                </form>
                <div class="league-section"></div>
                <div class="global-save-wrap"></div>
            </div>

            <form id="sync-creation">
                <div class="title">
                    <h3>Synchronization creation</h3>
                    <div class="response-creation"></div>
                </div>

                <div class="guru-loading"></div>
                <div class="league_select_block"></div>
                <div class="season_select_block"></div>
                <div class="datepickers_block">
                    <div class="period_select_block"></div>
                </div>
                <input type="submit" id="saveSync" value="Start syncing" class="button button-primary"/>
            </form>

            <div class="active-syncs">
                <div class="guru-loading"></div>

                <div style="display: flex; justify-content: space-between; align-items: center">
                    <div style="display: flex; align-items: center;">
                        <h3 style="margin-bottom: 10px">Active synchronizations</h3>
                        <a class="button button-primary" target="_blank" href="<?= $preview_page ?>" style="margin-left: 20px">Preview page</a>
                    </div>
                    <input type="submit" class="button button-primary" id="manualFetch" value="Fetch events" style="margin-left: 20px">
                </div>

                <table class="widefat fixed">
                    <thead>
                    <tr>
                        <th>League</th>
                        <th>Season</th>
                        <th>Events count</th>
                        <th>Period</th>
                        <th style="width: 30%;">Shortcodes</th>
                        <th>Last updated</th>
                        <th/>
                        <th/>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

        </div>
        <?php
    }
}
