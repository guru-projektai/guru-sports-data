<?php

class Euro2020_Widget extends WP_Widget {

    const WIDGET_ID = 'euro_2020_widget';
    const WIDGET_NAME = 'Euro2020 widget';

    /**
     * Euro2020_Widget constructor.
     */
    public function __construct() {
        $widget_ops = array(
            'classname' => self::WIDGET_ID,
            'description' => 'A widget for displaying Euro2020 block with odds feed',
        );

        add_filter('widget_title', array($this, 'custom_widget_title'));

        parent::__construct(self::WIDGET_ID, self::WIDGET_NAME, $widget_ops);
    }

    /**
     * Frontend display of the widget
     *
     * @param array $args
     * @param array $instance
     */
    public function widget($args, $instance) {
        extract( $args );
        echo $before_widget;

        if ( ! empty( $instance['title'] ) ) {
            $title = $instance['title'];
        }

        $title = apply_filters('widget_title', $title);

        $template = ! empty( $instance['template'] ) ? $instance['template'] : 'display.php';

        $templates_dir = WP_PLUGIN_DIR . '/guru-sports-data/templates/widget';
        $all_templates = scandir($templates_dir);
        $list_templates = array_diff($all_templates, array('.', '..', 'admin.php'));
        global $wp;
        $current_url = home_url($wp->request) . '/';

        wp_register_script('jquery-downcount-js', plugins_url('../assets/', __FILE__) . 'js/jquery.downCount.js');

        wp_enqueue_script('jquery-downcount-js');

        include Guru_Sports_Data_PATH . '/templates/widget/' . $template;

        echo $after_widget;
    }

    /**
     * Admin menu form logic
     *
     * @param array $instance
     * @return string|void
     */
    public function form($instance) {
        $title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__('Title', 'text_domain');
        $template = ! empty( $instance['template'] ) ? $instance['template'] : 'display.php';

        $templates_dir = WP_PLUGIN_DIR . '/crypto-currency-quotes/templates/widget';
        $all_templates = scandir($templates_dir);
        $list_templates = array_diff($all_templates, array('.', '..', 'admin.php'));

        include Crypto_Currency_Quotes_PATH . 'templates/widget/admin.php';
    }

    /**
     *  Add custom br (break line) for widget title
     *
     * @param $title
     * @return mixed
     */
    function custom_widget_title($title) {
        $title = str_replace( 'line_break', '<br class="break-line"/>', $title );

        return $title;
    }

    /**
     * Admin logic to updated widget parameters
     *
     * @param array $new_instance
     * @param array $old_instance
     * @return array
     */
    public function update($new_instance, $old_instance) {
        $instance = [];
        $instance['title'] = (! empty( $new_instance['title'])) ? strip_tags($new_instance['title']) : '';
        $instance['template'] = (! empty( $new_instance['template'])) ? strip_tags($new_instance['template']) : '';

        return $instance;
    }
}


function euro_2020_sidebar() {
    register_sidebar(
        [
            'name' => __( 'Euro2020 sidebar', 'landx' ),
            'id' => 'euro_2020_sidebar',
            'description' => __( 'Euro2020 sidebar', 'landx' ),
            'before_widget' => '<div class="euro-2020-widget-wrapper"><div class="container">',
            'after_widget' => '</div></div>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        ]
    );
}

add_action('widgets_init', 'euro_2020_sidebar');

add_action('widgets_init', function () {
    register_widget( 'Euro2020_Widget' );
});

/**
 * Adds pods post save hook.
 */
function guru_sports_data_save_free_bet_offer_pod_item($pieces, $is_new_item, $id) {
    $bookmaker_id = $pieces['fields']['bookmaker']['value'];

    // Return early.
    if (empty($bookmaker_id)) {
      return;
    }

    // Set Bookmaker ID.
    if (!empty($bookmaker_id)) {
      $pieces['fields']['bookmaker_id']['value'] = $bookmaker_id;
    }

    return $pieces;
}

add_action('pods_api_pre_save_pod_item_free_bet_offer', 'guru_sports_data_save_free_bet_offer_pod_item', 99, 3);