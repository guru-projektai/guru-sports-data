<?php

/**
 * Class Crawler
 */
class Crawler {
    /**
     * Crawl seasons based on url
     * @param $url
     * @param bool $send_json
     */
    public static function crawlSeasons($url = null, $send_json = true)
    {
        try {
            $_url = $url;
            $url_array = explode('/', $url); // example https://www.sofascore.com/tournament/football/europe/uefa-champions-league/7 . we will try to get ID of the league

            $league_id = (int) end($url_array); //get last item of the array

            $url = "https://divanscore.p.rapidapi.com/tournaments/get-seasons?tournamentId=$league_id";  // generate url for api for seasons;

            $seasons = Request::call($url, Request::TYPE_CRAWLER_TASK);

            $url = "https://divanscore.p.rapidapi.com/tournaments/detail?tournamentId=$league_id";  // generate url for api for league;

            $league = Request::call($url, Request::TYPE_CRAWLER_TASK);

            $league = json_decode($league['data']);

            $data = json_decode($seasons['data']);

            $seasons = [];

            foreach ($data->seasons as $k=>$s) {
                    $date['start'] = 0;
                    $date['last'] = 0;
                    $current = 0;
                    if ($k == 0) {
                        $date['start'] = $league->uniqueTournament->startDateTimestamp;
                        $date['last'] = $league->uniqueTournament->endDateTimestamp;
                        $current = 1;
                    }

                    $seasons[$k] = [
                        'season_id' => $s->id,
                        'season_title' => $s->name,
                        'league_title' => $league->uniqueTournament->name,
                        'league_id' => $league_id,
                        'start_date' => $date['start'],
                        'end_date' => $date['last'],
                        'current' => $current,
                    ];

            }

            if (empty($seasons) || ! $seasons) throw new Exception('Syncing data failed');

            if ($send_json) wp_send_json_success([
                'seasons' => $seasons,
                'url' => $_url,
                'league' => [
                    'title' => $league->uniqueTournament->name,
                    'id' => $league_id,
                ]
            ]);

        } catch (Exception $exception) {
            if ($send_json) wp_send_json_error('Something went wrong: ' . $exception->getMessage());
        }
    }
}
