<?php
/**
 * Plugin Name: Guru Sports Data
 * Description: Guru Sports Data
 * Version: 2.0.5
 * Author: Mindaugas & Alex Rastvortsev
 */

// require_once WP_PLUGIN_DIR . '/guru-trackers/vendor/autoload.php';
// require_once WP_PLUGIN_DIR . '/guru-trackers/includes/DatabaseService.php';

use SQLBuilder\Universal\Query\SelectQuery;

if ( !defined( 'ABSPATH' ) ) {
    die;
}

$site_url = get_site_url();
$is_lg_site = strpos($site_url, 'lazybuguru') !== FALSE || strpos($site_url, 'lg') !== FALSE;
$is_sbg_site = strpos($site_url, 'sbg') !== FALSE || strpos($site_url, 'smartbettingguide') !== FALSE;

define( 'GURU_SPORTS_DATA_IS_LG_SITE', $is_lg_site);
define( 'GURU_SPORTS_DATA_IS_SBG_SITE', $is_sbg_site);
define( 'Guru_Sports_Data_FILE', __FILE__ );
define( 'Guru_Sports_Data_PATH', plugin_dir_path( Guru_Sports_Data_FILE ) );
define( 'Guru_Sports_Data_URL', plugin_dir_url( Guru_Sports_Data_FILE ) );
define( 'GURU_SPORTS_DATA_LEAGUES_TABLE' , "wp_guru_sports_data_leagues" );
define( 'GURU_SPORTS_DATA_LEAGUES_SEASONS_TABLE' , "wp_guru_sports_data_league_season" );
define( 'GURU_SPORTS_DATA_SYNCS_TABLE' , "wp_guru_sports_data_syncs" );
define( 'GURU_SPORTS_DATA_EVENTS_TABLE' , "wp_guru_sports_data_events" );
define( 'GURU_SPORTS_SCHEDULE_SYNC' , "guru_sports_data_schedule_sync" );
define( 'GURU_SPORTS_SCHEDULE_SYNC_SINGLE' , "guru_sports_data_schedule_sync_single" );
define( 'GURU_SPORTS_SCHEDULE_SYNC_EVENT' , "guru_sports_data_schedule_sync_event" );
define( 'GURU_SPORTS_LEAGUE_UPDATE' , "guru_sports_data_league_update_callback" );
define( 'GURU_DATA_SCRAPER_ACCESS_KEY' , "guru_data_scraper_access_key" );
define( 'GURU_DATA_STAGING_ODDS_API' , "guru_data_staging_odds_api" );
define( 'GURU_DATA_PRODUCTION_ODDS_API' , "guru_data_production_odds_api" );
define( 'GURU_DATA_ODDS_API_URL' , "guru_data_odds_api_url" );
define( 'GURU_PROXY_LIST' , "GURU_PROXY_LIST" );

register_deactivation_hook( Guru_Sports_Data_FILE, array( 'Guru_Sports_Data', 'guru_sports_data_deactivate' ) );

final class Guru_Sports_Data
{

    /**
     * Plugin instance.
     *
     * @var Guru_Sports_Data
     * @access private
     */
    private static $instance = null;

    /**
     * Get plugin instance.
     *
     * @return Guru_Sports_Data
     * @static
     */
    public static function get_instance()
    {
        if ( ! isset(self::$instance) ) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Constructor.
     *
     * @access private
     */
    private function __construct()
    {
        if (!function_exists('write_log')) {
            function write_log($log) {
                if (true === WP_DEBUG) {
                    if (is_array($log) || is_object($log)) {
                        error_log(print_r($log, true));
                    } else {
                        error_log($log);
                    }
                }
            }
        }

        register_activation_hook(Guru_Sports_Data_FILE, array($this , 'guru_sports_data_activate'));

        $this->guru_sports_data_includes();

        new Guru_Sports_Data_Admin();

        add_filter('cron_schedules', array($this, 'add_cron_recurrence_interval'));
        add_filter('cron_schedules', array($this, 'add_cron_recurrence_interval_day'));
        add_filter('cron_schedules', array($this, 'add_cron_recurrence_interval_half_day'));

        add_filter('plugin_action_links_' . plugin_basename(__FILE__), array($this, 'guru_sports_data_add_plugin_page_settings_link'));

        add_action('admin_enqueue_scripts', array($this, 'load_custom_wp_admin_style'));
        add_action('wp_enqueue_scripts', array($this, 'enqueue'));

        add_action('admin_init', array($this, 'enqueue_admin_scripts'));

        // Fetch UEFA 2020 Odds
        // add_action('admin_init', array($this, 'guru_sports_data_fetch_odds'));
        add_action('admin_init', array($this, 'guru_sport_data_trigger_odds_save'));
        add_action('guru_sports_data_save_single_odd_data', array($this, 'guru_sports_data_save_single_odd_data'), 10, 11);
        add_action( 'guru_sports_data_trigger_odds_fetch', array($this, 'guru_sports_data_trigger_odds_fetch_action'), 10, 0);

        add_action('activated_plugin', array($this, 'save_error'));

        // Create shortcode to use it anywhere
        add_shortcode('guru-sports-data', array($this, 'guru_sports_data_shortcode'));

        add_action('wp_ajax_guru_sports_data_get_single_event_odds', array($this, 'guru_sports_data_get_single_event_odds'));
        add_action('wp_ajax_nopriv_guru_sports_data_get_single_event_odds', array($this, 'guru_sports_data_get_single_event_odds'));

        add_action('wp_ajax_retrieve_seasons_callback', array($this, 'retrieve_seasons_callback'));
        add_action('wp_ajax_nopriv_retrieve_seasons_callback', array($this, 'retrieve_seasons_callback'));

        add_action('wp_ajax_save_new_league_with_seasons', array($this, 'save_new_league_with_seasons'));
        add_action('wp_ajax_nopriv_save_new_league_with_seasons', array($this, 'save_new_league_with_seasons'));

        add_action('wp_ajax_get_leagues', array($this, 'get_leagues'));
        add_action('wp_ajax_nopriv_get_leagues', array($this, 'get_leagues'));

        add_action('wp_ajax_get_seasons', array($this, 'get_seasons'));
        add_action('wp_ajax_nopriv_get_seasons', array($this, 'get_seasons'));

        add_action('wp_ajax_save_sync', array($this, 'save_sync'));
        add_action('wp_ajax_nopriv_save_sync', array($this, 'save_sync'));

        add_action('wp_ajax_get_syncs', array($this, 'get_syncs'));
        add_action('wp_ajax_nopriv_get_syncs', array($this, 'get_syncs'));

        add_action('wp_ajax_delete_sync', array($this, 'delete_sync'));
        add_action('wp_ajax_nopriv_delete_sync', array($this, 'delete_sync'));

        add_action('wp_ajax_fetch_specific_sync', array($this, 'fetch_specific_sync'));
        add_action('wp_ajax_nopriv_fetch_specific_sync', array($this, 'fetch_specific_sync'));

        add_action('wp_ajax_get_events', array($this, 'get_events'));
        add_action('wp_ajax_nopriv_get_events', array($this, 'get_events'));

        add_action('wp_ajax_get_earliest_event_date', array($this, 'get_earliest_event_date'));
        add_action('wp_ajax_nopriv_get_earliest_event_date', array($this, 'get_earliest_event_date'));

        add_action('wp_ajax_update_proxy_callback', array($this, 'update_proxy_callback'));
        add_action('wp_ajax_nopriv_update_proxy_callback', array($this, 'update_proxy_callback'));

        add_action('wp_ajax_manual_fetch_events', array($this, 'guru_sports_data_schedule_sync_manual_callback'));
        add_action('wp_ajax_nopriv_manual_fetch_events', array($this, 'guru_sports_data_schedule_sync_manual_callback'));

//        add_action('admin_init', array($this, 'guru_sports_data_schedule_sync_callback'));
//        add_action('admin_init', array($this, 'guru_sports_data_proxies_update_callback'));
//        add_action('admin_init', array($this, 'update_check'));

        add_action('admin_init', array($this, 'guru_sports_data_add_testing_page'));

        add_action('action_scheduler_failed_execution', array($this, 'guru_sports_data_action_scheduler_failed_execution_callback'), 10, 2);

        add_action(GURU_SPORTS_SCHEDULE_SYNC, array($this, 'guru_sports_data_schedule_sync_manual_callback'));
        add_action(GURU_SPORTS_SCHEDULE_SYNC_SINGLE, array($this, 'guru_sports_data_schedule_sync_single_callback'), 10, 4);
        add_action(GURU_SPORTS_SCHEDULE_SYNC_EVENT, array($this, 'guru_sports_data_schedule_sync_event_callback'), 10, 3);
        add_action(GURU_SPORTS_LEAGUE_UPDATE, array($this, 'guru_sports_data_league_update_callback'));

        $this->setup_schedule();

    }

    public function guru_sports_data_add_testing_page()
    {
        if (get_option('update_guru_sports_data_testing_page') === false) {
            add_shortcodes_to_testing_page();

            update_option('update_guru_sports_data_testing_page', true);
        }
    }

    public function guru_sports_data_action_scheduler_failed_execution_callback($action_id, $exception)
    {
        $action = ActionScheduler::store()->fetch_action($action_id);

        $retry_hooks = [
            GURU_SPORTS_SCHEDULE_SYNC_SINGLE,
            GURU_SPORTS_SCHEDULE_SYNC,
            GURU_SPORTS_SCHEDULE_SYNC_EVENT,
        ];

        $action_hook = $action->get_hook();

        if (! in_array($action_hook, $retry_hooks, true)) return;

        $action_args = $action->get_args();

        if ($action_args['retry'] >= 5) return;

        ++$action_args['retry'];

        as_schedule_single_action(
            time(),
            $action_hook,
            $action_args,
            $action->get_group()
        );
    }

    public function update_proxy_callback()
    {
        update_option(GURU_DATA_STAGING_ODDS_API, $_POST['staging_odds_api']);
        update_option(GURU_DATA_PRODUCTION_ODDS_API, $_POST['production_odds_api']);
        update_option(GURU_DATA_ODDS_API_URL, $_POST['odds_api_url']);
    }

    public function get_earliest_event_date()
    {
        get_earliest_event_call(
            $_POST['league'],
            $_POST['season'],
            $_POST['limit'],
            $_POST['type'],
            $_POST['order'],
            $_POST['team1'],
            $_POST['team2'],
            $_POST['startDate']
        );
    }

    public function get_events()
    {
        get_events_call(
            $_POST['league'],
            $_POST['season'],
            $_POST['limit'],
            $_POST['type'],
            $_POST['order'],
            $_POST['team1'],
            $_POST['team2'],
            $_POST['startDate'],
            $_POST['endDate']
        );
    }

    public function guru_sports_data_shortcode($atts = [])
    {
        remove_filter( 'the_content', 'wpautop' );
        $league_name = '';
        $season_name = false; // Takes current season (current => 1)
        $limit = 10;
        $type = '';
        $order = 'ASC';
        $team1 = false;
        $team2 = false;
        $title = '';
        $show_more_text = get_option('more_events__guru');
        $show_less_text = get_option('less_events__guru');
        $no_events_text = get_option('no_events_on_day__guru');
        $show_more_text = ($show_more_text) ? $show_more_text : 'More Events';
        $show_less_text = ($show_less_text) ? $show_less_text : 'Less Events';
        $no_events_text = ($no_events_text) ? $no_events_text : 'No events on selected day';
        $league_id = null;
        $format = 'decimal';

        if (isset($atts['show-odds'])) {
            $show_odds = boolval($atts['show-odds']);
        }

        if (isset($atts['show-more-text'])) {
            $show_more_text = $atts['show-more-text'];
        }

        if (isset($atts['competition'])) {
            $competition = $atts['competition'];
        }

        if (isset($atts['tracker-type'])) {
            $tracker_type = $atts['tracker-type'];
        }

        if (isset($atts['show-less-text'])) {
            $show_less_text = $atts['show-less-text'];
        }

        if (isset($atts['no-events-text'])) {
            $no_events_text = $atts['no-events-text'];
        }

        if (isset($atts['title'])) {
            $title = $atts['title'];
        }

        if (isset($_GET['format'])) {
            $format = sanitize_text_field($_GET['format']);
        }

        if (isset($atts['league'])) {
            $league_name = $atts['league'];
			$league = get_league_id_by_name($league_name);

            $league_id = !empty($league) ?  $league[0]->league_id : null;
        }

	    if (empty($title) && !empty($league_name)) {
			$title = $league_name;
	    }

        if (isset($atts['season'])) {
            $season_name = $atts['season'];
        }

        if (isset($atts['limit'])) {
            $limit = $atts['limit'];
        }

        if (isset($atts['type'])) {
            $type = $atts['type'];

            if ($type === 'results') $order = 'DESC';
        }

        if (isset($atts['team1'])) {
            $team1 = $atts['team1'];
        }

        if (isset($atts['team2'])) {
            $team2 = $atts['team2'];
        }

        $data = fetch_events($league_name, $season_name, $limit, $type, $order, $team1, $team2);

//        if (! $data['events']) $data['events'];

        wp_register_style('flatpickr-datetimepicker-css', '//cdnjs.cloudflare.com/ajax/libs/flatpickr/4.2.3/flatpickr.css');
        wp_register_script('flatpickr-datetimepicker-js', '//cdnjs.cloudflare.com/ajax/libs/flatpickr/4.2.3/flatpickr.js');

        wp_enqueue_script('flatpickr-datetimepicker-js');
        wp_enqueue_style('flatpickr-datetimepicker-css');
        wp_register_script('sports-data-functions-js', Guru_Sports_Data_URL.'assets/js/functions.js');
        wp_enqueue_script('sports-data-functions-js');

        ob_start();

        include Guru_Sports_Data_PATH . 'templates/guru-sports-data-shortcode.php';

        return ob_get_clean();
    }

    /**
     * Register cron event
     */
    public function setup_schedule()
    {
        self::isTestingServer();

        if (! wp_next_scheduled(GURU_SPORTS_LEAGUE_UPDATE)) {
            // as_schedule_recurring_action(time(), 'every_1_week', GURU_SPORTS_LEAGUE_UPDATE);
        }

        $args = [
            'retry' => 0,
        ];

        // if (! wp_next_scheduled(GURU_SPORTS_SCHEDULE_SYNC, $args)) {
        //     as_schedule_recurring_action(strtotime('09:00:00'), 'every_1_day', GURU_SPORTS_SCHEDULE_SYNC, $args);
        // }
    }

    public static function isTestingServer()
    {
//        if (WP_ENV === 'development') throw new \Exception('Development mode is enabled. All requests will stop.');
    }

    /**
     * @param $schedules
     * @return mixed
     */
    public function add_cron_recurrence_interval($schedules)
    {
        $schedules['every_1_week'] = array(
            'interval' => 604800,
            'display' => __( 'Every 1 week', 'textdomain' )
        );

        return $schedules;
    }

    /**
     * @param $schedules
     * @return mixed
     */
    public function add_cron_recurrence_interval_day($schedules)
    {
        $schedules['every_1_day'] = array(
            'interval' => 86400,
            'display' => __( 'Every 1 day', 'textdomain' )
        );

        return $schedules;
    }

    /**
     * @param $schedules
     * @return mixed
     */
    public function add_cron_recurrence_interval_half_day($schedules)
    {
        $schedules['every_12_hours'] = array(
            'interval' => 43200,
            'display' => __( 'Every 12 hours', 'textdomain' )
        );

        return $schedules;
    }

    /**
     * Manual add schedule sync
     */
        public function guru_sports_data_schedule_sync_manual_callback($retry)
    {
        self::isTestingServer();

        $query = new SelectQuery;

        $query
            ->from(GURU_SPORTS_DATA_SYNCS_TABLE)
            ->select('*')
            ->where()
            ->equal('active', 1)
            ->limit(0);

        $data = (new DatabaseService($query))->build() ?: false;

        if (! $data) return;

        foreach ($data as $item) {
            $league_title = strtolower($item->league_title);

            set_transient('guru-sports-data-sync-' . $item->id, $item);

            as_schedule_single_action(
                time(),
                GURU_SPORTS_SCHEDULE_SYNC_SINGLE,
                [
                    'sync_id' => $item->id,
                    'retry' => $retry ?: 0,
                    'param_one' => 'last',
                    'param_two' => '0',
                    'league_title' => $league_title,
                ],
                "guru_sports_data_fetch_events"
            );
        }
    }

    /**
     * @throws Exception
     */
//    public function guru_sports_data_schedule_sync_callback()
//    {
//        Request::fetch_events();
//    }

    /**
     * @param $sync_id
     * @param $retry
     * @param null $param_one
     * @param null $param_two
     * @throws Exception
     */
    public function guru_sports_data_schedule_sync_single_callback($sync_id, $retry, $param_one = null, $param_two = null)
    {
        self::isTestingServer();

        $transient_id = "guru-sports-data-sync-$sync_id";

        $sync = get_transient($transient_id);

        Request::fetch_event_single($sync, $transient_id, $retry, $param_one, $param_two);
    }

    /**
     * Update league data and its seasons timestamp
     * Update active syncs with updated seasons timestamps (start_date & end_date)
     */
    public function guru_sports_data_league_update_callback()
    {
        self::isTestingServer();

        $query = new SelectQuery;

        $query
            ->from(GURU_SPORTS_DATA_LEAGUES_TABLE)
            ->select('*')
            ->limit(0);

        $data = (new DatabaseService($query))->build() ?: false;

        if (! $data) return;

        foreach ($data as $league) {
            if (! empty($league->url)) Crawler::crawlSeasons($league->url, false);
        }

        update_syncs();
    }

    /**
     * @param $event_id
     * @param $sync_id
     * @param $retry
     * Get from transient API cached data and saves event
     * Uploads to guru-sports-data-logos folder of team logos
     * @throws Exception
     */
    public function guru_sports_data_schedule_sync_event_callback($event_id, $sync_id, $retry)
    {
        self::isTestingServer();

        $data = get_transient('guru-sports-data-event-' . $event_id);

        $event_data = $data['data']['data'];

        // Save team logos (homeTeam and awayTeam)
        $team_ids = [
            $event_data['homeTeam']['id'],
            $event_data['awayTeam']['id'],
        ];

        $logo = !empty($event_data['tournament']['uniqueTournament']['category']['sport']['slug']) ? $event_data['tournament']['uniqueTournament']['category']['sport']['slug'] : null;

        if ($logo) {
            (new Request())->save_logo(Request::TEAM_LOGO, '', $logo, $team_ids);
        }

        // Delete transient after no errors
        $transient_id = 'guru-sports-data-event-' . $event_id;
        delete_transient($transient_id);

        // Save string (json) for DB
        $updated_data = json_encode($event_data);

        $data['data']['data'] = $updated_data;

        $update_by = isset($data['id']) ? ['id' => $data['id']] : ['id' => '9999999999'];
        $data['data']['modified'] = current_time('mysql'); // set modified data
        if ($update_by['id'] == '9999999999') { // check if it's created or updated
            $data['data']['created'] = current_time('mysql'); // set created data
        }
        (new DatabaseService)->createOrUpdate(GURU_SPORTS_DATA_EVENTS_TABLE, $update_by, $data['data']);
    }

    /**
     * Schedule an action with the hook 'guru_sports_data_trigger_odds_fetch'
     * to run at 10 minutes interval.
     */
    public function guru_sport_data_trigger_odds_save() {
        // Seen in Admin UI - Main Sports Data Import.
        if (false === as_next_scheduled_action(GURU_SPORTS_SCHEDULE_SYNC)) {
            $args = [
                'retry' => 0,
            ];

            as_schedule_cron_action(
                time(),
                '0 10 * * *',
                GURU_SPORTS_SCHEDULE_SYNC,
                $args,
                "guru_sports_data_fetch_events"
            );
        }

//        if (false === as_next_scheduled_action('guru_sports_data_trigger_odds_fetch')) {
//                as_schedule_recurring_action(
//                    time(),
//                    30 * 60,
//                    'guru_sports_data_trigger_odds_fetch',
//                    [],
//                    "guru_sports_data_save_odds"
//                );
//        }
    }

    /**
     * A callback to run when the 'guru_sports_data_trigger_odds_fetch' scheduled action is run.
     */
    public function guru_sports_data_trigger_odds_fetch_action() {
        self::guru_sports_data_fetch_odds();
    }

    /**
    * Search array key by value.
    */
    public function guru_sports_data_search_for_array_item($name, $array, $type = 'key') {
      if (!empty($array)) {
        foreach ($array as $key => $val) {
          if ($val['name'] === $name) {
            if ($type == 'key') {
                return $val['key'];
            } else {
                return $val['name'];
            }
          }
        }
      }

      return null;
    }

    public function guru_sports_data_fetch_odds() {
        $leagueKey = 'soccer_uefa_european_championship';

        $odds_cached = get_transient('uefa-2020-european-odds-data');
        $odds_cached = FALSE;
        $odds = FALSE;

        if (!$odds_cached) {
            $odds = Request::fetch_odds($leagueKey);
        }

        $odds_data = empty($odds_cached) && !empty($odds->data->success) && !empty($odds->data->data) ? $odds->data->data : $odds_cached;

        $odds_usa = Request::fetch_odds($leagueKey, 'us');
        $odds_usa_data = !empty($odds_usa->data->success) && !empty($odds_usa->data->data) ? $odds_usa->data->data : FALSE;

        set_transient('uefa-2020-european-odds-data', $odds_data);

        // EU Odds.
        if (!empty($odds_data)) {
            foreach ($odds_data as $key => $entry) {

                $teams = array();
                foreach ($entry->teams as $team_key => $team) {
                    $teams[] = array(
                        'key' => $team_key,
                        'name' => $team,
                    );
                }

                $event_id   = $entry->id;
                $team1      = $entry->teams[0];
                $team2      = $entry->teams[1];
                $home_team  = $entry->home_team == $team1 ? $team1 : $entry->home_team;
                $away_team  = $home_team == $team1 ? $team2 : $team1;
                $team1_key  = self::guru_sports_data_search_for_array_item($home_team, $teams, 'key');
                $team2_key  = self::guru_sports_data_search_for_array_item($away_team, $teams, 'key');
                $draw_key   = 2;
                $event_timestamp = $entry->commence_time;

                foreach ($entry->sites as $site) {
                    $bookmaker_name = $site->site_nice;
                    $bookmaker_id   = sbg_get_bookmaker_id_from_name($bookmaker_name);

                    if (!empty($site->odds->h2h) && !empty($bookmaker_id)) {
                        foreach ($site->odds as $site_odd_key => $site_odd) {
                            if ($site_odd_key == 'h2h') {

                                $odd_1 = $site_odd[$team1_key];
                                $odd_2 = $site_odd[$team2_key];
                                $odd_x = $site_odd[2];
                                $odd_last_update = $site->last_update;

                                $data = [
                                    'event_id'        => $event_id,
                                    'event_timestamp' => $event_timestamp,
                                    'bookmaker_id'    => $bookmaker_id,
                                    'bookmaker_name'  => $bookmaker_name,
                                    'home_team'       => $home_team,
                                    'away_team'       => $away_team,
                                    'odd_1'           => $odd_1,
                                    'odd_2'           => $odd_2,
                                    'odd_x'           => $odd_x,
                                    'odd_last_update' => $odd_last_update,
                                    'region'          => 'eu',
                                ];

                                // d($teams);
                                // var_dump($site_odd);
                                // var_dump($site_odd[0]);
                                // var_dump($site_odd[1]);
                                // var_dump($site_odd[2]);
                                // d($team1_key);
                                // d($team2_key);
                                // d($entry);
                                // d($data);
                                // die();

                                as_schedule_single_action(
                                    time(),
                                    'guru_sports_data_save_single_odd_data',
                                    [
                                        'event_id'        => $event_id,
                                        'event_timestamp' => $event_timestamp,
                                        'bookmaker_id'    => $bookmaker_id,
                                        'bookmaker_name'  => $bookmaker_name,
                                        'home_team'       => $home_team,
                                        'away_team'       => $away_team,
                                        'odd_1'           => $odd_1,
                                        'odd_2'           => $odd_2,
                                        'odd_x'           => $odd_x,
                                        'odd_last_update' => $odd_last_update,
                                        'region'          => 'eu',
                                    ],
                                    "guru_sports_data_save_odds"
                                );
                            }

                        }
                    }

                }

            }
        }

        // USA Odds
        if (!empty($odds_usa_data)) {
            foreach ($odds_usa_data as $key => $entry) {

                $teams = array();
                foreach ($entry->teams as $team_key => $team) {
                    $teams[] = array(
                        'key' => $team_key,
                        'name' => $team,
                    );
                }

                $event_id   = $entry->id;
                $team1      = $entry->teams[0];
                $team2      = $entry->teams[1];
                $home_team  = $entry->home_team == $team1 ? $team1 : $entry->home_team;
                $away_team  = $home_team == $team1 ? $team2 : $team1;
                $team1_key  = self::guru_sports_data_search_for_array_item($home_team, $teams, 'key');
                $team2_key  = self::guru_sports_data_search_for_array_item($away_team, $teams, 'key');
                $draw_key   = 2;
                $event_timestamp = $entry->commence_time;

                foreach ($entry->sites as $site) {
                    $bookmaker_name = $site->site_nice;
                    $bookmaker_id   = sbg_get_bookmaker_id_from_name($bookmaker_name);

                    if ($bookmaker_name != 'SugarHouse') {
                        continue;
                    }

                    if (!empty($site->odds->h2h) && !empty($bookmaker_id)) {
                        foreach ($site->odds as $site_odd_key => $site_odd) {
                            if ($site_odd_key == 'h2h') {

                                $odd_1 = $site_odd[$team1_key];
                                $odd_2 = $site_odd[$team2_key];
                                $odd_x = $site_odd[2];
                                $odd_last_update = $site->last_update;

                                $data = [
                                    'event_id'        => $event_id,
                                    'event_timestamp' => $event_timestamp,
                                    'bookmaker_id'    => $bookmaker_id,
                                    'bookmaker_name'  => $bookmaker_name,
                                    'home_team'       => $home_team,
                                    'away_team'       => $away_team,
                                    'odd_1'           => $odd_1,
                                    'odd_2'           => $odd_2,
                                    'odd_x'           => $odd_x,
                                    'odd_last_update' => $odd_last_update,
                                    'region'          => 'us',
                                ];

                                as_schedule_single_action(
                                    time(),
                                    'guru_sports_data_save_single_odd_data',
                                    [
                                        'event_id'        => $event_id,
                                        'event_timestamp' => $event_timestamp,
                                        'bookmaker_id'    => $bookmaker_id,
                                        'bookmaker_name'  => $bookmaker_name,
                                        'home_team'       => $home_team,
                                        'away_team'       => $away_team,
                                        'odd_1'           => $odd_1,
                                        'odd_2'           => $odd_2,
                                        'odd_x'           => $odd_x,
                                        'odd_last_update' => $odd_last_update,
                                        'region'          => 'us',
                                    ],
                                    "guru_sports_data_save_odds"
                                );
                            }

                        }
                    }

                }

            }
        }

        // die();
        // Crawler::crawlOdds($leagueKey);
    }

    public function guru_sports_data_get_single_event_odds() {
        // $home_team = 'Turkey';
        // $away_team = 'Italy';
        // $event_timestamp = 1623438000;

        $home_team = $_POST['home_team'];
        $away_team = $_POST['away_team'];
        $event_timestamp = $_POST['event_timestamp'];

        $odds = get_odds_by_event($home_team, $away_team, $event_timestamp, $get_links = TRUE);
        // d($odds);

        try {
            wp_send_json_success($odds);
        } catch (Exception $exception) {
            wp_send_json_error('Something went wrong with fetching odds: ' . $exception->getMessage());
        }
    }

    public function guru_sports_data_save_single_odd_data($event_id, $event_timestamp, $bookmaker_id, $bookmaker_name, $home_team, $away_team, $odd_1, $odd_2, $odd_x, $odd_last_update, $region) {
        $data =  [
            'name' => $home_team . ' - ' . $away_team . ' (' . $bookmaker_name . ')',
            'created' => current_time('mysql'),
            'event_id' => $event_id,
            'event_timestamp' => $event_timestamp,
            'bookmaker_id' => $bookmaker_id,
            'bookmaker_name' => $bookmaker_name,
            'home_team' => $home_team,
            'away_team' => $away_team,
            'odd_1' => $odd_1,
            'odd_2' => $odd_2,
            'odd_x' => $odd_x,
            'odd_last_update' => $odd_last_update,
            'region' => $region,
            'modified' => current_time('mysql'),
        ];

        $update_by = isset($data['event_id']) ? ['event_id' => $data['event_id'], 'bookmaker_id' => $data['bookmaker_id']] : ['event_id' => '9999999999'];

        (new DatabaseService)->createOrUpdate('wp_pods_odds_feed', $update_by, $data);
    }

    public function retrieve_seasons_callback()
    {
        Crawler::crawlSeasons($_POST['url']);
    }

    public function save_new_league_with_seasons()
    {
        save_new_league($_POST['data']);
    }

    public function get_leagues()
    {
        get_leagues_call();
    }

    public function get_seasons()
    {
        get_seasons_call($_POST['league_id']);
    }

    public function save_sync()
    {
        save_sync_call($_POST['data']);
    }

    public function get_syncs()
    {
        get_syncs_call();
    }

    public function delete_sync()
    {
        delete_sync_call($_POST['id']);
    }

    public function fetch_specific_sync()
    {
        fetch_specific_sync_call($_POST['id']);
    }

    public function load_custom_wp_admin_style() { }

    /**
     * Enqueue styles and js
     */
    public function enqueue_admin_scripts()
    {

    }

    public function enqueue()
    {
        //wp_enqueue_style('guru-sport-data-shortcode-styles', plugins_url( '/assets/dist/shortcode.css', __FILE__ ));
        //wp_enqueue_style('guru-sport-data-euro-2020-styles', plugins_url( '/assets/dist/euro-2020-widget.css', __FILE__ ));
        //wp_enqueue_style( 'slick-styles', plugins_url( '/assets/css/slick.css', __FILE__ ));
	    wp_enqueue_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js', array(), null, true );
	}

    /**
     * Saves error to txt file (in case)
     */
    public function save_error()
    {
        file_put_contents(dirname(__file__). '/error_activation.txt', ob_get_contents());
    }

    /**
     * Run when deactivate plugin.
     *
     * Deletes pods database
     * Deletes plugin options
     * Deletes scheduled hooks
     */
    public static function guru_sports_data_deactivate()
    {
        require_once Guru_Sports_Data_PATH . 'includes/guru-sports-data-deactivator.php';
        Guru_Sports_Data_Deactivator::deactivate();
    }

    /**
     * Run when activate plugin.
     *
     * Runs migration (database)
     * Imports default settings (API URL, API key, etc...)
     * Fetches from API first time to have data
     */
    public function guru_sports_data_activate()
    {
        require_once Guru_Sports_Data_PATH . 'includes/guru-sports-data-activator.php';
        Guru_Sports_Data_Activator::activate();
    }

    /**
     * Loading plugin functions files
     */
    public function guru_sports_data_includes()
    {
        require_once __DIR__ . '/includes/requests/guru-sports-data-request.php';
        require_once __DIR__ . '/includes/guru-sports-data-crawler.php';
        require_once __DIR__ . '/includes/admin-page.php';
        require_once __DIR__ . '/includes/guru-sports-data-functions.php';
        require_once __DIR__ . '/includes/euro-2020-widget.php';
    }

    /**
     * Migrate necessary database through pods
     */
    public static function migrate() {
		global $wpdb;
		    $dump = file_get_contents( Guru_Sports_Data_PATH . 'imports/sports_data.sql' );
		    // Execute SQL statements
		    global $wpdb;
		    // set the default character set and collation for the table
		    $charset_collate = $wpdb->get_charset_collate();
		    // Check that the table does not already exist before continuing

		    require_once ABSPATH . 'wp-admin/includes/upgrade.php';
		    $result = dbDelta( $dump );
		    $is_error = empty( $wpdb->last_error );
		    return $is_error;


		// remove PODS
//        pods_api()->import_package($leagues_table, false);
//        pods_api()->import_package($leagues_season_table, false);
//        pods_api()->import_package($events_table, false);
//        pods_api()->import_package($syncs_table, false);
//        pods_api()->cache_flush_pods();
//        pods_api()->load_pods();

    }

    public function guru_sports_data_add_plugin_page_settings_link()
    {
        $links[] = '<a href="' .
            admin_url( 'options-general.php?page=' . Guru_Sports_Data_Admin::MENU_URL ) .
            '">' . __('Settings') . '</a>';
        return $links;
    }

}

function Guru_Sports_Data()
{
    return Guru_Sports_Data::get_instance();
}

$GLOBALS['Guru_Sports_Data'] = Guru_Sports_Data();
